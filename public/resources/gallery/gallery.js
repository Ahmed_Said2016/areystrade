let galleryContainer,
    element,
    onSelectionFunction,
    navMediaTab,
    uploadForm,
    uploadingInput,
    uploadButton,
    uploadingLink,
    terminateButton,
    mediaDetailsWrapper,
    filterDateSelect,
    mediaContainer,
    galleryPath,
    selectionLimit,
    resultInputPrefix,
    progressThumb;
let thumbs = [];
let previews = [];
Node.prototype.gallery = function ({uploadTarget, path, limit, inputPrefix, onSelect}) {
    this.addEventListener('click', function (e) {
        e.preventDefault();
        element = this;
        onSelectionFunction = onSelect;
        galleryContainer = createWrapper();
        // let wrapper = createWrapper();
        let container = createGalleryContainer(galleryContainer);
        // gallery link
        let dataTarget = this.getAttribute('href');
        // uploading images link
        uploadingLink = this.hasAttribute('data-upload-target') ? this.getAttribute('data-upload-target') : uploadTarget;
        // Gallery path
        galleryPath = path !== undefined ? path : this.getAttribute('data-path');
        // determine Choosen Limit
        selectionLimit = limit;
        // determine input prefix
        resultInputPrefix = inputPrefix;
        closeGallery(container);
        container.appendChild(galleryHeader());
        createNav(container);
        createTabContent(container);
        container.appendChild(galleryFooter());
        // get gallery with ajax
        getGalleryItems(dataTarget);

    });

};
let getGalleryItems = (dataTarget) => {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            //console.log(typeof JSON.parse(this.response));
            convertResponseToItems(JSON.parse(this.response));
        }
    }
    xhr.open('GET', dataTarget);
    xhr.send();
}
let convertResponseToItems = (response) => {
    Array.prototype.forEach.call(response, (item) => {
        let container, mediaItem, imageWrapper, image, span;
        container = createDivNode('thumb');
        container.id = `__thumb_${item.id}`;
        container.setAttribute('data-id', item.id);
        mediaItem = createDivNode('media-item');
        imageWrapper = createDivNode('image-wrapper');
        image = document.createElement('img');
        image.src = `${galleryPath}/${item.thumb}`;
        image.setAttribute('data-date', item.created_at);
        image.setAttribute('alt', item.name);
        span = document.createElement('span');
        span.className = 'selected';
        imageWrapper.appendChild(image);
        mediaItem.appendChild(imageWrapper);
        mediaItem.appendChild(span);
        container.appendChild(mediaItem);
        // add container to thumbs array
        thumbs.push(container);
        // add function to container
        container.onclick = function () {
            clickThumbMethod(this);
        }
        // append image item to container
        mediaContainer.appendChild(container);
    })
}
let clickThumbMethod = (thumb) => {
    if (thumb.classList.contains('selected')) {
        deselectThumb(thumb);
        return;
    }
    switch (selectionLimit) {
        case 1:
            deselectAllThumbs();
            selectThumb(thumb);
            break;
        case undefined:
            selectThumb(thumb);
            break;
        default:
            selectThumbWithLimitation(thumb);
    }
}
let deselectAllThumbs = () => {
    previews = [];
    mediaDetailsWrapper.innerHTML = '';
    thumbs.forEach(thumb => {
        thumb.classList.remove('selected');
    });
}
let selectThumbWithLimitation = (thumb) => {
    let selectedThumbs = thumbs.filter((item) => {
        return item.classList.contains('selected');
    }).length;
    if (selectedThumbs < selectionLimit) {
        selectThumb(thumb);
    } else {
        alert(`${selectionLimit} only is allowed`);
    }
}
let selectThumb = (thumb) => {
    thumb.classList.add('selected');
    previews.forEach(function (preview) {
        if (preview.classList.contains('active')) {
            preview.classList.remove('active');
        }
    })
    mediaDetailsWrapper.appendChild(thumbPreview(thumb));
    terminateButton.disabled = false;
}
let deselectThumb = (thumb) => {
    thumb.classList.remove('selected');
    let selectPreview = previews.find((preview) => {
        return preview.id === `__preview${thumb.id}`;
    });
    previews = previews.filter(function (element) {
        return element.id !== selectPreview.id;
    });
    selectPreview.remove();
    if (previews.length > 0) {
        previews[0].classList.add('active');
    } else {
        terminateButton.disabled = true;
    }

}
let createWrapper = () => {
    let wrapper = document.createElement('DIV');
    wrapper.id = 'gallery_wrapper';
    wrapper.className = 'gallery-wrapper';
    document.body.appendChild(wrapper);
    return wrapper;
}
let createGalleryContainer = (wrapper) => {
    let container = document.createElement('DIV');
    container.className = 'gallery-container';
    wrapper.appendChild(container);
    return container;
}
let closeGallery = (container) => {
    let closeLink = document.createElement('a');
    closeLink.className = 'close-gallery';
    closeLink.innerHTML = '<i class="fas fa-times"></i>';
    closeLink.href = '#';
    container.appendChild(closeLink);
    closeLink.addEventListener('click', function (e) {
        e.preventDefault();
        terminateGallery();
    });
}
let terminateGallery = () => {
    galleryContainer.remove();
    thumbs = [];
}
let galleryHeader = () => {
    let header = document.createElement('H1');
    header.innerHTML = 'Gallery';
    return header;
}
let createNav = (container) => {
    let nav = document.createElement('NAV');
    createNavTabs(nav);
    container.appendChild(nav);
}
let createNavTabs = (nav) => {
    let div = document.createElement('DIV');
    div.classList.add('nav', 'nav-tabs');
    div.id = 'nav-tab';
    div.setAttribute('role', 'tablist');
    div.appendChild(createTab({
        classes: 'nav-item nav-link',
        id: 'nav-home-tab',
        dataToggle: 'tab',
        href: '#nav-home',
        role: 'tab',
        inner: 'Upload Files',
        ariaControls: 'nav-home',
        ariaSelected: 'true'
    }));
    navMediaTab = createTab({
        classes: 'nav-item nav-link active',
        id: 'nav-media-tab',
        dataToggle: 'tab',
        href: '#nav-media',
        role: 'tab',
        inner: 'Media Library',
        ariaControls: 'nav-home',
        ariaSelected: 'true'
    });
    div.appendChild(navMediaTab);
    nav.appendChild(div);
}
let createTab = ({classes, id, dataToggle, href, role, inner, ariaControls, ariaSelected}) => {
    let tab = document.createElement('a');
    tab.className = classes;
    tab.id = id;
    tab.setAttribute('data-toggle', dataToggle);
    tab.href = href;
    tab.setAttribute('role', role);
    tab.innerHTML = inner;
    tab.setAttribute('aria-controls', ariaControls);
    tab.setAttribute('aria-selected', ariaSelected);
    return tab;

}
let createTabContent = (container) => {
    let div = document.createElement('DIV');
    div.className = 'tab-content';
    div.id = 'nav-tabContent';
    div.appendChild(tabPan({
        classes: 'tab-pane fade',
        id: 'nav-home',
        role: 'tabpanel',
        ariaLabelledby: 'nav-home-tab',
        content: uploadFormContainer(),
    }));
    div.appendChild(tabPan({
        classes: 'tab-pane fade show active',
        id: 'nav-media',
        role: 'tabpanel',
        ariaLabelledby: 'nav-media-tab',
        content: galleryWrapper(),
    }));
    container.appendChild(div);
}
let tabPan = ({classes, id, role, ariaLabelledby, content}) => {
    let div = document.createElement('DIV');
    div.id = id;
    div.className = classes;
    div.setAttribute('role', role);
    div.setAttribute('aria-labelledby', ariaLabelledby);
    if (content !== undefined) {
        div.appendChild(content);
    }
    return div;

}
let galleryFooter = () => {
    terminateButton = document.createElement('BUTTON');
    terminateButton.className = 'btn btn-gallery-footer';
    terminateButton.disabled = true;
    terminateButton.innerHTML = 'Use this files';
    terminateButton.addEventListener('click', () => {
        extractResult();
    });
    let div = document.createElement('DIV');
    div.className = 'gallery-footer';
    div.appendChild(terminateButton);
    return div;

}
let extractResult = () => {
    let parentElement = element.parentNode;
    let nextElement = element.nextSibling;
    if (nextElement !== null) {
        if (nextElement.nextElementSibling !== null && nextElement.nextElementSibling.className === 'media-result') {
            nextElement.nextElementSibling.remove();
            //console.log("first "+nextElement.nextElementSibling);
        }
        if (nextElement.className === 'media-result') {
            nextElement.remove();
            //console.log(nextElement);
        }
        parentElement.insertBefore(createResultContainer(), element.nextSibling);
    } else {
        parentElement.appendChild(createResultContainer());
    }
    terminateGallery();
    if (onSelectionFunction !== undefined) {
        onSelectionFunction(element);
    }
}
let createResultContainer = () => {
    let container = createDivNode('media-result');
    container.appendChild(mediaDetailsWrapper);
    return container;
}
let uploadFormContainer = () => {
    let div = document.createElement('DIV');
    div.className = 'gallery-upload-wrapper';
    let header = document.createElement('H2');
    header.innerHTML = 'Select files anywhere to upload';
    let span = document.createElement('SPAN');
    span.className = 'gallery-max-size';
    span.innerHTML = 'Maximum upload file size: 64 MB.';
    div.appendChild(header);
    div.appendChild(uploadingForm({uploadingLink}));
    div.appendChild(span);
    return div;
}
let prepareUploadingInput = () => {
    uploadingInput = document.createElement('INPUT');
    uploadingInput.setAttribute('type', 'file');
    uploadingInput.setAttribute('name', 'image');
    uploadingInput.addEventListener('change', function () {
        uploadingImage();
    });
}
let uploadingForm = ({uploadingLink}) => {
    uploadForm = document.createElement('FORM');
    uploadForm.setAttribute('method', 'post');
    uploadForm.setAttribute('enctype', 'multipart/form-data');
    uploadForm.setAttribute('action', uploadingLink);
    // uploadForm.appendChild(csrfToken());
    uploadForm.appendChild(uploadingArea());
    uploadForm.appendChild(uploadingButton());
    return uploadForm;

}
let csrfToken = () => {
    let csrfInput = document.createElement('INPUT');
    csrfInput.setAttribute('type', 'hidden');
    csrfInput.setAttribute('name', '_token');
    //csrfInput.setAttribute('value', document.head.querySelector('[name~=csrf-token][content]').content);
    return csrfInput;
}
let uploadingArea = () => {
    let div = document.createElement('DIV');
    div.className = 'upload-hidden';
    prepareUploadingInput();
    div.appendChild(uploadingInput);
    return div;

}
let uploadingButton = () => {
    uploadButton = document.createElement('BUTTON');
    uploadButton.className = 'gallery-upload-button';
    uploadButton.innerHTML = 'Select files';
    uploadButton.addEventListener('click', function (e) {
        e.preventDefault();
        uploadingInput.click();
    })
    return uploadButton;
}
let uploadingImage = () => {
    let data = new FormData(uploadForm);
    uploadForm.reset();
    navMediaTab.click();
    createProgressThumb();
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.status === 200 && this.readyState === 4) {
            initLoadingResponse(JSON.parse(this.responseText));
        }
    }
    xhr.upload.addEventListener('progress', function (e) {
        const percent = e.lengthComputable ? (e.loaded / e.total) * 100 : 0;
        document.getElementById('__progress_par').style.width = percent.toFixed(0) + "%";
    })

    xhr.open('POST', uploadingLink);
    // xhr.setRequestHeader("X-CSRF-TOKEN", document.head.querySelector('[name~=csrf-token][content]').content);
    xhr.send(data);
}
let createProgressThumb = () => {
    let container, mediaItem, loadingArea, progressContainer, progressBar, span;
    container = createDivNode('thumb');
    mediaItem = createDivNode('media-item');
    loadingArea = createDivNode('loading-area');
    progressContainer = createDivNode('progress');
    progressBar = createDivNode('progress-bar');
    progressBar.id = '__progress_par';
    progressBar.setAttribute('role', 'progressbar');
    progressBar.setAttribute('aria-valuenow', '0');
    progressBar.setAttribute('aria-valuemin', '0');
    progressBar.setAttribute('aria-valuemax', '100');
    progressBar.style.cssText = 'width:0%';
    span = document.createElement('span');
    span.className = 'selected';
    progressContainer.appendChild(progressBar);
    loadingArea.appendChild(progressContainer);
    mediaItem.appendChild(loadingArea);
    mediaItem.appendChild(span);
    container.appendChild(mediaItem);
    // add container to thumbs array
    thumbs.push(container);
    // append image item to container
    mediaContainer.prepend(container);
    // progress thumb
    progressThumb = container;
}
let initLoadingResponse = (response) => {
    let mediaItem = progressThumb.querySelector('.media-item');
    let loadingArea = mediaItem.querySelector('.loading-area');
    loadingArea.remove();
    let imageWrapper = createDivNode('image-wrapper');
    let image = document.createElement('img');
    image.src = `${galleryPath}/${response.thumb}`;
    image.setAttribute('data-date', response.created_at.date);
    image.setAttribute('alt', response.name);
    imageWrapper.appendChild(image);
    mediaItem.prepend(imageWrapper);
    progressThumb.id = `__thumb_${response.id}`;
    progressThumb.setAttribute('data-id', response.id);
    progressThumb.onclick = function () {
        clickThumbMethod(this);
    };
    progressThumb = undefined;
};
let galleryWrapper = () => {
    let mediaWrapper = createDivNode('media-wrapper');
    let row = createDivNode('row');
    let itemWrapper = createDivNode('col-md-9 media-items-wrapper');
    let detailsWrapper = createDivNode('col-md-3 media-details-wrapper');
    mediaDetailsWrapper = createDivNode('media-details');

    detailsWrapper.appendChild(mediaDetailsWrapper);
    itemWrapper.appendChild(mediaItemsContainer());
    row.appendChild(itemWrapper);
    row.appendChild(detailsWrapper);
    mediaWrapper.appendChild(row);
    return mediaWrapper;

}
let mediaItemsContainer = () => {
    let container = createDivNode('media-container');
    let header = createDivNode('media-header')
    let mediaBody = createDivNode('media-body');
    let mediaBodyContainer = createDivNode('media-body-container');
    mediaContainer = createDivNode('media-row');
    mediaBodyContainer.appendChild(mediaContainer);
    mediaBody.appendChild(mediaBodyContainer);
    header.appendChild(createFilterDate());
    container.appendChild(header);
    container.appendChild(mediaBody);
    return container;
}
let createFilterDate = () => {
    filterDateSelect = document.createElement('SELECT');
    filterDateSelect.className = 'custom-select mr-sm-2';
    filterDateSelect.id = 'inlineFormCustomSelect';
    let initOption = document.createElement('option');
    initOption.text = 'Select date';
    let optionAll = document.createElement('option');
    optionAll.text = 'All dates';
    optionAll.value = 'all';
    filterDateSelect.add(initOption);
    filterDateSelect.add(optionAll);
    filterDateSelect.addEventListener('change', function () {
        // filter by date
    });
    return filterDateSelect;
}
const createDivNode = (nodeClass) => {
    let div = document.createElement('DIV');
    div.className = nodeClass;
    return div;
}
let thumbPreview = (thumb) => {
    let thumbImage,
        thumbId,
        preview,
        header,
        imageWrapper,
        image,
        titleSpan,
        dateSpan,
        deletedLink,
        inputsWrapper,
        galleryIdInput;
    thumbImage = thumb.getElementsByTagName('img')[0];
    thumbId = thumb.getAttribute('data-id');
    preview = createDivNode('thumb-preview active');
    preview.id = `__preview${thumb.id}`;
    header = document.createElement('h2');
    header.innerHTML = 'ATTACHMENT DETAILS';
    imageWrapper = createDivNode('image-wrapper');
    image = document.createElement('img');
    image.src = thumbImage.src;
    imageWrapper.appendChild(image);
    titleSpan = document.createElement('span');
    titleSpan.className = 'img-title';
    titleSpan.innerHTML = thumbImage.getAttribute('alt');
    dateSpan = document.createElement('span');
    dateSpan.className = 'date';
    dateSpan.innerHTML = thumbImage.getAttribute('data-date');
    deletedLink = document.createElement('a');
    deletedLink.href = '#';
    deletedLink.className = 'delete-img';
    deletedLink.innerHTML = 'Delete Permanently';
    inputsWrapper = createDivNode('details-inputs');
    galleryIdInput = document.createElement('input');
    galleryIdInput.setAttribute('type', 'hidden');
    galleryIdInput.setAttribute('name', `${resultInputPrefix}[${thumbId}][image_id]`);
    galleryIdInput.value = thumbId;
    inputsWrapper.appendChild(galleryIdInput);
    inputsWrapper.appendChild(previewField({
        id: thumbId,
        text: 'Alt Text',
        name: 'alt',
        note: 'Describe the purpose of the image(opens in a new tab).',
        type: 'input'
    }));
    inputsWrapper.appendChild(previewField({id: thumbId, text: 'Title', name: 'title', type: 'textarea'}));
    inputsWrapper.appendChild(previewField({id: thumbId, text: 'Caption', name: 'caption', type: 'textarea'}));
    //inputsWrapper.appendChild(previewTextAreaField({id: thumbId, text: 'Description', name: 'description'}));
    preview.appendChild(header);
    preview.appendChild(imageWrapper);
    preview.appendChild(titleSpan);
    preview.appendChild(dateSpan);
    preview.appendChild(deletedLink);
    preview.appendChild(inputsWrapper);
    // preview to preview array
    previews.push(preview);
    return preview;

};

const previewField = ({id, text, name, note, type}) => {
    let field = createDivNode('form-group row');
    let label = document.createElement('label');
    label.className = 'col-sm-4 col-form-label';
    label.innerHTML = text;
    let inputContainer = createDivNode('col-sm-8');
    let inputElement = document.createElement(type);
    inputElement.setAttribute('name', `${resultInputPrefix}[${id}][${name}]`);
    inputElement.className = 'form-control';
    inputContainer.appendChild(inputElement);
    if (note === undefined) {
        let noteLabel = document.createElement('small');
        noteLabel.className = 'form-text text-muted';
        noteLabel.innerHTML = note;
        inputContainer.appendChild(noteLabel);
    }
    field.appendChild(label);
    field.appendChild(inputContainer);
    return field;
};

