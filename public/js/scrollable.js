const prevControl = document.getElementById('__previous');
const nextControl = document.getElementById('__next');
const activeStyle = "scrollable__control--active";

nextControl.addEventListener('click', function (event) {
    event.preventDefault();
    const elements = scrollableElements(this);
    if (elements.scrollContainer.clientWidth + elements.offset < elements.scrollWidth && this.classList.contains(activeStyle)) {
        prevControl.classList.add(activeStyle);
        elements.scrollable.scrollLeft += elements.scrollContainer.clientWidth;
        if ((elements.scrollContainer.clientWidth * 2) + elements.offset > elements.scrollWidth) {
            this.classList.remove(activeStyle);
        }
        console.log(`container width:${elements.scrollContainer.clientWidth} offset:${elements.offset} scroll width:${elements.scrollWidth}`);
    }
});
prevControl.addEventListener('click', function (event) {
    event.preventDefault();
    const elements = scrollableElements(this);
    if (elements.offset > 0 && this.classList.contains(activeStyle)) {
        nextControl.classList.add(activeStyle);
        elements.scrollable.scrollLeft -= elements.scrollContainer.clientWidth;
        if (elements.offset <= elements.scrollContainer.clientWidth) {
            this.classList.remove(activeStyle);
        }
    }
});
const scrollableElements = (element) => {
    return {
        scrollContainer: element.parentNode.querySelector('.scrollable__container'),
        get scrollable() {
            return this.scrollContainer.querySelector('.scrollable__wrapper');
        },
        get scrollWidth() {
            return this.scrollable.scrollWidth;
        },
        get offset() {
            return this.scrollable.scrollLeft;
        }
    }
};
for (let scrollable of document.getElementsByClassName('scrollable__wrapper')) {
    scrollable.scrollLeft = 0;
}