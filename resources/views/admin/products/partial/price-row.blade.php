@php
$id=$key??$price->id??0;
@endphp
<tr tabindex="{{$id}}" class="__price_elements">
    @if(!is_null($price->id))
        <input type="hidden" name="price[{{$id}}][id]" value="{{$id}}">
    @endif
    <td>
        <span>
            <i class="las la-greater-than-equal"></i>
        </span>
        <input type="number" name="price[{{$id}}][minimum_quantity]" value="{{$price->minimum_quantity}}"
               aria-label="Price Quantity">
    </td>
    <td>
        <span>
            US <i class="las la-dollar-sign"></i>
        </span>
        <input type="number" name="price[{{$id}}][amount]" value="{{$price->amount}}" aria-label="Price Amount">
    </td>
    <td>
        <a href="#" class="remove-row"><i class="las la-trash-alt"></i></a>
    </td>
</tr>
