@php
    $category_id=old('main.category_id')??optional($product->category)->id;
@endphp
<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-product-basic" role="tabpanel"
         aria-labelledby="nav-product-basic-tab">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="__select_category">Category:</label>
                    <select class="custom-select" id="__select_category" name="main[category_id]"
                            data-product-id="{{$product->id}}" data-route="{{route('ajax.admin.category.filters')}}"
                            required>
                        <option value=""> --- Select Category ---</option>
                        @foreach ($categories as $category)
                            <option value="{{$category->id}}"{{$category->id==$category_id?'selected':''}}>
                                {{implode(' > ',array_reverse($category->full_en_name))}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="__product_brand">Brand</label>
                    <select name="main[brand_id]" id="__product_brand" class="custom-select">

                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="__vendor">Vendor</label>
                    <select name="main[vendor_id]" class="custom-select" id="__vendor" required>
                        <option value="">Select a vendor</option>
                        @foreach($vendors as $vendor)
                            @php
                                $prevVendor=old('main.vendor_id')??optional($product->vendor)->id;
                            @endphp
                            <option value="{{$vendor->id}}"{{$vendor->id==$prevVendor?'selected':''}}>{{$vendor->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Product name</label>
            <div class="row">
                <div class="col">
                    <x-form-elements.input-flag-group name="main.en_name" :value="$product->en_name"
                                                      placeHolder="English Product Name" image="en" :required="true"/>
                </div>
                <div class="col">
                    <x-form-elements.input-flag-group name="main.ar_name" :value="$product->ar_name"
                                                      placeHolder="Arabic Product Name" image="ar" :required="true"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Product Slug</label>
            <div class="row">
                <div class="col">
                    <x-form-elements.input-flag-group name="main.en_slug" :value="$product->en_slug"
                                                      placeHolder="English Product Slug" image="en" :required="true"/>
                </div>
                <div class="col">
                    <x-form-elements.input-flag-group name="main.ar_slug" :value="$product->ar_slug"
                                                      placeHolder="Arabic Product Slug" image="ar" :required="true"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Status</label>
                    <x-form-elements.selector name="main.status" :list="\App\Models\Product::statusOptionsOnly()"
                                              :required="true" :current-value="$product->status_key"/>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Sort order</label>
                    <x-form-elements.input-type-number :value="$product->sort_order" name="main.sort_order"/>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Intro Image ( 350 x 350 )</label>
                    <x-form-elements.input-type-file name="main.image"/>
                    @error('main.image')
                    <small class="text-danger">Image Is Required</small>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    {{-- meta tags --}}
    <div class="tab-pane fade" id="nav-meta" role="tabpanel"
         aria-labelledby="nav-meta-tab">
        <x-dashboard.meta-tags :model="$product" input-array="tags"/>
    </div>
    {{-- end meta tags --}}
    {{-- description --}}
    <div class="tab-pane fade" id="nav-description" role="tabpanel"
         aria-labelledby="nav-description-tab">
        <b>Product Photos <small>(photos is required)</small></b>
        <div class="gallery-product-wrapper">
            <span class="notes">Image file size should be less than 5MB. Supported formats: .jpeg .jpg .png</span>
            <span class="notes">Recommended image size is more than 640px * 640px. Images should be clear and easy
                for buyers to view at a glance</span>
            <a id="__add_gallery" href="{{route('api.gallery.index')}}" class="add-gallery">
                Select from Photo Bank
            </a>
            <div class="media-result">
                <div class="media-details">
                    @if(old('gallery'))
                        @foreach(old('gallery') as $image)
                            <x-old-gallery-thumb name="gallery" :old="$image"/>
                        @endforeach
                    @elseif(!is_null($product->images))
                        @foreach($product->images as $image)
                            @php
                                $name="gallery[{$image->id}]";
                            @endphp
                            <x-gallery-thumb :image="$image" :input-name="$name"/>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="__en_description">Describe Product in English Language</label>
            <textarea id="__en_description" name="description[en_description]">
                {!!old('description.en_description')??optional($product->description)->en_description!!}
            </textarea>
        </div>
        <div class="form-group">
            <label for="__ar_description">Describe Product in Arabic Language</label>
            <textarea id="__ar_description" name="description[ar_description]">
                {!!old('description.ar_description')??optional($product->description)->ar_description!!}
            </textarea>
        </div>
    </div>
    {{-- end description --}}
    {{-- filters --}}
    <div class="tab-pane fade" id="nav-filters" role="tabpanel"
         aria-labelledby="nav-filters-tab">
        <h2 class="section-title">Product Filters</h2>
        <div id="__product_filters_wrapper">
            @includeWhen($category_id,'admin.products.partial.filters',compact('category_id'))
        </div>
    </div>
    {{-- Trade Information--}}
    <div class="tab-pane fade" id="nav-price" role="tabpanel" aria-labelledby="nav-nav-price">
        <h2 class="section-title">Trade Information</h2>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Product Price Setting</label>
            @php
                $currentPriceUnit=old('price_setting.unit')??optional($product->priceSetting)->unit;
                $currentPriceType=old('price_setting.type')??optional($product->priceSetting)->type;
            @endphp
            <div class="col-sm-9">
                <div class="form-check form-check-inline">
                    <input name="price_setting[type]" class="form-check-input" type="radio"
                           value="fob" {{$currentPriceType=='fob'?'checked':''}}>
                    <label class="form-check-label">FOB Price</label>
                </div>
                <div class="form-check form-check-inline">
                    <input name="price_setting[type]" class="form-check-input" type="radio"
                           value="cif" {{$currentPriceType=='cif'?'checked':''}}>
                    <label class="form-check-label">CIF Price</label>
                </div>
            </div>
        </div>
        <div class="form-group required row">
            <label class="col-sm-3 col-form-label">Product Units</label>
            <div class="col-sm-3">
                <select name="price_setting[unit]" class="custom-select" aria-label="Product Units" required>
                    <option disabled>Select Unit</option>
                    @foreach(trans('products.units') as $unit)
                        <option value="{{$unit}}" {!! $currentPriceUnit==$unit?'selected':'' !!}>{{$unit}}</option>
                    @endforeach

                </select>
            </div>
        </div>
        <div class="form-group required row">
            <label class="col-sm-3 col-form-label">Quantity Price</label>
            <div class="col-sm-9">
                <table class="table price-table">
                    <thead>
                    <tr>
                        <th>MOQ (Unit)</th>
                        <th>Price (Unit)</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="__price_elements_container">
                    @php
                        $prices = $product->prices;
                        $oldPrices=old('price')??null;
                    @endphp
                    @if(!is_null($oldPrices))
                        @foreach($oldPrices as $key=>$price)
                            @include('admin.products.partial.price-row',['price'=>new \App\Models\Price($price),'key'=>$key])
                        @endforeach
                    @elseif(!is_null($prices) && $prices->count()>0)
                        @foreach($prices as $price)
                            @include('admin.products.partial.price-row')
                        @endforeach
                    @else
                        @include('admin.products.partial.price-row',['price'=>new \App\Models\Price()])
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3">
                            <a href="#" id="__insert_new_row">New price range</a> Can set range
                            price within 4
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

    </div>
</div>
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('resources/gallery/gallery.css')}}">
@endpush
@push('javascript')
    <script src="https://cdn.ckeditor.com/ckeditor5/12.0.0/classic/ckeditor.js"></script>
    <script src="{{asset('resources/gallery/gallery.js')}}"></script>
    <script>
        const enDescription = document.getElementById('__en_description');
        const arDescription = document.getElementById('__ar_description');
        ClassicEditor.create(enDescription);
        ClassicEditor.create(arDescription);
        // Gallery
        document.getElementById('__add_gallery').gallery({
            uploadTarget: "{{route('api.gallery.store')}}",
            inputPrefix: 'gallery',
            limit: 5,
            path: "{{asset('storage')}}",
        });
        // get filters after select category
        document.getElementById('__select_category').onchange = function () {
            if (this.value.length) {
                let product_id = this.getAttribute('data-product-id');
                let route = this.getAttribute('data-route');
                let url = `${route}?category_id=${this.value}&product_id=${product_id}`;
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        // type result in filter wrapper div
                        document.getElementById('__product_filters_wrapper').innerHTML = this.responseText;
                    }
                };
                xhr.open('GET', url, true);
                xhr.send();

            }
        };
        // price Elements
        const priceElements = document.getElementsByClassName('__price_elements');
        const addPriceRowLink = document.getElementById('__insert_new_row');
        const priceElementsContainer = document.getElementById('__price_elements_container');
        const createPriceElement = (name, index, spanContent) => {
            let td, input, span;
            td = document.createElement('td');
            span = document.createElement('span');
            span.innerHTML = `${spanContent} `;
            input = document.createElement('input');
            input.type = 'number';
            input.name = `price[${index}][${name}]`;
            td.append(span);
            td.append(input);
            return td;
        };
        const createRemoveLink = () => {
            let td = document.createElement('td');
            let removeLink = document.createElement('a');
            removeLink.href = '#';
            removeLink.className = 'remove-row';
            removeLink.innerHTML = `<i class="las la-trash-alt"></i>`;
            removeRow(removeLink);
            td.append(removeLink);
            return td;
        };
        const createPriceRow = (index) => {
            let tr, quantityTd, priceTd, removeLinkTd;
            tr = document.createElement('tr');
            tr.className = '__price_elements';
            tr.setAttribute('tabindex', index);
            quantityTd = createPriceElement('minimum_quantity', index, '<i class="las la-greater-than-equal"></i>');
            priceTd = createPriceElement('amount', index, 'US <i class="las la-dollar-sign"></i>');
            removeLinkTd = createRemoveLink();
            tr.append(quantityTd, priceTd, removeLinkTd);
            return tr;
        };
        const removeRow = (link) => {
            link.addEventListener('click', function (event) {
                event.preventDefault();
                if (priceElements.length > 1) {
                    let row = link.parentNode.parentNode;
                    row.remove();
                    addPriceRowLink.classList.remove('in-active');
                }
            })
        };
        const indexProvider = () => {
            let index = 0;
            if (priceElements.length === 0) {
                return index;
            }
            Array.prototype.forEach.call(priceElements, element => {
                let elementIndex = element.getAttribute('tabindex');
                if (elementIndex > index) {
                    index = elementIndex;
                }
            });
            return parseInt(index) + 1;
        };
        let removeLinks = document.getElementsByClassName('remove-row');
        for (let link of removeLinks) {
            removeRow(link);
        }
        addPriceRowLink.onclick = event => {
            event.preventDefault();
            if (priceElements.length < 4) {
                console.log(priceElements.length);
                let index = indexProvider();
                priceElementsContainer.append(createPriceRow(index));
                if (priceElements.length === 4) {
                    addPriceRowLink.classList.add('in-active');
                }
            }


        };
    </script>
@endpush