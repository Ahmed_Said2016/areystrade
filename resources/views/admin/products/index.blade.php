@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Products</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Index</a>
        </li>
    </ul>
@endsection
@section('content')
    <section class="content">
        <div class="card data-table-wrapper">
            <div class="card-header card-header-min">
                <h2>All Products</h2>
                <a href="{{route('admin.products.create')}}" class="btn btn-warning addable">
                    <i class="las la-plus"></i> Add new Product
                </a>

            </div>
            <div class="card-body">
                @include('admin.layouts.search-form',['route'=>route('admin.products.index'),'options'=>\App\Models\Product::statusOptions()])
                <div class="result-count">
                    <i class="lab la-shopware"></i> {{$products->lastItem()??0}}/{{$products->total()}} Product
                </div>
                <table class="table table-striped table-min">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category</th>
                        <th>Product</th>
                        <th>Vendor</th>
                        <th>Min Price</th>
                        <th>Min Qty</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td class="text-left">{{str_limit(implode(' > ', array_reverse($product->category->full_en_name)),50,'...')}}</td>
                            <td>{{$product->en_name}}</td>
                            <td>{{$product->vendor->name}}</td>
                            <td>{{$product->min_price->amount}}</td>
                            <td>{{$product->min_quantity->minimum_quantity}}</td>
                            <td class="actions">
                                <a href="{{route('admin.products.edit',compact('product'))}}" class="editable">
                                    <i class="las la-edit"></i>
                                </a>
                                <form action="{{route('admin.products.destroy',compact('product'))}}" method="post">
                                    @csrf @method('DELETE')
                                    <button class="text-danger">
                                        <i class="las la-trash-restore"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper">
                    {{$products->withQueryString()->links()}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('css/panel/table.min.css')}}">
@endsection