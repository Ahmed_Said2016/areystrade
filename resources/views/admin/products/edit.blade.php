@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Products</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Update</a>
        </li>
    </ul>
@endsection
@section('content')
    <section class="content">
        <div class="card form-wrapper">
            @include('admin.layouts.card-header',['title'=>"Update Product :{$product->en_name}",'form_id'=>'main_form'])
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-product-basic-tab" data-toggle="tab"
                           href="#nav-product-basic"
                           role="tab" aria-controls="nav-product-basic" aria-selected="true">
                            Product details
                        </a>
                        <a class="nav-item nav-link" id="nav-meta-tab" data-toggle="tab" href="#nav-meta"
                           role="tab" aria-controls="nav-meta" aria-selected="false">
                            Meta Tags
                        </a>
                        <a class="nav-item nav-link" id="nav-description-tab" data-toggle="tab"
                           href="#nav-description"
                           role="tab" aria-controls="nav-description" aria-selected="false">
                            Descriptions
                        </a>
                        <a class="nav-item nav-link" id="nav-filters-tab" data-toggle="tab" href="#nav-filters"
                           role="tab" aria-controls="nav-filters" aria-selected="false">
                            Filters
                        </a>
                        <a class="nav-item nav-link" id="nav-price-tab" data-toggle="tab" href="#nav-price"
                           role="tab" aria-controls="nav-price" aria-selected="false">
                            Trade Information
                        </a>

                    </div>
                </nav>
                <form method="post" id="main_form" action="{{route('admin.products.update',compact('product'))}}"
                      enctype="multipart/form-data">
                    @csrf @method('PUT')
                    @include('admin.products.partial.form')
                </form>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/panel/form.css')}}">
@endsection
@prepend('javascript')
    <script src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>
@endprepend