<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="{{asset('css/panel/login.css')}}">
    <title>C-Panel</title>
</head>
<body>
<div class="login-container">
    <div class="login-area">
        <h1>Admin Dashboard</h1>
        <form method="post" action="{{route('admin.login')}}">
            {{csrf_field()}}
            <div class="form-group">
                <div class="input-group @error('email') is-invalid @enderror mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="email-addon">
                            <i class="las la-user-circle"></i>
                        </span>
                    </div>
                    <input type="email" name="email" class="form-control"
                           value="{{old('email')}}" placeholder="email@email.com" aria-label="email"
                           aria-describedby="email-addon" readonly>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group @error('password') is-invalid @enderror mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="password-addon">
                            <i class="las la-unlock-alt"></i>
                        </span>
                    </div>
                    <input type="password" name="password" class="form-control" placeholder="Password"
                           aria-label="password"
                           aria-describedby="password-addon" readonly>
                </div>
            </div>
            <div class="form-check-container">
                <label>
                    <input type="checkbox" name="remember">
                    <span class="checkmark"></span>
                    Remember me
                </label>
            </div>
            <div class="login-btn-area">
                <button class="btn btn-primary btn-block">
                    login
                </button>
            </div>
            <div class="row footer-link-area">
                <div class="col">
                    <a href="{{route('home')}}">
                        <i class="las la-arrow-left"></i> Back to Home
                    </a>
                </div>
                <div class="col text-right">
                    <a href="">
                        Forget your password?
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    for (let formControl of document.getElementsByClassName('form-control')) {
        formControl.addEventListener('focus', function () {
            if (this.hasAttribute('readonly')) {
                this.removeAttribute('readonly');
            }
        });
        formControl.addEventListener('blur', function () {

            this.setAttribute('readonly', 'true');
        });
    }
</script>
</body>
</html>