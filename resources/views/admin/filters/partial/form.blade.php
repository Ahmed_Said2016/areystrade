<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
         aria-labelledby="nav-home-tab">
        <div class="form-group">
            <label>Group name</label>
            <div class="row">
                <div class="col">
                    <x-form-elements.input-flag-group name="filters.basic.en_name" :value="$filter->en_name"
                                                      placeHolder="English Filter Name" image="en" :required="true"/>
                </div>
                <div class="col">
                    <x-form-elements.input-flag-group name="filters.basic.ar_name" :value="$filter->ar_name"
                                                      placeHolder="Arabic Filter Name" image="ar" :required="true"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Status</label>
                    <select class="custom-select" name="filters[basic][status]" aria-label="Filter Status" required>
                        @php
                        $currentFilterStatus=$filter->status['status']??null;
                        @endphp
                        @foreach(\App\Models\Filter::statusOptions() as $key=>$status)
                            <option value="{{$key}}" {{$currentFilterStatus===$status['status']?'selected':''}}>{{$status['status']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Sort order</label>
                    <input type="number" class="form-control" name="filters[basic][sort_order]"
                           aria-label="Filter Sort Order"
                           value="{{old('filters.basic.sort_order]')??$filter->sort_order??0}}"
                           required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Has Single Value</label>
                    <select class="custom-select" name="filters[basic][has_single_value]" aria-label="Filter Value" required>

                        @foreach(\App\Models\Filter::singleAttributesOptions() as $key=>$status)
                            <option value="{{$key}}" {{$key===$filter->has_single_value?'selected':''}}>{{$status['status']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Is Required</label>
                    <select class="custom-select" name="filters[basic][is_required]" aria-label="Filter Status" required>
                        @foreach(\App\Models\Filter::booleanAttributesOptions() as $key=>$status)
                            <option value="{{$key}}" {{$key===$filter->is_required?'selected':''}}>{{$status['status']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Accept Value</label>
                    <select class="custom-select" name="filters[basic][accept_value]" aria-label="Filter Status" required>
                        @foreach(\App\Models\Filter::booleanAttributesOptions() as $key=>$status)
                            <option value="{{$key}}" {{$key===$filter->accept_value?'selected':''}}>{{$status['status']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-filters" role="tabpanel"
         aria-labelledby="nav-filters-tab">
        <div class="group-items-section">
            <h2>
                <span>Filters List</span>
                <a class="btn btn-warning" id="__add_row">
                    <i class="fas fa-plus-circle"></i> Add row
                </a>
            </h2>
            <div class="group-items-wrapper-header">
                <div class="row">
                    <div class="col-md-4">
                        <span>Filter English Name</span>
                    </div>
                    <div class="col-md-4">
                        <span>Filter Arabic Name</span>
                    </div>
                    <div class="col-md-3">
                        <span>Filter Sort Order</span>
                    </div>
                </div>
            </div>
            <div class="group-items-wrapper-body" id="__items_container">
                @if(old('filters.items'))
                    @foreach(old('filters.items') as $key=>$value)
                        <div class="row" tabindex="{{$key}}">
                            @php
                                $en_key="filters.items.$key.en_name";
                                $ar_key="filters.items.$key.ar_name";
                            @endphp
                            <div class="col-md-4">
                                <x-form-elements.input-flag-group :name="$en_key" :value="$value['en_name']"
                                                                  placeHolder="English Filter Name" image="en"
                                                                  :required="true"/>
                            </div>
                            <div class="col-md-4">
                                <x-form-elements.input-flag-group :name="$ar_key" :value="$value['ar_name']"
                                                                  placeHolder="Arabic Filter Name" image="ar"
                                                                  :required="true"/>
                            </div>
                            <div class="col-md-3">
                                <input type="number" value="{{$value['sort_order']??0}}"
                                       class="form-control" aria-label="Filter Sort Order"
                                       name="filters[items][{{$key}}][sort_order]" required>
                            </div>
                            <div class="col-md-1">
                                <a href="#" class="btn btn-danger btn-block __remove_item">
                                    <i class="las la-trash"></i>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @elseif(optional($filter)->items()->exists())
                    @foreach($filter->items as $item)
                        <div class="row" tabindex="{{$item->id}}">
                            <input type="hidden" name="filters[items][{{$item->id}}][id]" value="{{$item->id}}">
                            <div class="col-md-4">
                                <x-form-elements.input-flag-group :name="sprintf('filters.items.%d.en_name',$item->id)"
                                                                  :value="$item->en_name"
                                                                  placeHolder="English Filter Name" image="en"
                                                                  :required="true"/>
                            </div>
                            <div class="col-md-4">
                                <x-form-elements.input-flag-group :name="sprintf('filters.items.%d.ar_name',$item->id)"
                                                                  :value="$item->ar_name"
                                                                  placeHolder="Arabic Filter Name" image="ar"
                                                                  :required="true"/>
                            </div>
                            <div class="col-md-3">
                                <input type="number" value="{{$item->sort_order??0}}"
                                       class="form-control" aria-label="Filter Sort Order"
                                       name="filters[items][{{$item->id}}][sort_order]" required>
                            </div>
                            <div class="col-md-1">
                                <a href="#" class="btn btn-danger btn-block __remove_item">
                                    <i class="las la-trash"></i>
                                </a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@push('javascript')
    <script>
        const itemsContainer = document.getElementById('__items_container');
        const addItemRowLink = document.getElementById('__add_row');
        const removeLinks = document.getElementsByClassName('__remove_item');
        for (let link of removeLinks) {
            link.addEventListener('click', function (e) {
                e.preventDefault();
                this.parentNode.parentNode.remove();
            });
        }
        addItemRowLink.addEventListener('click', (e) => {
            e.preventDefault();
            createItemRow();
        });
        const getLastTabIndex = () => {
            let rows = itemsContainer.children;
            if (rows.length <= 0) {
                return 0;
            }
            return Array.prototype.map.call(rows, function (row) {
                if (row.classList.contains('row')) {
                    return parseInt(row.getAttribute('tabindex'));
                }
            }).reduce((a, b) => {
                return Math.max(a, b);
            });
        };
        const createDivElement = (styleName) => {
            let div = document.createElement('div');
            div.className = styleName;
            return div;
        };

        const createInputElement = ({placeHolder, name, imageSrc}) => {
            let container, inputGroup, inputGroupPrepend, span, image, input;
            container = createDivElement('col-md-4');
            inputGroup = createDivElement('input-group mb-3');
            inputGroupPrepend = createDivElement('input-group-prepend');
            span = document.createElement('span');
            span.className = "input-group-text";
            image = document.createElement('img');
            input = document.createElement('input');
            input.name = name;
            input.className = "form-control";
            input.placeholder = placeHolder;
            input.required = true;
            input.autocomplete = "off";
            image.src = imageSrc;
            image.alt = name;
            span.append(image);
            inputGroupPrepend.append(span);
            inputGroup.append(inputGroupPrepend);
            inputGroup.append(input);
            container.append(inputGroup);
            return container;

        };
        const createSortOrderElement = (index) => {
            let container = createDivElement('col-md-3');
            let input = document.createElement('input');
            input.type = 'number';
            input.value = "0";
            input.className = 'form-control';
            input.required = true;
            input.name = `filters[items][${index}][sort_order]`;
            container.append(input);
            return container;
        };
        const crateRemoveRowLink = () => {
            let container = createDivElement('col-md-1');
            let link = document.createElement('a');
            link.href = "#";
            link.className = "btn btn-danger btn-block __remove_item";
            link.innerHTML = '<i class="las la-trash"></i>';
            link.addEventListener('click', function (e) {
                e.preventDefault();

                this.parentNode.parentNode.remove();
            });
            container.append(link);
            return container;
        };
        const createRow = (index) => {
            let container, englishInput, arabicInput, sortOrderInput, removeLink;
            container = document.createElement('div');
            container.className = 'row';
            container.setAttribute('tabindex', index);
            englishInput = createInputElement({
                placeHolder: 'English Name',
                name: `filters[items][${index}][en_name]`,
                imageSrc: "{{asset('images/panel/en-flag.jpg')}}"
            });
            arabicInput = createInputElement({
                placeHolder: 'Arabic Name',
                name: `filters[items][${index}][ar_name]`,
                imageSrc: "{{asset('images/panel/eg-flag.jpg')}}"
            });
            sortOrderInput = createSortOrderElement(index);
            removeLink = crateRemoveRowLink();
            container.append(englishInput);
            container.append(arabicInput);
            container.append(sortOrderInput);
            container.append(removeLink);
            return container;
        };
        const createItemRow = () => {
            let index = getLastTabIndex() + 1;
            let row = createRow(index);
            itemsContainer.append(row);
        };

    </script>
@endpush