<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">
            <img src="{{asset('images/panel/en-flag.jpg')}}" alt="Filter English Name">
        </span>
    </div>
    <input type="text" name="filters[items][{{$key}}][en_name]" value="{{$value['en_name']}}" class="form-control"
           placeholder="English name" aria-label="english filter name" autocomplete="off" required>
</div>