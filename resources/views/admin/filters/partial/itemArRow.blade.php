<div class="input-group mb-3">
    <div class="input-group-prepend">
        <span class="input-group-text">
            <img src="{{asset('images/panel/eg-flag.jpg')}}" alt="Filter arabic Name">
        </span>
    </div>
    <input type="text" name="filters[items][{{$key}}][ar_name]" value="{{$value['ar_name']}}" class="form-control"
           placeholder="Arabic name" aria-label="arabic filter name" autocomplete="off" required>
</div>