@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Filters</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Creating</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card form-wrapper">
            <div class="card-header card-header-min">
                <h2>Add new filter</h2>
                <div class="card-header-actions">
                    <button class="btn btn-success" form="basic_form">
                        <i class="las la-save"></i>
                    </button>
                    <a class="btn btn-warning" href="{{url()->previous()}}">
                        <i class="fas fa-reply-all"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                           role="tab" aria-controls="nav-home" aria-selected="true">
                            Filters Group
                        </a>
                        <a class="nav-item nav-link" id="nav-filters-tab" data-toggle="tab" href="#nav-filters"
                           role="tab" aria-controls="nav-filters" aria-selected="false">
                            Filters
                        </a>

                    </div>
                </nav>
                <form method="post" id="basic_form" action="{{route('admin.filters.store')}}">
                    @csrf
                    @include('admin.filters.partial.form')
                </form>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/panel/form.css')}}">
@endsection
@push('javascript')
    <script src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>
@endpush