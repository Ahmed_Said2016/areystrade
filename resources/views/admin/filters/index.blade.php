@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Filters</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Index</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card data-table-wrapper">
            <div class="card-header card-header-min">
                <h2>All Products Filters</h2>
                <a href="{{route('admin.filters.create')}}" class="btn btn-warning addable">
                    <i class="las la-plus"></i> new filter
                </a>
            </div>
            <div class="card-body">
                <form method="get" action="{{route('admin.filters.index')}}" id="__filterForm">
                    <div class="search-form-wrapper">
                        <div class="search-form">
                            <div class="search-form-col">
                                <select name="limit" class="custom-select">
                                    <option disabled selected>Select limit</option>
                                    @foreach(paginationLimit() as $limit)
                                        @php
                                            $selectedLimit=request()->has('limit')&& request()->get('limit')===$limit?'selected':'';
                                        @endphp
                                        <option value="{{$limit}}" {{$selectedLimit}}>{{$limit}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="search-form-col">
                                <input name="criteria" class="form-control" autocomplete="off" placeholder="Search For">
                            </div>
                            <div class="search-form-col">
                                <select class="custom-select" name="status">
                                    <option disabled selected>Select Status</option>
                                    <option value="">All</option>
                                    @foreach(\App\Models\Vendor::statusOptions() as $key=>$value)
                                        <option value="{{$key}}">{{$value['status']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-success"><i class="fas fa-filter"></i></button>
                            <button type="reset" class="btn btn-secondary" onclick="this.form.reset();">
                                <i class="fas fa-power-off"></i>
                            </button>
                        </div>
                    </div>
                </form>
                <div class="result-count">
                    <i class="las la-filter"></i> {{$filters->lastItem()}}/{{$filters->total()}} Filter
                </div>
                <table class="table table-striped table-min">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Filter</th>
                        <th>Sort order</th>
                        <th>Status</th>
                        <th>Accept Value</th>
                        <th>Single/Multiple</th>
                        <th>Required</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($filters as $filter)
                        <tr>
                            <td>{{$filter->id}}</td>
                            <td>{{$filter->en_name}}</td>
                            <td>{{$filter->sort_order}}</td>
                            <td class="status">
                                <button class="btn  {{$filter->status['btn_color']}}">{{$filter->status['status']}}</button>
                            </td>
                            <td class="status">
                                <i class="{{$filter->acceptable_status['style']}}"></i>
                            </td>
                            <td class="status">
                                <button class="btn {{$filter->single_status['btn_color']}}">{{$filter->single_status['status']}}</button>
                            </td>
                            <td class="status">
                                <i class="{{$filter->required_status['style']}}"></i>
                            </td>
                            <td class="actions">
                                <a href="{{route('admin.filters.edit',compact('filter'))}}" class="editable">
                                    <i class="las la-edit"></i>
                                </a>
                                <form action="{{route('admin.filters.destroy',compact('filter'))}}" method="post">
                                    @csrf @method('DELETE')
                                    <button class="text-danger">
                                        <i class="las la-trash-restore"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper">
                    {{$filters->withQueryString()->links()}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('css/panel/table.min.css')}}">
@stop