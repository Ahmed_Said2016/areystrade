@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Brands</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Creating</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card form-wrapper">
            <div class="card-header card-header-min">
                <h2>Add new Brand</h2>
                <div class="card-header-actions">
                    <button class="btn btn-success" form="basic_form">
                        <i class="las la-save"></i>
                    </button>
                    <a class="btn btn-warning" href="{{url()->previous()}}">
                        <i class="fas fa-reply-all"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{route('admin.brands.store')}}" id="basic_form" enctype="multipart/form-data">
                    @csrf
                    @include('admin.brands.partial.form')
                </form>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/panel/form.css')}}">
@endsection
