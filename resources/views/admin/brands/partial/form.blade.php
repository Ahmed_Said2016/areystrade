<div class="form-group">
    <label>Brand name <i class="fas fa-trademark"></i></label>
    <div class="row">
        <div class="col">
            <x-form-elements.input-flag-group name="en_name" :value="$brand->en_name"
                                              placeHolder="English Filter Name" image="en" :required="true"/>
        </div>
        <div class="col">
            <x-form-elements.input-flag-group name="ar_name" :value="$brand->ar_name"
                                              placeHolder="Arabic Filter Name" image="ar" :required="true"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label>Status</label>
            <select class="custom-select" name="status" aria-label="Brand Status" required>
                @php
                    $currentStatus=$brand->status['status']??null;
                @endphp
                @foreach(\App\Models\Brand::statusOptions() as $key=>$status)
                    <option value="{{$key}}" {{$currentStatus===$status['status']?'selected':''}}>{{$status['status']}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Sort order</label>
            <input type="number" class="form-control" name="sort_order"
                   aria-label="Brand Sort Order"
                   value="{{old('sort_order]')??$brand->sort_order??0}}"
                   required>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label>Brand Image</label>
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input">
                <label class="custom-file-label">Choose image</label>
            </div>
        </div>
    </div>
</div>