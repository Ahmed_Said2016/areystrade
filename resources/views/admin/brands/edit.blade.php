@extends('admin.layouts.master')
@section('content')
    @if($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="alert-heading">Oops! something went wrong</h4>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <section class="content">
        <div class="card form-wrapper">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <h2>Edit brand</h2>
                    </div>
                    <div class="col text-right">
                        <button class="btn btn-success" form="basic_form"><i class="fas fa-save"></i></button>
                        <a class="btn btn-primary" href="{{URL::previous()}}"><i class="fas fa-reply-all"></i></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{route('admin.brands.update',compact('brand'))}}" id="basic_form"
                      enctype="multipart/form-data">
                    @csrf @method('PUT')
                    @include('admin.brands.partial.form')
                </form>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/panel/form.css')}}">
@endsection
