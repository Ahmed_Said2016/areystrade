@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Brands</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Index</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card data-table-wrapper">
            <div class="card-header card-header-min">
                <h2>All Products Brands</h2>
                <a href="{{route('admin.brands.create')}}" class="btn btn-warning addable">
                    <i class="fas fa-forward"></i> Add new brand
                </a>
            </div>
            <div class="card-body">
                <form method="get" action="{{route('admin.brands.index')}}">
                    <div class="search-form-wrapper">
                        <div class="search-form">
                            <div class="search-form-col">
                                <select name="limit" class="custom-select">
                                    <option disabled selected>Select limit</option>
                                    @foreach(paginationLimit() as $limit)
                                        @php
                                            $selectedLimit=request()->has('limit')&& request()->get('limit')===$limit?'selected':'';
                                        @endphp
                                        <option value="{{$limit}}" {{$selectedLimit}}>{{$limit}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="search-form-col">
                                <input name="criteria" class="form-control" autocomplete="off" placeholder="Search For">
                            </div>
                            <div class="search-form-col">
                                <select class="custom-select" name="status">
                                    <option disabled selected>Select Status</option>
                                    <option value="">All</option>
                                    @foreach(\App\Models\Vendor::statusOptions() as $key=>$value)
                                        <option value="{{$key}}">{{$value['status']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-success"><i class="fas fa-filter"></i></button>
                            <button type="reset" class="btn btn-secondary" onclick="this.form.reset();">
                                <i class="fas fa-power-off"></i>
                            </button>
                        </div>
                    </div>
                </form>
                <div class="result-count">
                    <i class="lab la-adversal"></i> {{$brands->lastItem()}}/{{$brands->total()}} Brand
                </div>
                <table class="table table-striped table-min">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th class="ar-font">الاسم</th>
                        <th>Logo</th>
                        <th>Sort order</th>
                        <th>Publish date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($brands as $brand)
                        <tr>
                            <td>{{$brand->id}}</td>
                            <td>{{$brand->en_name}}</td>
                            <td class="ar-font">{{$brand->ar_name}}</td>
                            <td class="logo-thumb"><img src="{{asset('storage/'.$brand->thumb)}}" alt="#"></td>
                            <td>{{$brand->sort_order}}</td>
                            <td class="numbers">{{$brand->created_at}}</td>
                            <td class="status">
                                <button class="btn  {{$brand->status['btn_color']}}">{{$brand->status['status']}}</button>
                            </td>
                            <td class="actions">
                                <a href="{{route('admin.brands.edit',compact('brand'))}}" class="editable">
                                    <i class="las la-edit"></i>
                                </a>
                                <form action="{{route('admin.brands.destroy',compact('brand'))}}" method="post">
                                    @csrf @method('DELETE')
                                    <button class="text-danger">
                                        <i class="las la-trash-restore"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper">
                    {{$brands->withQueryString()->links()}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('css/panel/table.min.css')}}">
@stop