@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Sections</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Index</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card data-table-wrapper">
            <div class="card-header card-header-min">
                <h2>All Sections</h2>
                <a href="{{route('admin.sections.create')}}" class="btn btn-warning addable">
                    <i class="las la-plus"></i> Add new section
                </a>
            </div>
            <div class="card-body">
                @include('admin.layouts.search-form',['route'=>route('admin.sections.index'),'options'=>\App\Models\Section::statusOptions()])
                <div class="result-count">
                    <i class="las la-filter"></i> {{$sections->lastItem()}}/{{$sections->total()}} Sections
                </div>
                <table class="table table-striped table-min">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Section</th>
                        <th>Sort order</th>
                        <th>Publish date</th>
                        <th>Status</th>
                        <th>Home</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sections as $section)
                        <tr>
                            <td>{{$section->id}}</td>
                            <td>{{$section->en_name}}</td>
                            <td>{{$section->sort_order}}</td>
                            <td class="numbers">{{$section->created_at}}</td>
                            <td class="status">
                                @if($section->status)
                                    <button class="btn btn-success">Confirmed</button>
                                @else
                                    <button class="btn btn-warning">Pending</button>
                                @endif
                            </td>
                            <td class="status">
                                @if($section->home)
                                    <button class="btn btn-success">Confirmed</button>
                                @else
                                    <button class="btn btn-warning">Pending</button>
                                @endif
                            </td>
                            <td class="actions">
                                <a href="{{route('admin.sections.edit',['id'=>$section->id])}}" class="editable">
                                    <i class="fas fa-sign-out-alt"></i>
                                </a>
                                <form action="{{route('admin.sections.destroy',['id'=>$section->id])}}"
                                      method="post"
                                      id="delete_row">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="text-danger"><i class="fas fa-trash-restore"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper">
                    {{$sections->withQueryString()->links()}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('css/panel/table.min.css')}}">
@endsection