<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
         aria-labelledby="nav-home-tab">
        <div class="form-group">
            <label>Section name</label>
            <div class="row">
                <div class="col">
                    <x-form-elements.input-flag-group name="section.main.en_name" :value="$section->en_name"
                                                      placeHolder="English Section Name" image="en" :required="true"/>
                </div>
                <div class="col">
                    <x-form-elements.input-flag-group name="section.main.ar_name" :value="$section->ar_name"
                                                      placeHolder="English Section Name" image="ar" :required="true"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Status</label>
                    <select class="custom-select" name="section[main][status]" aria-label="Section Status" required>
                        @php
                            $currentFilterStatus=$section->status['status']??null;
                        @endphp
                        @foreach(\App\Models\Section::statusOptions() as $key=>$status)
                            <option value="{{$key}}" {{$currentFilterStatus===$status['status']?'selected':''}}>{{$status['status']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Sort order</label>
                    <input type="number" class="form-control" name="section[main][sort_order]"
                           aria-label="Section Sort Order" min="0"
                           value="{{old('section.main.sort_order]')??$section->sort_order??0}}"
                           required>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Symbol</label>
                    <input class="form-control" value="{{old('section.main.symbol]')??$section->symbol??''}}"
                           name="section[main][symbol]" list="symbols" aria-label="Section Menu Symbol">
                    <datalist id="symbols">
                        <option value="fas fa-mobile-alt">Phones</option>
                        <option value="fas fa-tv">Tv</option>
                        <option value="fas fa-laptop">Laptop</option>
                        <option value="fas fa-baby-carriage">Baby</option>
                        <option value="fas fa-home">Home</option>
                        <option value="fas fa-ankh">Souvenir</option>
                        <option value="fas fa-paw">Animals</option>
                    </datalist>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Banner Image</label>
                    <div class="custom-file @error('section.main.image') is-invalid @enderror">
                        <input type="file" name="section[main][image]" class="custom-file-input">
                        <label class="custom-file-label">Choose image</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="nav-details" role="tabpanel"
         aria-labelledby="nav-details-tab">
        <x-dashboard.meta-tags :model="$section" input-array="section.tags"/>
    </div>
</div>
