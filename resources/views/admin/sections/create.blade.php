@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Sections</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Create</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card form-wrapper">
@include('admin.layouts.card-header',['title'=>'Add new Section','form_id'=>'main_form'])
            <div class="card-body">
                @include('admin.sections.partial.nav')
                <form method="post" id="main_form" action="{{route('admin.sections.store')}}"
                      enctype="multipart/form-data">
                    @csrf
                    @include('admin.sections.partial.form')
                </form>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/panel/form.css')}}">
@endsection
@push('javascript')
    <script src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>
@endpush