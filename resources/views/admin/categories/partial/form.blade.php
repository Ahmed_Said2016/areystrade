<div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-category-basic" role="tabpanel"
         aria-labelledby="nav-category-basic-tab">
        <div class="form-group">
            <label>Parent:</label>
            <select class="custom-select" id="parent_input" aria-label="Parent Category" name="main[parent_id]">
                <option value="">Select Parent Category</option>
                @foreach ($categories as $categoryInList)
                    <option value="{{$categoryInList->id}}" {{$categoryInList->id===$category->parent_id?'selected':''}}>
                        {{implode(' > ',array_reverse($categoryInList->full_en_name))}}
                    </option>
                @endforeach

            </select>
        </div>
        <div class="form-group">
            <label>Category Name</label>
            <div class="row">
                <div class="col">
                    <x-form-elements.input-flag-group name="main.en_name" :value="$category->en_name"
                                                      placeHolder="English Category Name" image="en" :required="true"/>
                </div>
                <div class="col">
                    <x-form-elements.input-flag-group name="main.ar_name" :value="$category->ar_name"
                                                      placeHolder="Arabic Category Name" image="ar" :required="true"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Category Slug</label>
            <div class="row">
                <div class="col">
                    <x-form-elements.input-flag-group name="main.en_slug" :value="$category->en_slug"
                                                      placeHolder="English Category Slug" image="en" :required="true"/>
                </div>
                <div class="col">
                    <x-form-elements.input-flag-group name="main.ar_slug" :value="$category->ar_slug"
                                                      placeHolder="Arabic Category Slug" image="ar" :required="true"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Status</label>
                    <x-form-elements.selector name="main.status" :list="\App\Models\Category::statusOptionsOnly()"
                                              :required="true" :current-value="$category->status_key"/>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Sort order</label>
                    <x-form-elements.input-type-number :value="$category->sort_order" name="main.sort_order"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Banner Image ( 1350 x 325 )</label>
                    <x-form-elements.input-type-file name="main.image"/>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Icon</label>
                    <input class="form-control" value="{{old('main.icon')??$category->icon??''}}"
                           name="main.icon" list="symbols" aria-label="Category Menu icon">
                    <datalist id="symbols">
                        <option value="fas fa-mobile-alt">Phones</option>
                        <option value="fas fa-tv">Tv</option>
                        <option value="fas fa-laptop">Laptop</option>
                        <option value="fas fa-baby-carriage">Baby</option>
                        <option value="fas fa-home">Home</option>
                        <option value="fas fa-ankh">Souvenir</option>
                        <option value="fas fa-paw">Animals</option>
                    </datalist>
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label>Category Has Products</label>
                    <x-form-elements.selector name="main.has_products"
                                              :list="\App\Models\Category::productOptionsOnly()"
                                              :required="true" :current-value="$category->has_products"
                                              id="__toggle_has_product">
                        <option value="">if this Category has Products or Not</option>
                    </x-form-elements.selector>
                </div>
            </div>
        </div>
    </div>
    {{-- meta tags --}}
    <div class="tab-pane fade" id="nav-meta" role="tabpanel"
         aria-labelledby="nav-meta-tab">
        <x-dashboard.meta-tags :model="$category" input-array="tags"/>
    </div>
    {{-- end meta tags --}}
    {{-- brands --}}
    <div class="tab-pane fade" id="nav-brands" role="tabpanel"
         aria-labelledby="nav-brands-tab">
        <div class="row">
            <div class="col" id="brands_area">
                <div class="form-group">
                    <label>Brands</label>
                    <select name="brands[]" class="form-control is-invalid multi-choice" multiple="multiple"
                            id="__brands"
                            style="width: 100%" aria-label="Brands"{{!$category->has_products?' disabled':''}}>
                        @foreach($brands as $brand)
                            <option value="{{$brand->id}}" {{in_array($brand->id,$category->brands->pluck('id')->toArray())?'selected':''}}>
                                {{$brand->en_name}}</option>
                        @endforeach
                    </select>
                    @error('brands')
                    <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    {{-- end brands --}}
    {{-- filters --}}
    <div class="tab-pane fade" id="nav-filters" role="tabpanel"
         aria-labelledby="nav-filters-tab">
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label>Filters</label>
                    <select name="filters[]" class="form-control multi-choice" multiple="multiple" id="__filters"
                            style="width: 100%" aria-label="Filters"{{!$category->has_products?' disabled':''}}>
                        @foreach($filters as $filter)
                            <option value="{{$filter->id}}" {{in_array($filter->id,$category->filters->pluck('id')->toArray())?'selected':''}}>
                                {{$filter->en_name}}</option>
                        @endforeach
                    </select>
                    @error('filters')
                    <small class="text-danger">{{$message}}</small>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    {{-- end filters --}}
</div>
@push('javascript')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $('.multi-choice').select2([]);
        const toggleHasProductStatus = document.getElementById('__toggle_has_product');
        const filtersSelector = document.getElementById('__filters');
        const brandSelector = document.getElementById('__brands');
        toggleHasProductStatus.onchange = function () {
            if (parseInt(this.value) === 1) {
                filtersSelector.disabled = false;
                brandSelector.disabled = false;
                return true;
            }
            filtersSelector.disabled = true;
            brandSelector.disabled = true;
        }
    </script>
@endpush