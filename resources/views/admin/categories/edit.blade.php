@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Categories</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Edit</a>
        </li>
    </ul>
@endsection
@section('content')
    @error('has_product_cant_changing')
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="alert-heading">Oops! something went wrong</h4>
        {{$message}}
    </div>
    @enderror
    <section class="content">
        <div class="card form-wrapper">
            @include('admin.layouts.card-header',['title'=>"Update {$category->en_name}",'form_id'=>'main_form'])
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-category-basic-tab" data-toggle="tab"
                           href="#nav-category-basic"
                           role="tab" aria-controls="nav-category-basic" aria-selected="true">
                            Category details
                        </a>
                        <a class="nav-item nav-link" id="nav-meta-tab" data-toggle="tab" href="#nav-meta"
                           role="tab" aria-controls="nav-meta" aria-selected="false">
                            Meta Tags
                        </a>
                        <a class="nav-item nav-link" id="nav-brands-tab" data-toggle="tab" href="#nav-brands"
                           role="tab" aria-controls="nav-brands" aria-selected="false">
                            Brands
                        </a>
                        <a class="nav-item nav-link" id="nav-filters-tab" data-toggle="tab" href="#nav-filters"
                           role="tab" aria-controls="nav-filters" aria-selected="false">
                            Filters
                        </a>

                    </div>
                </nav>
                <form method="post" id="main_form" action="{{route('admin.categories.update',compact('category'))}}"
                      enctype="multipart/form-data">
                    @csrf @method('PUT')
                    @include('admin.categories.partial.form')
                </form>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/panel/form.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>>
@endsection
@prepend('javascript')
    <script src="{{asset('js/jquery-2.2.3.min.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js"></script>
@endprepend