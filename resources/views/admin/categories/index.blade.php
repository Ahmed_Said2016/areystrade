@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Categories</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Index</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card data-table-wrapper">
            <div class="card-header card-header-min">
                <h2>All Products Categories</h2>
                <a href="{{route('admin.categories.create')}}" class="btn btn-warning addable">
                    <i class="las la-plus"></i> Add new category
                </a>

            </div>
            <div class="card-body">
                @include('admin.layouts.search-form',['route'=>route('admin.categories.index'),'options'=>\App\Models\Category::statusOptions()])
                <div class="result-count">
                    <i class="las la-filter"></i> {{$categories->lastItem()??0}}/{{$categories->total()}} Category
                </div>
                <table class="table table-striped table-min">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category</th>
                        <th>Sort order</th>
                        <th>Status</th>
                        <th>Has Product</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td class="text-left">{{implode(' > ', array_reverse($category->full_en_name))}}</td>
                            <td>{{$category->sort_order}}</td>
                            <td class="status">
                                <button class="btn  {{$category->status['btn_color']}}">{{$category->status['status']}}</button>
                            </td>
                            <td class="status">
                                <button class="btn  {{$category->has_product_status['btn_color']}}">{{$category->has_product_status['status']}}</button>
                            </td>
                            <td class="actions">
                                <a href="{{route('admin.categories.edit',compact('category'))}}" class="editable">
                                    <i class="las la-edit"></i>
                                </a>
                                <form action="{{route('admin.categories.destroy',compact('category'))}}" method="post">
                                    @csrf @method('DELETE')
                                    <button class="text-danger">
                                        <i class="las la-trash-restore"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper">
                    {{$categories->withQueryString()->links()}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('css/panel/table.min.css')}}">
@endsection