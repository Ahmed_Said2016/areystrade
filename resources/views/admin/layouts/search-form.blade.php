<form method="get" action="{{$route}}">
    <div class="search-form-wrapper">
        <div class="search-form">
            <div class="search-form-col">
                <select name="limit" class="custom-select" aria-label="Page Limit">
                    <option disabled selected>Select limit</option>
                    @foreach(paginationLimit() as $limit)
                        @php
                            $selectedLimit=request()->has('limit')&& request()->get('limit')===$limit?'selected':'';
                        @endphp
                        <option value="{{$limit}}" {{$selectedLimit}}>{{$limit}}</option>
                    @endforeach
                </select>
            </div>
            <div class="search-form-col">
                <input name="criteria" class="form-control" autocomplete="off" aria-label="Search For" placeholder="Search For">
            </div>
            <div class="search-form-col">
                <select class="custom-select" name="status" aria-label="Select Status">
                    <option disabled selected>Select Status</option>
                    <option value="">All</option>
                    @foreach($options as $key=>$value)
                        <option value="{{$key}}">{{$value['status']}}</option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-success"><i class="fas fa-filter"></i></button>
            <button type="reset" class="btn btn-secondary" onclick="this.form.reset();">
                <i class="fas fa-power-off"></i>
            </button>
        </div>
    </div>
</form>