<div class="main-nav">
    <div class="dashboard-title">
        <i class="fas fa-tachometer-alt"></i>
        Control Panel
    </div>
    <ul class="sidebar">
        <li>
            <a href="{{route('admin.vendors.index')}}" class="green{{request()->segment(2)==='vendors'?' active':''}}">
                <i class="fas fa-users"></i> Vendors
            </a>
        </li>
        <li>
            <a href="{{route('admin.filters.index')}}" class="red{{request()->segment(2)==='filters'?' active':''}}">
                <i class="fas fa-filter"></i> Filters
            </a>
        </li>
        <li>
            <a href="{{route('admin.brands.index')}}" class="yellow{{request()->segment(2)==='brands'?' active':''}}">
                <i class="fas fa-bookmark"></i> Brands
            </a>
        </li>
        <li>
            <a href="{{route('admin.categories.index')}}"
               class="purple{{request()->segment(2)==='categories'?' active':''}}">
                <i class="fab fa-pagelines"></i> Categories
            </a>
        </li>
        <li>
            <a href="{{route('admin.products.index')}}" class="red">
                <i class="fab fa-product-hunt"></i> Products
            </a>
        </li>
        <li>
            <a href="" class="green">
                <i class="fas fa-user-circle"></i> Customers
            </a>
        </li>
        <li>
            <a href="" class="yellow">
                <i class="fas fa-universal-access"></i> Sales
            </a>
        </li>
        <li>
            <a href="" class="red">
                <i class="fas fa-users-cog"></i> Settings
            </a>
        </li>
    </ul>
</div>