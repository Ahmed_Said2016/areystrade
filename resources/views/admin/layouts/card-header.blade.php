<div class="card-header card-header-min">
    <h2>{{$title}}</h2>
    <div class="card-header-actions">
        <button class="btn btn-success" form="{{$form_id}}">
            <i class="las la-save"></i>
        </button>
        <a class="btn btn-warning" href="{{url()->previous()}}">
            <i class="fas fa-reply-all"></i>
        </a>
    </div>
</div>
