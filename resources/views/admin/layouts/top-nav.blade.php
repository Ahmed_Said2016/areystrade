<div class="top-nav">
    <div class="nav-logo">
        <a href="#" id="toggle_side_menu">
            <img src="{{asset('images/logo-matrix-neg.png')}}" alt="matrix">
            <i class="las la-bars"></i>
        </a>
    </div>
    <h1 class="header-title">
        <i class="las la-clipboard"></i>  Admin Dashboard
    </h1>
    <div class="header-link-area">
        <a href="{{route('admin.welcome')}}" class="nav-field">
            <i class="las la-home"></i>
        </a>
        <a href="" class="nav-field">
            <i class="lar la-bell"></i>
        </a>
        <a href="" class="nav-field active">
            <span>{{auth()->guard('admin')->user()->name}}</span>
            <i class="lar la-user-circle"></i>
        </a>
        <a href="#" class="nav-field fullscreen" title="full screen mode">
            <i class="las la-desktop"></i>
        </a>
        <a class="nav-field" href="{{route('admin.logout')}}" title="logout">
            <i class="las la-power-off"></i>
        </a>
        <a class="nav-field">
            <i class="las la-tools"></i>
        </a>
    </div>
</div>
@section('top-nav-js')
    @parent
    <script>
        $("a.fullscreen").click(function (event) {
            event.preventDefault();
            var elem = document.documentElement;
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) { /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) { /* IE/Edge */
                elem.msRequestFullscreen();
            }
        });
    </script>
@endsection