<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link rel="stylesheet" href="{{asset('css/panel/main.css')}}">
    @yield('css')
    @stack('css')
    <title>C-Panel</title>
</head>
<body>
<div class="panel-container">
    <div class="header-section">
        @include('admin.layouts.top-nav')
    </div>

    <div class="monitor-section {{$_COOKIE['menu_is_hidden']??''}}" id="__monitor_section">
        @include('admin.layouts.notifi')
        <div class="left-nav-section">
            @include('admin.layouts.main-nav')
        </div>
        <div class="content-section">
            @yield('directory')
            <div class="__content-wrapper">
                <div class="__content">
                    @yield('content')
                </div>
            </div>
        </div>
        <div class="footer">
            <span class="copy-rights">Copyright © 2019</span>
            <a href="mailto:info.matrixcode@gmail.com">Matrix Code Micro Systems</a> . All rights reserved.
        </div>
    </div>
</div>
@yield('javascript')
@stack('javascript')
<script>
    const monitorSection = document.getElementById('__monitor_section');
    document.getElementById('toggle_side_menu').addEventListener('click', function (e) {
        e.preventDefault();
        if (monitorSection.classList.contains('hidden-menu')) {
            monitorSection.classList.remove('hidden-menu');
            document.cookie = 'menu_is_hidden=';
            return true;
        }
        monitorSection.classList.add('hidden-menu');
        setCookie("menu_is_hidden", "hidden-menu", 365);
    });

    const setCookie = (name, value, days) => {
        let d = new Date();
        d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = name + "=" + value + ";" + expires + ";path=/";
    }
</script>
</body>
</html>