@if(session()->has('failure') || session()->has('success'))
    @php
        $notificationStyle='notifi-fail';
        $notificationSymbol='la-times';
        $notificationHeader='Oops! something went wrong';
        $notificationMessage=session()->get('failure');
        if(session()->has('success')){
            $notificationStyle='notifi-success';
            $notificationSymbol='la-check';
            $notificationHeader='Well Done!';
            $notificationMessage=session()->get('success');
        }
    @endphp
    <div class="alert-notifi" id="__notification">
        <div class="notifi {{$notificationStyle}} animate__animated animate__bounceInDown">
            <i class="las {{$notificationSymbol}}"></i>
            <div class="message">
                <span>{{$notificationHeader}}</span>
                {{$notificationMessage}}
            </div>
            <a href="#" class="notification-close" id="__close_notification">
                <i class="las la-times"></i>
            </a>
        </div>
    </div>
    @push('javascript')
        <script>
            const notificationContainer = document.getElementById('__notification');
            document.getElementById('__close_notification').addEventListener('click', function (event) {
                event.preventDefault();
                notificationContainer.classList.add('hide');
            })
        </script>
    @endpush
@endif
