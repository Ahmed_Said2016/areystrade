<div class="form-group">
    <label>Email</label>
    <x-form-elements.input name="email" :value="optional($vendor)->email" placeHolder="Vendor Email"
                           :required="true" type="email"/>
</div>
<div class="form-group">
    <label>Name</label>
    <x-form-elements.input name="name" :value="optional($vendor)->name" placeHolder="Vendor Name"
                           :required="true"/>
</div>