@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Vendors</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Editing</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card with-no-radius form-wrapper">
            <div class="card-header card-header-min">
                <h2>Edit {{$vendor->name}}</h2>
                <div class="card-header-actions">
                    <button class="btn btn-success" form="basic_form">
                        <i class="las la-plus"></i>
                    </button>
                    <a class="btn btn-warning" href="{{URL::previous()}}">
                        <i class="las la-arrow-circle-left"></i>
                    </a>
                </div>

            </div>
            <div class="card-body">

                <form method="post" id="basic_form" action="{{route('admin.vendors.update',['vendor'=>$vendor])}}">
                    @csrf
                    @method('PUT')
                    @include('admin.vendors.partial.form')
                </form>
            </div>
        </div>

    </section>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/panel/form.css')}}">
@endsection