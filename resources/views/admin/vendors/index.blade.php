@extends('admin.layouts.master')
@section('directory')
    <ul class="nav directory">
        <li class="nav-item">
            <a class="nav-link">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Vendors</a>
        </li>
        <li class="nav-item">
            <a class="nav-link">Index</a>
        </li>
    </ul>
@stop
@section('content')
    <section class="content">
        <div class="card data-table-wrapper">
            <div class="card-header card-header-min">
                <h2>All Vendors</h2>
                <a href="{{route('admin.vendors.create')}}" class="btn btn-warning btn-addable">
                    <i class="las la-plus"></i> new Vendor
                </a>

            </div>
            <div class="card-body">
                <form method="get" action="{{route('admin.vendors.index')}}" id="__filterForm">
                    <div class="search-form-wrapper">
                        <div class="search-form">
                            <div class="search-form-col">
                                <select name="limit" class="form-control">
                                    <option disabled selected>Select limit</option>
                                    @foreach(paginationLimit() as $limit)
                                        @php
                                            $selectedLimit=request()->has('limit')&& request()->get('limit')===$limit?'selected':'';
                                        @endphp
                                        <option value="{{$limit}}" {{$selectedLimit}}>{{$limit}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="search-form-col">
                                <input name="criteria" class="form-control" autocomplete="off" placeholder="Search For">
                            </div>
                            <div class="search-form-col">
                                <select class="form-control" name="status">
                                    <option disabled selected>Select Status</option>
                                    <option value="">All</option>
                                    @foreach(\App\Models\Vendor::statusOptions() as $key=>$value)
                                        <option value="{{$key}}">{{$value['status']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-success">Filter</button>
                            <button type="reset" class="btn btn-secondary" onclick="this.form.reset();">Reset</button>
                        </div>
                    </div>
                </form>
                <div class="result-count">
                    <i class="las la-user-circle"></i> {{$vendors->lastItem()}}/{{$vendors->total()}} Vendors
                </div>
                <table class="table table-striped table-min">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Publish date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($vendors as $vendor)
                        <tr>
                            <td>{{$vendor->id}}</td>
                            <td>{{$vendor->email}}</td>
                            <td>{{$vendor->name}}</td>
                            <td class="numbers">{{$vendor->created_at}}</td>
                            <td class="status">
                                <button class="btn {{$vendor->confirm['btn_color']}}">{{$vendor->confirm['status']}}</button>
                            </td>
                            <td class="actions">
                                <a href="{{route('admin.vendors.edit',compact('vendor'))}}" class="editable">
                                    <i class="las la-edit"></i>
                                </a>
                                <form action="{{route('admin.vendors.destroy',compact('vendor'))}}" method="post"
                                      id="delete_row">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="text-danger">
                                        <i class="las la-trash-restore"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="pagination-wrapper">
                    {{$vendors->withQueryString()->links()}}
                </div>
            </div>
        </div>
    </section>
@stop
@section('css')
    <link rel="stylesheet" href="{{asset('css/panel/table.min.css')}}">
@stop