@foreach($category->filters as $filter)
    @if($filter->accept_value)
        @php
            /** @var  \App\Models\Filter $filter */
            /** @var  \App\Models\Product $product */
                $inputName = "filters.{$filter->id}.value";
                $productFilters = optional($product->filters)->toarray();
                $attributes = !is_null($productFilters) ? $productFilters : [];
                $value = null;
                array_map(function ($attribute) use (&$value, $filter) {
                    if ($attribute['id'] == $filter->id) {
                        $value = $attribute['pivot']['value'];
                    }
                }, $attributes);
        @endphp
        <x-form-elements.inline-input
                :label="$filter->en_name"
                :name="$inputName"
                :required="(bool)$filter->is_required"
                :value="$value"
        />
    @else
        @php
            $currentFilterList=optional($product->filterItems)->pluck('id')->toArray();
        @endphp
        @if($filter->has_single_value)
            <x-form-elements.inline-select
                    :label="$filter->en_name"
                    name="filter_items."
                    :list="$filter->items"
                    :required="(bool)$filter->is_required"
                    :current="$currentFilterList"
            />
        @else
            <x-form-elements.inline-check-boxes
                    :label="$filter->en_name"
                    name="filter_items."
                    :list="$filter->items"
                    :required="(bool)$filter->is_required"
                    :current="$currentFilterList"
            />
        @endif
    @endif
@endforeach
