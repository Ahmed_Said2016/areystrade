@extends('design.layouts.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/welcome.css')}}">
@endpush
@push('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/scrollable.js')}}"></script>
@endpush
@section('content')
    <div class="banner">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item">
                    <img class="d-block w-100"
                         src="{{asset('images/UK_GRD_DesktopHero_1500x600_v1._CB483365719_.jpg')}}"
                         alt="First slide">
                </div>
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{asset('images/Ship45EN_1X._CB454091417_.jpg')}}"
                         alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100"
                         src="{{asset('images/m_generic_01-desktop_gw-d-uk-1500x600._CB481580320_.jpg')}}"
                         alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <i class="fas fa-chevron-left"></i>

            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <i class="fas fa-chevron-right"></i>

            </a>
        </div>
    </div>
    <div class="welcome-container">
        <div class="container">
            <div class="upper-wrapper">
                <div class="row">
                    <div class="col-md-4">
                        <div class="welcome-slogan">
                            <div class="welcome-slogan__part welcome-slogan__part--main">
                                <span>AREYS</span>
                                <span>LOGISTICS</span>
                            </div>
                            <div class="welcome-slogan__part welcome-slogan__part--icon">
                                <i class="las la-shipping-fast"></i>
                            </div>
                            <div class="welcome-slogan__part welcome-slogan__part--txt">
                                <h3>Fast & Reliable</h3>
                                <p>Shipping by Ocean or Air</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="welcome-slogan">
                            <div class="welcome-slogan__part welcome-slogan__part--main">
                                <span>TRADE</span>
                                <span>Assurance</span>
                            </div>
                            <div class="welcome-slogan__part welcome-slogan__part--icon">
                                <i class="las la-shield-alt"></i>
                            </div>
                            <div class="welcome-slogan__part welcome-slogan__part--txt">
                                <h3>Protect Your Orders</h3>
                                <p>when payment is made</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="welcome-slogan">
                            <div class="welcome-slogan__part welcome-slogan__part--main">
                                <span>SELL ON</span>
                                <span>AREYSTRADE</span>
                            </div>
                            <div class="welcome-slogan__part welcome-slogan__part--icon">
                                <i class="las la-plane"></i>
                            </div>
                            <div class="welcome-slogan__part welcome-slogan__part--txt">
                                <h3>Reach Many</h3>
                                <p>B2B Buyers Globally</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @foreach(__('test.welcome_top_categories') as $welcome_category)
                        <div class="col-md-4">
                            <div class="upper-item">
                                <a href="">
                                    <img class="welcome-upper-category__img" src="{{asset($welcome_category)}}" alt="#">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="welcome-section welcome-section--white">
                <div class="welcome-section__header">
                    <h1 class="welcome-section__title">New Products from Exhibitors</h1>
                    <span class="welcome-section__txt">From our exhibitors, selected by analysts</span>
                    <a href="#" class="welcome-section__link">
                        See All Categories <i class="las la-angle-double-right"></i>
                    </a>
                </div>
                <div class="welcome-section__body">
                    <div class="category-nav">
                        <div class="category-nav__header">
                            <ul class="category-nav__list">
                                @foreach(__('test.welcome_categories_nav') as $key=>$category)
                                    <li class="category-nav__item">
                                        <a href="#"
                                           class="category-nav__link {{$key===0?'category-nav__link--active':''}}">
                                            {!!  $category['icon']!!}
                                            <span>{{$category['category']}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="category-nav__body">
                            <h1 class="category-nav__title">
                                Auto Parts & Accessories
                                <a href="">
                                    See All Auto Parts & Accessories Products
                                    <i class="las la-long-arrow-alt-right"></i>
                                </a>
                            </h1>
                            <div class="row">
                                @foreach(__('test.test_category_products') as $product)
                                    <div class="col-md-3 category-product">
                                        <div class="category-product__wrapper">
                                            <div class="category-product__img">
                                                <img src="{{asset($product['image'])}}"
                                                     alt="#">
                                            </div>
                                            <div class="category-product__txt">
                                                <h1 class="category-product__title">
                                                    {{$product['title']}}
                                                </h1>
                                                <div class="category-product__price">
                                                    US$ {{$product['price']}}<span>/ {{$product['unit']}}</span>
                                                </div>
                                                <span class="category-product__quantity">
                                                    {{$product['min_order']}} {{$product['unit']}} (Min. Order)
                                                </span>
                                                <div class="category-product__action">
                                                    <a href="#"
                                                       class="category-product__btn category-product__btn--primary">
                                                        <i class="las la-mail-bulk"></i> <span>Inquiry Now</span>
                                                    </a>
                                                    <a href="#"
                                                       class="category-product__btn category-product__btn--secondary">
                                                        <i class="las la-sms"></i>
                                                    </a>
                                                    <a href="#"
                                                       class="category-product__btn category-product__btn--secondary">
                                                        <i class="lar la-bookmark"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="welcome-section welcome-section--white">
                <div class="welcome-section__header">
                    <h1 class="welcome-section__title">New Trending</h1>
                </div>
                <div class="welcome-section__body">
                    <div class="scrollable">
                        <a href=""
                           class="scrollable__control scrollable__control--prev"
                           id="__previous">
                            <i class="las la-angle-left"></i>
                        </a>
                        <a href="#"
                           class="scrollable__control scrollable__control--next scrollable__control--active"
                           id="__next">
                            <i class="las la-angle-right"></i>
                        </a>
                        <div class="scrollable__container">
                            <div class="scrollable__wrapper">
                                @for($i=1;$i<=3;$i++)
                                    @foreach(__('test.test_products') as $product)
                                        <div class="scrollable__item">
                                            <div class="product-item">
                                                <div class="product-item__image">
                                                    <img class="lazy"
                                                         src="{{asset('images/loading.gif')}}"
                                                         data-src="{{asset($product['image'])}}"
                                                         alt="Small Shoulder Leather Bag - Burgundy">
                                                </div>
                                                <div class="product-item__txt">
                                                    <h2 class="product-item__title">{{$product['title']}}</h2>
                                                    <div class="product-item__price">
                                                        from USD<span>{{$product['price']}}</span>/{{$product['unit']}}
                                                    </div>
                                                    <div class="product-item__quantity">
                                                        Min.Orders<span>({{$product['min_order']}})</span>{{$product['unit']}}
                                                    </div>

                                                    <button class="product-item__btn"></button>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endfor
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="rfq">
                <div class="rfq__txt">
                    <h1>Can't find the product that meets your need exactly?</h1>
                    <h2>Submit a Buying Request to get targeted quotations from verified suppliers.</h2>
                </div>
                <div class="rfq__form">
                    <h3 class="rfq__title">Get Quotations Now</h3>
                    <div class="rfq__row">
                        <input class="form-control" name="product_name" placeholder="Product Name or Keywords"
                               aria-label="Product Name"
                               autocomplete="off">
                    </div>
                    <div class="rfq__row">
                            <textarea class="form-control" name="product_description" aria-label="Product Description"
                                      placeholder="Product Description"></textarea>
                    </div>
                    <div class="rfq__row">
                        <div class="rfq__col">
                            <input class="form-control" name="product_quantity" placeholder="Purchase Quantity"
                                   aria-label="Product Quantity"
                                   autocomplete="off">
                        </div>
                        <div class="rfq__col">
                            <select class="custom-select" name="product_unit" aria-label="Product Unit">
                                @foreach(__('products.units') as $unit)
                                    <option value="{{$unit}}">{{$unit}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button class="rfq__btn"><i class="las la-paper-plane"></i> Request for Quotation Now</button>
                </div>
            </div>
            <div class="solutions">
                <h1 class="solutions__title">Sourcing Solutions & Tailored Services </h1>
                <div class="solutions__container">
                    <div class="solutions__wrapper">
                        <div class="solutions__item">
                            <div class="solutions__text">
                                <h3>Source form Industry Hub</h3>
                                <ul>
                                    <li>Manufacturing Base</li>
                                    <li>Industry Competitiveness</li>
                                    <li>Original Products</li>
                                </ul>
                            </div>
                            <div class="solutions__mask">
                                <img src="{{asset('images/__design/solutions-pic1.jpg')}}" alt="#">
                            </div>
                        </div>
                    </div>
                    <div class="solutions__wrapper">
                        <div class="solutions__item">
                            <div class="solutions__text">
                                <h3>MEI Awards-Winning Products</h3>
                                <ul>
                                    <li>Manufacturing Excellence</li>
                                    <li>Innovative Design</li>
                                    <li>Awarded by Industry Experts</li>
                                </ul>
                            </div>
                            <div class="solutions__mask">
                                <img src="{{asset('images/__design/solutions-pic2.jpg')}}" alt="#">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection