<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link href="https://unpkg.com/ionicons@4.5.5/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.alternative.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/welcome.css')}}">
</head>
<body>
<div class="wrapper" id="__wrapper">
    <div class="wrapper__sidebar">
        <div class="sidebar">
            <ul class="sidebar__list">
                @foreach(__('test.categories') as $category)
                    <li class="sidebar__item">
                        <a href="javascript:void(0)" class="sidebar__link">
                            {{$category}}
                            <div class="sidebar__link__arrow">
                                <i class="las la-angle-down"></i>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

    </div>
    <div class="wrapper__container">
        <div class="c-header" id="__header_nav">
            <div class="c-header__topnav">
                <div class="container">
                    <div class="l-toplist">
                        <ul class="l-toplist__list">
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-shipping-fast"></i> <span>Ready to Ship</span></a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="lar la-handshake"></i> Trade Insurance</a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-lightbulb"></i> Sourcing Solutions</a>
                            </li>
                        </ul>
                        <ul class="l-toplist__list l-toplist__list--right">
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-user"></i> Sign in | Join Free</a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-shopping-bag"></i> Orders</a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-shopping-cart"></i> Cart</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="c-header__mainnav">
                <div class="container">
                    <div class="middlenav">
                        <div class="middlenav__logo">
                            <a href="">
                                <img src="{{asset('images/logo-test.png')}}" alt="Areys Trade">
                            </a>
                        </div>
                        <div class="middlenav__search">
                            <div class="search">
                                <div class="search__box">
                                    <select class="search__element search__element--selector custom-select" name="type"
                                            aria-label="search type">
                                        <option value="products" selected>Products</option>
                                        <option value="suppliers">Suppliers</option>
                                    </select>
                                    <input class="search__element search__element--input"
                                           placeholder="What your are looking for" aria-label="search">
                                    <button class="search__element search__element--btn">
                                        <i class="las la-search"></i> Search
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="middlenav__links">
                            <ul class="middlenav__list">
                                <li class="middlenav__item">
                                    <a href=""><i class="las la-coins"></i> USD</a>
                                </li>
                                <li class="middlenav__item middlenav__item--arabic">
                                    <a href=""><i class="las la-globe-africa"></i> العربية</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-header__linknav">
                <div class="container">
                    <div class="nav">
                        <ul class="nav__list">
                            <li class="nav__item">
                                <a href="javascript:void(0)">All <span>Categories</span>
                                    <div class="hamburger" id="__show_side_menu">
                                        <div class="hamburger__line"></div>
                                    </div>
                                </a>
                            </li>
                            @foreach(__('test.nav') as $key=>$item)
                                <li class="nav__item">
                                    <a href="javascript:void(0)">{{$item}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="images/UK_GRD_DesktopHero_1500x600_v1._CB483365719_.jpg"
                             alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/Ship45EN_1X._CB454091417_.jpg" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="images/m_generic_01-desktop_gw-d-uk-1500x600._CB481580320_.jpg"
                             alt="Third slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <i class="fas fa-chevron-left"></i>

                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <i class="fas fa-chevron-right"></i>

                </a>
            </div>
        </div>
        <div class="welcome-container">
            <div class="container">
                <div class="upper-wrapper">
                    <div class="row">
                        <div class="col-md-4">



                            <div class="welcome-upper-ref">
                                <div class="ref-part">
                                    <div class="ref-part-title">
                                        <span>AREYS</span>
                                        <span>LOGISTICS</span>
                                    </div>
                                    <div class="ref-part-icon">
                                        <i class="fas fa-truck"></i>
                                    </div>
                                </div>
                                <div class="ref-part">
                                    <h3>FAST & RELIABLE</h3>
                                    <p>Shipping by Ocean or Air</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="welcome-upper-ref">
                                <div class="ref-part">
                                    <div class="ref-part-title">
                                        <span>Trade</span>
                                        <span>Assurance</span>
                                    </div>
                                </div>
                                <div class="ref-part">
                                    <h3>Protect Your Orders</h3>
                                    <h3>when payment is made through AreysTrade.com</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="welcome-upper-ref">
                                <div class="ref-part">
                                    <div class="ref-part-title">
                                        <span>SELL ON</span>
                                        <span>AREYSTRADE.com</span>
                                    </div>
                                </div>
                                <div class="ref-part">
                                    <h3>Reach many B2B Buyers</h3>
                                    <h3>Globaly</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="welcome-category welcome-category--orange">
                                <div class="welcome-category__img">
                                    <img src="{{asset('images/test/royalty-line-pkm-19007-robot-de-cuisine-1900w-pkm-19007--2.png')}}"
                                         alt="category_1">
                                </div>
                                <div class="welcome-category__txt">
                                    <h3>Kitchenware</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="welcome-category welcome-category--orange">
                                <div class="welcome-category__img">
                                    <img src="{{asset('images/test/1-18841_001-samsung-d8000-side1-televisor-smart-tv-png.png')}}"
                                         alt="category_1">
                                </div>
                                <div class="welcome-category__txt">
                                    <h3>Smart TV</h3>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-3">
                            <div class="welcome-category welcome-category--orange">
                                <div class="welcome-category__img">
                                    <img src="{{asset('images/test/gym_equipment_PNG160.png')}}"
                                         alt="category_1">
                                </div>
                                <div class="welcome-category__txt">
                                    <h3>Gym Equipment</h3>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="product-list">
                        <h1>Recommended For You</h1>
                        <div class="products-list-wrapper">
                            <span class="list-control-prev unavailable">
                                <i class="fas fa-chevron-left"></i>
                            </span>
                            <span class="list-control-next">
                                <i class="fas fa-chevron-right"></i>
                            </span>
                            <div class="product-list-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20women%20handbag/1"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/a5b510b8d6f66c15f3d03c80a918d95f.png"
                                                         alt="leather women handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather women handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/black%20leather%20women%20handbag/2"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9f13934abc74ea66bb5104ec9efca409.png"
                                                         alt="black leather women handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>black leather women handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/women%20leather%20black%20handbag/4"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/00b728b3c2f2a4b1774ce457c53c442f.png"
                                                         alt="women leather black handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>women leather black handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20tiger%20print%20women%20handbag%20with%20metal%20chain/5"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/25c3f0c76eae90e4d93f63a2a47cff45.png"
                                                         alt="leather tiger print women handbag with metal chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather tiger print women handbag with met...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Leather%20chocolate%20brown%20handbag%20for%20women%20with%20silver%20chain/6"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e90bddb8df93acbdf5915894ee63a1e9.png"
                                                         alt="Leather chocolate brown handbag for women with silver chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Leather chocolate brown handbag for women...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Michael%20Korse%20blue-printed%20handbag%20for%20women/7"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/d17cc699b6a3f840f9dc1409d70aaee8.png"
                                                         alt="Michael Korse blue-printed handbag for women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Michael Korse blue-printed handbag for wom...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Fashionable%20brown%20leather%20backpack/10"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/28cbcf73f4161d84c2dda4345175558f.png"
                                                         alt="Fashionable brown leather backpack">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Fashionable brown leather backpack</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Michael%20Korse%20leather%20white%20handbag/11"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9d5a2d820dddb1932bce826bd910fb3d.png"
                                                         alt="Michael Korse leather white handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Michael Korse leather white handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Elegant%20multi-color%20leather%20handbag%20with%20golden%20metal%20chain/12"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e98048c93536f747aa0277118f300fa7.png"
                                                         alt="Elegant multi-color leather handbag with golden metal chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Elegant multi-color leather handbag with g...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Stylish%20and%20practical%20cross-body%20black%20bag/13"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/20d14773db65fc6b8988783c7128483a.png"
                                                         alt="Stylish and practical cross-body black bag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Stylish and practical cross-body black bag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Stylish%20backpack%20with%20map-print/15"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/2ed640a96cfd05b3f8c61253d7a07f91.png"
                                                         alt="Stylish backpack with map-print">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Stylish backpack with map-print</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20handbag%20coral%20color/16"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/686aa048c93e0e16776d3e86003502a3.png"
                                                         alt="leather handbag coral color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather handbag coral color</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20handbag%20with%20unique%20goldenrod%20color/17"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e1926fb4a42d91f4847703fc0801e8ed.png"
                                                         alt="leather handbag with unique goldenrod color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather handbag with unique goldenrod colo...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Black%20Leather%20Handbag/19"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/01f322878bf0ac253b1fe60c3a58e9a5.jpg"
                                                         alt="Black Leather Handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Black Leather Handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Medium%20Size%20Leather%20Backpack/22"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/a76c8ef78d6c0ea41e75e291b7c1133e.jpg"
                                                         alt="Medium Size Leather Backpack">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Medium Size Leather Backpack</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Leather%20Handbag%20black%20for%20Women/26"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/4da0f2dd72b17fe74eecd2e9211ea497.jpg"
                                                         alt="Leather Handbag black for Women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Leather Handbag black for Women</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Stylish%20Backpack%20Brown/29"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/a51c96f7ca249b986d20bfe5eb7c862b.jpg"
                                                         alt="Stylish Backpack Brown">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Stylish Backpack Brown</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Leather%20Laptop%20Bag%20Chocolate%20Brown/31"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/2242ab9f0cc1c4fd21a1c5a426ef6baf.jpg"
                                                         alt="Leather Laptop Bag Chocolate Brown">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Leather Laptop Bag Chocolate Brown</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Mini%20Round%20Shoulder%20Bag/35"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/edffb687aae59065e501d0f71941b829.jpg"
                                                         alt="Mini Round Shoulder Bag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Mini Round Shoulder Bag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Gucci%20Marmont%20matelass%C3%A9%20shoulder%20bag%20%E2%80%93%20Navy%20Blue/37"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/69837d9b1378757b42cf76c9b5bb968e.jpg"
                                                         alt="Gucci Marmont matelassé shoulder bag – Navy Blue">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Gucci Marmont matelassé shoulder bag – Nav...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Gucci%20Marmont%20mini%20round%20shoulder%20bag%20%E2%80%93%20Hibiscus%20Red/44"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/2a44fc7fca9ca7702127dc9304c92421.png"
                                                         alt="Gucci Marmont mini round shoulder bag – Hibiscus Red">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Gucci Marmont mini round shoulder bag – Hi...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Mini%20Shoulder%20Leather%20Bag%20-%20Black/47"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/7598f9bf3c4d61a71909d44fdf000951.png"
                                                         alt="Mini Shoulder Leather Bag - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Mini Shoulder Leather Bag - Black</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Embroidered%20Flowers%20Velour%20Cardigan%20-%20Wine/51"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/6883555b08c2f1abd669a4417899dd71.jpg"
                                                         alt="Dalydress Embroidered Flowers Velour Cardigan - Wine">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Embroidered Flowers Velour Cardi...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Glittery%20Long%20Sleeves%20Ribbed%20Cardigan%20-%20Grey/54"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/52c7a978208d174941b35343405b1316.jpg"
                                                         alt="Dalydress Glittery Long Sleeves Ribbed Cardigan - Grey">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Glittery Long Sleeves Ribbed Car...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Ravin%20Solid%20Basic%20V-Neck%20Long%20Sleeves%20Pullover%20-%20Heather%20Navy%20Blue/55"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/48ccb8629a4a74016e94787219ecbf3d.jpg"
                                                         alt="Ravin Solid Basic V-Neck Long Sleeves Pullover - Heather Navy Blue">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Ravin Solid Basic V-Neck Long Sleeves Pull...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Ravin%20Bi-Tone%20Men%27s%20Long%20Sleeves%20Pullover/59"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/89512f5c14acab722c26796c1b8694c3.jpg"
                                                         alt="Ravin Bi-Tone Men&#039;s Long Sleeves Pullover">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Ravin Bi-Tone Men&#039;s Long Sleeves Pullover</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/White%20T-Shirt%20-i%27m%20not%20weird%20i%27m%20limited%20edition/60"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/90bd1e69e815529bb8c9754d4600f171.jpg"
                                                         alt="White T-Shirt -i&#039;m not weird i&#039;m limited edition">
                                                </div>
                                                <div class="product-text">

                                                    <h2>White T-Shirt -i&#039;m not weird i&#039;m limited e...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Cool%20Guy%20T-Shirt%20%E2%80%93%20white%20Color%20for%20Men/64"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/6b8f268c68ab4d00083827a1f91f2ba9.jpg"
                                                         alt="Cool Guy T-Shirt – white Color for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Cool Guy T-Shirt – white Color for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Cool%20Guy%20White%20T-Shirt%20for%20Men/66"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/fef1a07653257651189cdd72b870dee8.jpg"
                                                         alt="Cool Guy White T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Cool Guy White T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Joker%20Face%20White%20T-Shirt%20for%20Men/70"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9dfe81fb638e5562d7cee5c1d8b9b171.jpg"
                                                         alt="Joker Face White T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Joker Face White T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Malcolm%20X%20-%20Black%20T-Shirt%20for%20Men/72"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/c260eb7e44f2a1c7dc862300b2c75cd5.jpg"
                                                         alt="Malcolm X - Black T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Malcolm X - Black T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Need%20for%20Speed%20White%20T-Shirt%20for%20Men/73"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/8117d7e18002b2a51dd1212e667851da.jpg"
                                                         alt="Need for Speed White T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Need for Speed White T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Ride%20to%20Live%20-%20Black%20T-Shirt%20for%20Men/75"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/4d7bf5dd12d7c8c984df6dcc9b5673f7.jpg"
                                                         alt="Ride to Live - Black T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Ride to Live - Black T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Ride%20to%20Live%20-%20White%20T-Shirt%20for%20Men/76"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/eca82477b36081a98bd9a488280642ff.jpg"
                                                         alt="Ride to Live - White T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Ride to Live - White T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Asian%20Fusion%20Pants%20%E2%80%93%20Grey%20Color%20-%20Women/78"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/540505cc79aa64ae3b94b663e282a176.jpg"
                                                         alt="Dalydress Asian Fusion Pants – Grey Color - Women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Asian Fusion Pants – Grey Color...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Floral%20With%20Middle%20Buttons%20Cardigan%20%E2%80%93%20Multi-Color/81"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/40895011a6fc6feac73d58eb31737909.jpg"
                                                         alt="Dalydress Floral With Middle Buttons Cardigan – Multi-Color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Floral With Middle Buttons Cardi...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Two%20Buttons%20Classic%20Blazer%20-%20Navy%20Blue%20-%20Men/82"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/0201e380f46f42bf7f77d95b5fbdd9c5.jpg"
                                                         alt="Dalydress Two Buttons Classic Blazer - Navy Blue - Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Two Buttons Classic Blazer - Nav...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Zipper%20Sleeveless%20Vest%20-%20Black/85"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e5d391a9a618b750629c5b9cd9d02693.jpg"
                                                         alt="Premoda Zipper Sleeveless Vest - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Zipper Sleeveless Vest - Black</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Basic%20Pants%20%E2%80%93%20Camel%20Color%20-%20Women/87"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/0ec2f59b614d2770642c814160d842f6.jpg"
                                                         alt="Dalydress Basic Pants – Camel Color - Women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Basic Pants – Camel Color - Wome...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Patterned%20Fringed%20Slip%20On%20Jacket/89"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/3f560705ca2707e4d12a2ed72c317676.jpg"
                                                         alt="Premoda Patterned Fringed Slip On Jacket">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Patterned Fringed Slip On Jacket</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Suede%20Tassels%20Clip%20Toe%20Sandals%20-%20Beige/91"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/1d7aa0f3aff2da0d66477c61b776ce16.jpg"
                                                         alt="Dalydress Suede Tassels Clip Toe Sandals - Beige">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Suede Tassels Clip Toe Sandals -...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Decorative%20Tassels%20&amp;%20Beads%20Flat%20Sandals%20-%20Off-White/93"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/7fdd0317db9975a6ed3b1e5aba088431.jpg"
                                                         alt="Dalydress Decorative Tassels &amp; Beads Flat Sandals - Off-White">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Decorative Tassels &amp; Beads Flat...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Rubber%20Summer%20Slipper%20With%20Flower%20-%20Black/95"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/97536ee8dbd8d8e4445c1b56dfb1d591.jpg"
                                                         alt="Dalydress Rubber Summer Slipper With Flower - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Rubber Summer Slipper With Flowe...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Simple%20Lace%20Up%20Casual%20Sneakers%20-%20Black/97"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/37066e819717d01559b247b9466b82b9.jpg"
                                                         alt="Premoda Simple Lace Up Casual Sneakers - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Simple Lace Up Casual Sneakers - B...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="info-holder">
                        <img src="images/Free-Shipping-en.png" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="category-holder">
                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/c/Home%20Essentials/4">
                            <img src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/categories/43f22c2d2a0b5382533c814a5f15d3b5.png"
                                 alt="Home Essentials">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="category-holder">
                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/c/Ladies%20Fashion/5">
                            <img src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/categories/0cd770dafd867319c0f3464430618152.png"
                                 alt="Ladies Fashion">
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="product-list">
                        <h1>Most popular</h1>
                        <div class="products-list-wrapper">
                            <span class="list-control-prev unavailable">
                                <i class="fas fa-chevron-left"></i>
                            </span>
                            <span class="list-control-next">
                                <i class="fas fa-chevron-right"></i>
                            </span>
                            <div class="product-list-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20women%20handbag/1"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/a5b510b8d6f66c15f3d03c80a918d95f.png"
                                                         alt="leather women handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather women handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/black%20leather%20women%20handbag/2"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9f13934abc74ea66bb5104ec9efca409.png"
                                                         alt="black leather women handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>black leather women handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/women%20leather%20black%20handbag/4"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/00b728b3c2f2a4b1774ce457c53c442f.png"
                                                         alt="women leather black handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>women leather black handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20tiger%20print%20women%20handbag%20with%20metal%20chain/5"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/25c3f0c76eae90e4d93f63a2a47cff45.png"
                                                         alt="leather tiger print women handbag with metal chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather tiger print women handbag with met...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Leather%20chocolate%20brown%20handbag%20for%20women%20with%20silver%20chain/6"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e90bddb8df93acbdf5915894ee63a1e9.png"
                                                         alt="Leather chocolate brown handbag for women with silver chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Leather chocolate brown handbag for women...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Michael%20Korse%20blue-printed%20handbag%20for%20women/7"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/d17cc699b6a3f840f9dc1409d70aaee8.png"
                                                         alt="Michael Korse blue-printed handbag for women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Michael Korse blue-printed handbag for wom...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Fashionable%20brown%20leather%20backpack/10"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/28cbcf73f4161d84c2dda4345175558f.png"
                                                         alt="Fashionable brown leather backpack">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Fashionable brown leather backpack</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Michael%20Korse%20leather%20white%20handbag/11"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9d5a2d820dddb1932bce826bd910fb3d.png"
                                                         alt="Michael Korse leather white handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Michael Korse leather white handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Elegant%20multi-color%20leather%20handbag%20with%20golden%20metal%20chain/12"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e98048c93536f747aa0277118f300fa7.png"
                                                         alt="Elegant multi-color leather handbag with golden metal chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Elegant multi-color leather handbag with g...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Stylish%20and%20practical%20cross-body%20black%20bag/13"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/20d14773db65fc6b8988783c7128483a.png"
                                                         alt="Stylish and practical cross-body black bag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Stylish and practical cross-body black bag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Stylish%20backpack%20with%20map-print/15"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/2ed640a96cfd05b3f8c61253d7a07f91.png"
                                                         alt="Stylish backpack with map-print">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Stylish backpack with map-print</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20handbag%20coral%20color/16"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/686aa048c93e0e16776d3e86003502a3.png"
                                                         alt="leather handbag coral color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather handbag coral color</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20handbag%20with%20unique%20goldenrod%20color/17"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e1926fb4a42d91f4847703fc0801e8ed.png"
                                                         alt="leather handbag with unique goldenrod color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather handbag with unique goldenrod colo...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Medium%20Size%20Backpack/20"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/d477a8d6cefc8822fc88c0f1ede2ddbd.jpg"
                                                         alt="Medium Size Backpack">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Medium Size Backpack</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Shouler%20Bag%20for%20Women/23"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/ee2ba68f606fb48a7f49401c7013d1da.jpg"
                                                         alt="Shouler Bag for Women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Shouler Bag for Women</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Backpack%20Medium%20Size/27"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/f1bb5ee42424c707532c619f0937a00c.jpg"
                                                         alt="Backpack Medium Size">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Backpack Medium Size</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Goldenrod%20Leather%20Handbag/30"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/dafbe7942416f924ea3124c2de19be6b.jpg"
                                                         alt="Goldenrod Leather Handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Goldenrod Leather Handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Black%20Leather%20Shoulder%20bag/32"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/b78989364519751bb5ba6125fc62239b.jpg"
                                                         alt="Black Leather Shoulder bag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Black Leather Shoulder bag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Set%20of%204%20Wallets%20Brown/34"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/8d50050785ab758c7ccda1695e7f0d14.jpg"
                                                         alt="Set of 4 Wallets Brown">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Set of 4 Wallets Brown</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Mini%20Leather%20Shoulder%20Bag-Blue/36"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/19a25f69b3719af0e3b75cb5829f249e.jpg"
                                                         alt="Mini Leather Shoulder Bag-Blue">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Mini Leather Shoulder Bag-Blue</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Gucci%20Marmont%20mini%20top%20handle%20bag%20%E2%80%93%20Navy%20Blue/38"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/475d161210d72d8f37c969c79c89b0c8.jpg"
                                                         alt="Gucci Marmont mini top handle bag – Navy Blue">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Gucci Marmont mini top handle bag – Navy B...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Gucci%20Mini%20Shoulder%20Leather%20Bag%20-%20Black/45"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/6387454d0a04a728882602f81940d8ba.png"
                                                         alt="Gucci Mini Shoulder Leather Bag - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Gucci Mini Shoulder Leather Bag - Black</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Small%20Shoulder%20Leather%20Bag%20-%20Black/48"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/6f3d0726b06e1dd55a8e1c57a27b1dbd.png"
                                                         alt="Small Shoulder Leather Bag - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Small Shoulder Leather Bag - Black</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Small%20Shoulder%20Leather%20Bag%20-%20Burgundy/49"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/d2fa4646c3a6250e27dac5dca4db5006.png"
                                                         alt="Small Shoulder Leather Bag - Burgundy">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Small Shoulder Leather Bag - Burgundy</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Golden%20Decorative%20Buttons%20Basic%20Pullover%20-%20Heather%20Dark%20Grey/52"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/dcc50a03f667f5b88070e62235af2815.jpg"
                                                         alt="Dalydress Golden Decorative Buttons Basic Pullover - Heather Dark Grey">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Golden Decorative Buttons Basic...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Ravin%20Striped%20Slip%20On%20Pullover%20-%20Olive/56"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/30c9d823ae3be32464ff31cbc147dfe4.jpg"
                                                         alt="Ravin Striped Slip On Pullover - Olive">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Ravin Striped Slip On Pullover - Olive</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Ravin%20Patterned%20V-Neck%20Casual%20Pullover%20-%20Burgundy/57"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/4bc484e1d0cac3b948daa7cc1abc7588.jpg"
                                                         alt="Ravin Patterned V-Neck Casual Pullover - Burgundy">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Ravin Patterned V-Neck Casual Pullover - B...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Attention!%20White%20T-Shirt%20for%20Men/61"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/37a1ec4708c55702aa5fb5c7bbd61ce9.jpg"
                                                         alt="Attention! White T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Attention! White T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Call%20of%20Duty%20White%20T-Shirt%20for%20Men/62"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e524e3cf35e9cbf53f9e0ce703ec30d4.jpg"
                                                         alt="Call of Duty White T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Call of Duty White T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Cool%20Guy%20T-Shirt%20%E2%80%93%20Heather%20Grey%20Color%20for%20Men/63"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/c79b58b5fe6e4111f8abc05c9fa80d08.jpg"
                                                         alt="Cool Guy T-Shirt – Heather Grey Color for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Cool Guy T-Shirt – Heather Grey Color for...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Counter%20Strike%20black%20T-Shirt%20for%20Men/67"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/0e1000790c75d48d891960bb18b5ef69.jpg"
                                                         alt="Counter Strike black T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Counter Strike black T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Hell%20Riders%20-%20T-Shirt%20%E2%80%93%20white%20Color%20for%20Men/68"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/ca2f7db4cf29fd27bf28863759a39f5b.jpg"
                                                         alt="Hell Riders - T-Shirt – white Color for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Hell Riders - T-Shirt – white Color for Me...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Joker%20Smile%20White%20T-Shirt%20for%20Men/71"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/c88348fb555ac66afc3592f84dbe1c9c.jpg"
                                                         alt="Joker Smile White T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Joker Smile White T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Respect%20for%20Riders%20T-Shirt%20%E2%80%93%20Heather%20Grey%20Color%20for%20Men/74"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/bcf212d329eebcd8e68a91b5a29035ed.jpg"
                                                         alt="Respect for Riders T-Shirt – Heather Grey Color for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Respect for Riders T-Shirt – Heather Grey...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20%E2%80%93%20Retro%20Vibes%20Skirt%20%E2%80%93%20Brown%20Color%20-%20Women/79"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/223f62262b501613d5f279bc1fc4c31a.jpg"
                                                         alt="Dalydress – Retro Vibes Skirt – Brown Color - Women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress – Retro Vibes Skirt – Brown Colo...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Rubi%20Short%20Cardigan%20%E2%80%93%20Red%20-%20Women/80"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/c20e7dd052239797f5d634b60105424c.jpg"
                                                         alt="Dalydress Rubi Short Cardigan – Red - Women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Rubi Short Cardigan – Red - Wome...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Waterproof%20Sleeveless%20Vest%20-%20Burgundy/83"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/473fafb9c554986d12eac4afcc9c5a30.jpg"
                                                         alt="Premoda Waterproof Sleeveless Vest - Burgundy">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Waterproof Sleeveless Vest - Burgu...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Elastic%20Waist%20Comfy%20Sportive%20Pants%20-%20Steel%20Blue/86"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/31277c29a22924211fe036f3cf684b7f.jpg"
                                                         alt="Premoda Elastic Waist Comfy Sportive Pants - Steel Blue">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Elastic Waist Comfy Sportive Pants...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Embroidery%20Black%20Zipped%20Jacket%20With%20Two%20Pockets/88"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/94a0e915c66b001fdd91179cea32b721.jpg"
                                                         alt="Premoda Embroidery Black Zipped Jacket With Two Pockets">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Embroidery Black Zipped Jacket Wit...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Lace%20Up%20Open%20Toe%20Mid%20Heel%20Sandals%20-%20Blue%20Jeans/92"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/6556348cc950cd0713a61515e8c8ad49.jpg"
                                                         alt="Dalydress Lace Up Open Toe Mid Heel Sandals - Blue Jeans">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Lace Up Open Toe Mid Heel Sandal...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Rubber%20Thong%20Summer%20Flat%20Sandals%20-%20Black/94"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/03ac471901f66b9a3b86505b04913f4f.jpg"
                                                         alt="Dalydress Rubber Thong Summer Flat Sandals - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Rubber Thong Summer Flat Sandals...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Rubber%20Thong%20Summer%20Flat%20Sandals%20-%20White/96"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/ad964ba250e2d289180dac04f55fe29c.jpg"
                                                         alt="Dalydress Rubber Thong Summer Flat Sandals - White">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Rubber Thong Summer Flat Sandals...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Solid%20Leather%20Slip%20On%20Casual%20Loafers%20-%20Navy%20Blue/98"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9cb6c4ff5edb1424de5412a121cead97.jpg"
                                                         alt="Premoda Solid Leather Slip On Casual Loafers - Navy Blue">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Solid Leather Slip On Casual Loafe...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Denim%20Round%20Toe%20Lace%20Up%20Casual%20Shoes%20-%20Navy%20Blue%20Jeans/100"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/227363d8a32b1360f5c97fb4b50fc022.jpg"
                                                         alt="Premoda Denim Round Toe Lace Up Casual Shoes - Navy Blue Jeans">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Denim Round Toe Lace Up Casual Sho...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Denim%20Lace%20Up%20Casual%20Shoes%20-%20Blue%20Jeans/101"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e3f8293831642f67ff36c03672f33a0e.jpg"
                                                         alt="Premoda Denim Lace Up Casual Shoes - Blue Jeans">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Denim Lace Up Casual Shoes - Blue...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="product-list">
                        <h1>Top picks in souvenir</h1>
                        <div class="products-list-wrapper">
                            <span class="list-control-prev unavailable">
                                <i class="fas fa-chevron-left"></i>
                            </span>
                            <span class="list-control-next">
                                <i class="fas fa-chevron-right"></i>
                            </span>
                            <div class="product-list-row">
                                <div class="row">
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20women%20handbag/1"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/a5b510b8d6f66c15f3d03c80a918d95f.png"
                                                         alt="leather women handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather women handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/black%20leather%20women%20handbag/2"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9f13934abc74ea66bb5104ec9efca409.png"
                                                         alt="black leather women handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>black leather women handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/women%20leather%20black%20handbag/4"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/00b728b3c2f2a4b1774ce457c53c442f.png"
                                                         alt="women leather black handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>women leather black handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20tiger%20print%20women%20handbag%20with%20metal%20chain/5"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/25c3f0c76eae90e4d93f63a2a47cff45.png"
                                                         alt="leather tiger print women handbag with metal chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather tiger print women handbag with met...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Leather%20chocolate%20brown%20handbag%20for%20women%20with%20silver%20chain/6"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e90bddb8df93acbdf5915894ee63a1e9.png"
                                                         alt="Leather chocolate brown handbag for women with silver chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Leather chocolate brown handbag for women...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Michael%20Korse%20blue-printed%20handbag%20for%20women/7"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/d17cc699b6a3f840f9dc1409d70aaee8.png"
                                                         alt="Michael Korse blue-printed handbag for women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Michael Korse blue-printed handbag for wom...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Fashionable%20brown%20leather%20backpack/10"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/28cbcf73f4161d84c2dda4345175558f.png"
                                                         alt="Fashionable brown leather backpack">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Fashionable brown leather backpack</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Michael%20Korse%20leather%20white%20handbag/11"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/9d5a2d820dddb1932bce826bd910fb3d.png"
                                                         alt="Michael Korse leather white handbag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Michael Korse leather white handbag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Elegant%20multi-color%20leather%20handbag%20with%20golden%20metal%20chain/12"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e98048c93536f747aa0277118f300fa7.png"
                                                         alt="Elegant multi-color leather handbag with golden metal chain">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Elegant multi-color leather handbag with g...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Stylish%20and%20practical%20cross-body%20black%20bag/13"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/20d14773db65fc6b8988783c7128483a.png"
                                                         alt="Stylish and practical cross-body black bag">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Stylish and practical cross-body black bag</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Stylish%20backpack%20with%20map-print/15"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/2ed640a96cfd05b3f8c61253d7a07f91.png"
                                                         alt="Stylish backpack with map-print">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Stylish backpack with map-print</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20handbag%20coral%20color/16"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/686aa048c93e0e16776d3e86003502a3.png"
                                                         alt="leather handbag coral color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather handbag coral color</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/leather%20handbag%20with%20unique%20goldenrod%20color/17"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e1926fb4a42d91f4847703fc0801e8ed.png"
                                                         alt="leather handbag with unique goldenrod color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>leather handbag with unique goldenrod colo...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Black%20Leather%20Handbag%20for%20Women/21"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/e5cfdc9ac667e91fd95eb7b5f7c0130d.jpg"
                                                         alt="Black Leather Handbag for Women">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Black Leather Handbag for Women</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Leather%20Shoulder%20Bag%20Chocolate%20Brown/24"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/2e022613ff2d2a31c7bc181575a16499.jpg"
                                                         alt="Leather Shoulder Bag Chocolate Brown">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Leather Shoulder Bag Chocolate Brown</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Shoulder%20Bag%20Multi-color/33"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/ba06adfcdf30be59053dca570e97a4b5.jpg"
                                                         alt="Shoulder Bag Multi-color">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Shoulder Bag Multi-color</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Gucci%20Marmont%20mini%20shoulder%20bag-%20Red/39"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/c9b6583483e3cf8d3d1b6957557e3051.jpg"
                                                         alt="Gucci Marmont mini shoulder bag- Red">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Gucci Marmont mini shoulder bag- Red</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Small%20Shoulder%20Leather%20Bag%20%E2%80%93%20Dusty%20Pink/46"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/f86f291085caf750c5f293eafc7a68b1.png"
                                                         alt="Small Shoulder Leather Bag – Dusty Pink">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Small Shoulder Leather Bag – Dusty Pink</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Gucci%20Ophidia%20small%20shoulder%20bag%E2%80%93%20Hibiscus%20Red/50"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/d31b856746c03d1990044b3a7e71d0a2.png"
                                                         alt="Gucci Ophidia small shoulder bag– Hibiscus Red">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Gucci Ophidia small shoulder bag– Hibiscus...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Dalydress%20Bi-Tone%20Cap%20Sleeves%20Slip%20On%20Top%20-%20White%20&amp;%20Beige/53"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/b82eace1fdb0fbcb1e73ab866774403c.jpg"
                                                         alt="Dalydress Bi-Tone Cap Sleeves Slip On Top - White &amp; Beige">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Dalydress Bi-Tone Cap Sleeves Slip On Top...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Ravin%20Knitted%20Long%20Sleeves%20Dark%20Aqua%20Pullover/58"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/f5f71891c67494044b16d5ff2aa4c7a1.jpg"
                                                         alt="Ravin Knitted Long Sleeves Dark Aqua Pullover">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Ravin Knitted Long Sleeves Dark Aqua Pullo...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Cool%20Guy%20Black%20T-Shirt%20for%20Men/65"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/f9b3d64e66c33526812f938516c3f495.jpg"
                                                         alt="Cool Guy Black T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Cool Guy Black T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Joker%20black%20T-Shirt%20for%20Men/69"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/2a00b980ecfd1b0a518513bfe8341db7.jpg"
                                                         alt="Joker black T-Shirt for Men">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Joker black T-Shirt for Men</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Waterproof%20Casual%20Jacket%20-%20Black/84"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/30dc90a8a0a1c3663ab6d9f508a0a54d.jpg"
                                                         alt="Premoda Waterproof Casual Jacket - Black">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Waterproof Casual Jacket - Black</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Decorative%20Pearls%20Slip%20On%20Pullover%20-%20Pale%20Teal%20Green/90"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/760636b594fcd1d5386b24bc1c265469.jpg"
                                                         alt="Premoda Decorative Pearls Slip On Pullover - Pale Teal Green">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Decorative Pearls Slip On Pullover...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/en/p/Premoda%20Textured%20Leather%20Elegant%20Loafers%20-%20Burnt%20Brown/99"
                                           class="product-link">
                                            <div class="products">
                                                <div class="product-img">
                                                    <img class="lazy"
                                                         src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/loading.gif"
                                                         data-src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/products/thumbMd/85201e2c062d5db9d30a3aa3d2473820.jpg"
                                                         alt="Premoda Textured Leather Elegant Loafers - Burnt Brown">
                                                </div>
                                                <div class="product-text">

                                                    <h2>Premoda Textured Leather Elegant Loafers -...</h2>
                                                    <div class="row">
                                                        <div class="col-8 product-price">
                                                            <div class="discount-wrapper">
                                                                usd 109.00
                                                                <span>7% off</span>
                                                            </div>
                                                            usd 105.00
                                                        </div>
                                                        <div class="col-4 text-right">
                                                            <div class="add-to-cart-wrapper">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="footer-links-wrapper row">
                    <div class="footer-links col-md-3">
                        <h3>Get to Know Us</h3>
                        <ul>
                            <li>
                                <a href="" class="nav_a">Careers</a>
                            </li>
                            <li>
                                <a href="">Blog</a>
                            </li>
                            <li>
                                <a href="">About SouvenirSharm</a>
                            </li>
                            <li>
                                <a href="">Investor Relations</a>
                            </li>
                            <li>
                                <a href="">SouvenirSharm Devices</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-links col-md-3">
                        <h3>Make Money with Us</h3>
                        <ul>
                            <li>
                                <a href="">Sell on SouvenirSharm</a>
                            </li>
                            <li>
                                <a href="">Sell Your Services on SouvenirSharm</a>
                            </li>
                            <li>
                                <a href="">Sell on SouvenirSharm Business</a>
                            </li>
                            <li>
                                <a href="">Sell Your Apps on SouvenirSharm</a>
                            </li>
                            <li>
                                <a href="">Become an Affiliate</a>
                            </li>
                            <li>
                                <a href="">Advertise Your Products</a>
                            </li>
                            <li>
                                <a href="">Self-Publish with Us</a>
                            </li>
                            <li>
                                <a href="">See all</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-links col-md-3">
                        <h3>SouvenirSharm Payment Products</h3>
                        <ul>
                            <li>
                                <a href="">SouvenirSharm Business Card</a>
                            </li>
                            <li>
                                <a href="">Shop with Points</a>
                            </li>
                            <li>
                                <a href="">Reload Your Balance</a>
                            </li>
                            <li>
                                <a href="">SouvenirSharm Currency Converter</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-links col-md-3">
                        <h3>Let Us Help You</h3>
                        <ul>
                            <li>
                                <a href="">Your Account</a>
                            </li>
                            <li>
                                <a href="">Your Orders</a>
                            </li>
                            <li>
                                <a href="">Shipping Rates & Policies</a>
                            </li>
                            <li>
                                <a href="">Returns & Replacements</a>
                            </li>
                            <li>
                                <a href="">Manage Your Content and Devices</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-ref">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 social-media text-center">
                            <span>follow us on</span>
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                            <a href=""><i class="fab fa-twitter"></i></a>
                            <a href=""><i class="fab fa-instagram"></i></a>
                            <a href=""><i class="fab fa-youtube"></i></a>
                        </div>
                        <div class="col-md-4 languages text-center">
                            <span>languages</span>
                            <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/change-lang/en">
                                <img src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/en-flag.jpg">
                            </a>
                            <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/change-lang/ar">
                                <img src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/eg-flag.jpg">
                            </a>
                            <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/change-lang/ru">
                                <img src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/ru-flag.jpg">
                            </a>
                            <a href="https://turquoise-hotel.net/souvenirsharm.com/new/public/change-lang/it">
                                <img src="https://turquoise-hotel.net/souvenirsharm.com/new/public/images/it-flag.jpg">
                            </a>
                        </div>
                        <div class="col-md-4 payment-methods text-center">
                            <span>payment methods</span>
                            <i class="fas fa-money-bill-wave"></i>
                            <i class="fab fa-paypal"></i>
                            <i class="fab fa-cc-visa"></i>
                            <i class="fab fa-cc-mastercard"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-copy-rights text-center">
                &copy; 2019 souvenirsharm.com, powered py <a href="mailto:info.matrixcode@gmail.com"> Matrix Code</a>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://turquoise-hotel.net/souvenirsharm.com/new/public/js/yall.js"></script>
<script>document.addEventListener("DOMContentLoaded", yall);</script>

<script>
    const wrapper = document.getElementById('__wrapper');
    const showSideBarIcon = document.getElementById('__show_side_menu');
    const headerNav = document.getElementById('__header_nav');
    showSideBarIcon.addEventListener('click', function () {
        if (!wrapper.classList.contains('wrapper--toggle')) {
            wrapper.classList.add('wrapper--toggle');
            headerNav.classList.add('c-header--moving');
            showSideBarIcon.classList.add('hamburger--active');
            return true;
        }
        wrapper.classList.remove('wrapper--toggle');
        headerNav.classList.remove('c-header--moving');
        showSideBarIcon.classList.remove('hamburger--active');
    });
    $("a.drop-down-link").click(function (event) {
        event.preventDefault();
        var li = $(this).closest('li');
        var allLi = $(this).closest('li').closest('ul').find('li');
        if (li.hasClass('active')) {
            allLi.removeClass('active');
            return true;
        }
        allLi.removeClass('active');
        li.addClass('active');
    });
</script>

<script>
    $("a.mob-menu-icon-wrapper").click(function (event) {
        event.preventDefault();
        var icon = $(this).find('.mob-menu-icon');
        if (!icon.hasClass('lunched')) {
            icon.addClass('lunched');
            sideMenu(true);
            return true;
        }
        icon.removeClass('lunched');
        sideMenu(false);
    });

    function sideMenu(is_lunched) {
        var parent = $(".body-wrapper");
        var mainNav = $(".main-nav");
        if (is_lunched) {
            parent.addClass('lunched');
            mainNav.addClass('lunched');
        } else {
            parent.removeClass('lunched');
            mainNav.removeClass('lunched');
        }
    }
</script>
<script>
    $("li.menu-item > a").click(function (event) {
        event.preventDefault();
        var parent = $(this).closest('li');
        var submenu = parent.find('ul.sub-menu');
        if (submenu.length) {
            if (!parent.hasClass('lunched')) {
                parent.addClass('lunched');
                return true;
            }
            parent.removeClass('lunched');
        }
    });
</script>
<script>
    // scroll list
    $(".list-control-next").click(function () {
        var parent = $(this).closest('.products-list-wrapper');
        var list = parent.find('.product-list-row');
        var row = list.find('.row');
        var row_scroll_width = parseInt(row[0].scrollWidth);
        var offset = list.scrollLeft();
        var true_offset = parseInt(offset) + list.width() + 15;
        var prev_elm = parent.find('.list-control-prev');

        if (true_offset < row_scroll_width) {
            if (true_offset + list.width() > row_scroll_width) {
                $(this).addClass('unavailable');
            }
            if (prev_elm.hasClass('unavailable')) {
                prev_elm.removeClass('unavailable');
            }
            offset = offset + list.width();
            list.animate({scrollLeft: offset}, 500);
            return true;
        }


    });
    $(".list-control-prev").click(function () {
        var parent = $(this).closest('.products-list-wrapper');
        var list = parent.find('.product-list-row');
        var offset = list.scrollLeft();
        var next_elm = parent.find('.list-control-next');
        if (offset > 0) {
            if (offset < list.width()) {
                $(this).addClass('unavailable');
            }
            if (next_elm.hasClass('unavailable')) {
                next_elm.removeClass('unavailable');
            }
            offset = offset - list.width();
            console.log(offset);
            list.animate({scrollLeft: offset}, 500);
            return true;
        }
    });
</script>
</body>
</html>