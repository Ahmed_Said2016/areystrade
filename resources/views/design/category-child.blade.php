@extends('design.layouts.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/category.child.css')}}">
@endpush
@section('content')
    <div class="content-insider">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="filter">
                        <div class="filter__section">
                            <h4 class="filter__title">Supplier Types</h4>
                            <div class="filter__items">
                                <x-front.checkbox name="vendor_type" value="1">
                                    <i class="ic ic--trade-assurance"></i> Trade Assurance
                                </x-front.checkbox>
                                <x-front.checkbox name="vendor_type" value="0">
                                    <i class="ic ic--verified"></i>Supplier
                                </x-front.checkbox>
                            </div>
                        </div>
                        <div class="filter__section">
                            <h4 class="filter__title">Product Types</h4>
                            <div class="filter__items">
                                <x-front.checkbox name="product_type" value="1">Ready to Ship</x-front.checkbox>
                                <x-front.checkbox name="product_type" value="0">Paid Samples</x-front.checkbox>
                            </div>
                        </div>
                        <div class="filter__section">
                            <h4 class="filter__title">Min. Order</h4>
                            <div class="filter__items">
                                <div class="filter__inputs filter__inputs--inline">
                                    <div class="filter__col">
                                        <input class="filter__input" type="number" placeholder="less than"
                                               name="min_order"
                                               aria-label="filters">
                                    </div>
                                    <button class="filter__btn">OK</button>
                                </div>
                            </div>
                        </div>
                        <div class="filter__section">
                            <h4 class="filter__title">Price</h4>
                            <div class="filter__items">
                                <div class="filter__inputs filter__inputs--inline">
                                    <div class="filter__col">
                                        <input class="filter__input" type="number" placeholder="min" name="price_from"
                                               aria-label="filters">
                                    </div>
                                    <span class="filter__separator">-</span>
                                    <div class="filter__col">
                                        <input class="filter__input" type="number" placeholder="max" name="price_to"
                                               aria-label="filters">
                                    </div>
                                    <button class="filter__btn">OK</button>
                                </div>
                            </div>
                        </div>
                        <div class="filter__section">
                            <h4 class="filter__title">Supplier Country/Region</h4>
                            <div class="filter__items">
                                <div class="filter__inputs">
                                    <div class="filter__col">
                                        <span class="filter__addon"><i class="las la-search"></i></span>
                                        <input class="filter__input" type="number" placeholder="less than"
                                               name="min_order"
                                               aria-label="filters">
                                    </div>
                                </div>
                                <div class="filter__multi">
                                    <h5 class="filter__title filter__title--secondary">All Countries & Regions</h5>
                                    @for($i=1;$i<=3;$i++)
                                        @foreach(__('test.countries') as $country)
                                            <x-front.checkbox name="country[]" value="1">
                                                <span class="checkbox__img checkbox__img--flag">
                                                    <img src="{{asset('images/__design/flags/'.$country['code'].".webp")}}"
                                                         alt="flag">
                                                </span>
                                                {{$country['name']}} <span class="checkbox__amount">({{$country['amount']}})</span>
                                            </x-front.checkbox>
                                        @endforeach
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="filter__section">
                            <h4 class="filter__title">Management Certification</h4>
                            <div class="filter__items">
                                <div class="filter__inputs">
                                    <div class="filter__col">
                                        <span class="filter__addon"><i class="las la-search"></i></span>
                                        <input class="filter__input" type="number" placeholder="search"
                                               name="min_order"
                                               aria-label="filters">
                                    </div>
                                </div>
                                <div class="filter__multi">
                                    <x-front.checkbox name="country[]" value="1">
                                                <span class="checkbox__img checkbox__img--logo">
                                                    <img src="{{asset('images/__design/certificates/ISO-LOGO.jpg')}}"
                                                         alt="flag">
                                                </span>
                                        ISO9001 <span class="checkbox__amount">(182)</span>
                                    </x-front.checkbox>
                                    <x-front.checkbox name="country[]" value="1">
                                                <span class="checkbox__img checkbox__img--logo">
                                                    <img src="{{asset('images/__design/certificates/ISO-LOGO.jpg')}}"
                                                         alt="flag">
                                                </span>
                                        ISO14001 <span class="checkbox__amount">(91)</span>
                                    </x-front.checkbox>
                                    <x-front.checkbox name="country[]" value="1">
                                                <span class="checkbox__img checkbox__img--logo">
                                                    <img src="{{asset('images/__design/certificates/bsci1.jpg')}}"
                                                         alt="flag">
                                                </span>
                                        BSCI <span class="checkbox__amount">(73)</span>
                                    </x-front.checkbox>
                                    <x-front.checkbox name="country[]" value="1">
                                                <span class="checkbox__img checkbox__img--logo">
                                                    <img src="{{asset('images/__design/certificates/ohsas.png')}}"
                                                         alt="flag">
                                                </span>
                                        OHSAS18001 <span class="checkbox__amount">(51)</span>
                                    </x-front.checkbox>
                                    <div class="filter__note">
                                        *Certification Disclaimer: Any assessment, certification, inspection and/or
                                        related examination related to any authenticity of certificates are provided or
                                        conducted by independent third parties with no involvement from Alibaba.com.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="products">
                        <div class="products__header">
                            <div class="products__count"><span>13358</span> results for "Product"</div>
                            <div class="products__sorts">
                                <div class="sorts">
                                    <span class="sorts__label">SORT BY</span>
                                    <div class="sorts__all">
                                        <a href="javascript:void(0)" class="sorts__link">Best Match</a>
                                        <div class="sorts__choose">
                                            <a href="javascript:void(0)" class="sorts__item">Best Match</a>
                                            <a href="javascript:void(0)" class="sorts__item">Transaction Level</a>
                                            <a href="javascript:void(0)" class="sorts__item">Response Rate</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="products__result">
                            @for($i=1;$i<=4;$i++)
                                <div class="row products__row">
                                    @foreach(array_slice(__('test.test_products'),0,4) as $product)
                                        <div class="col-md-3">
                                            <div class="product-item">
                                                <div class="product-item__image">
                                                    <img class="lazy"
                                                         src="{{asset('images/loading.gif')}}"
                                                         data-src="{{asset($product['image'])}}"
                                                         alt="Small Shoulder Leather Bag - Burgundy">
                                                </div>
                                                <div class="product-item__txt">
                                                    <h2 class="product-item__title">{{$product['title']}}</h2>
                                                    <div class="product-item__price">
                                                        from USD<span>{{$product['price']}}</span>/{{$product['unit']}}
                                                    </div>
                                                    <div class="product-item__quantity">
                                                        Min.Orders<span>({{$product['min_order']}})</span>{{$product['unit']}}
                                                    </div>

                                                    <button class="product-item__btn"></button>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row products__row">
                                    @foreach(array_slice(__('test.test_products'),-4) as $product)
                                        <div class="col-md-3">
                                            <div class="product-item">
                                                <div class="product-item__image">
                                                    <img class="lazy"
                                                         src="{{asset('images/loading.gif')}}"
                                                         data-src="{{asset($product['image'])}}"
                                                         alt="Small Shoulder Leather Bag - Burgundy">
                                                </div>
                                                <div class="product-item__txt">
                                                    <h2 class="product-item__title">{{$product['title']}}</h2>
                                                    <div class="product-item__price">
                                                        from USD<span>{{$product['price']}}</span>/{{$product['unit']}}
                                                    </div>
                                                    <div class="product-item__quantity">
                                                        Min.Orders<span>({{$product['min_order']}})</span>{{$product['unit']}}
                                                    </div>

                                                    <button class="product-item__btn"></button>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop