<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.alternative.css')}}">
    @stack('css')
</head>
<body>
<div class="wrapper" id="__wrapper">
    <div class="wrapper__sidebar">
        <div class="sidebar">
            <ul class="sidebar__list">
                @foreach(__('test.categories') as $category)
                    <li class="sidebar__item">
                        <a href="javascript:void(0)" class="sidebar__link">
                            {{$category}}
                            <div class="sidebar__link__arrow">
                                <i class="las la-angle-down"></i>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="wrapper__container">
        <div class="c-header" id="__header_nav">
            <div class="c-header__topnav">
                <div class="container">
                    <div class="l-toplist">
                        <ul class="l-toplist__list">
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-shipping-fast"></i> <span>Ready to Ship</span></a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="lar la-handshake"></i> Trade Insurance</a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-lightbulb"></i> Sourcing Solutions</a>
                            </li>
                        </ul>
                        <ul class="l-toplist__list l-toplist__list--right">
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-user"></i> Sign in | Join Free</a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-shopping-bag"></i> Orders</a>
                            </li>
                            <li class="l-toplist__item">
                                <a href=""><i class="las la-shopping-cart"></i> Cart</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="c-header__mainnav">
                <div class="container">
                    <div class="middlenav">
                        <div class="middlenav__logo">
                            <a href="">
                                <img src="{{asset('images/logo-test.png')}}" alt="Areys Trade">
                            </a>
                        </div>
                        <div class="middlenav__search">
                            <div class="search">
                                <div class="search__box">
                                    <select class="search__element search__element--selector custom-select" name="type"
                                            aria-label="search type">
                                        <option value="products" selected>Products</option>
                                        <option value="suppliers">Suppliers</option>
                                    </select>
                                    <input class="search__element search__element--input"
                                           placeholder="What your are looking for" aria-label="search">
                                    <button class="search__element search__element--btn">
                                        <i class="las la-search"></i> Search
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="middlenav__links">
                            <ul class="middlenav__list">
                                <li class="middlenav__item">
                                    <a href=""><i class="las la-coins"></i> USD</a>
                                </li>
                                <li class="middlenav__item middlenav__item--arabic">
                                    <a href=""><i class="las la-globe-africa"></i> العربية</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-header__linknav">
                <div class="container">
                    <div class="nav">
                        <ul class="nav__list">
                            <li class="nav__item">
                                <a href="javascript:void(0)">All <span>Categories</span>
                                    <div class="hamburger" id="__show_side_menu">
                                        <div class="hamburger__line"></div>
                                    </div>
                                </a>
                            </li>
                            @foreach(__('test.nav') as $key=>$item)
                                <li class="nav__item">
                                    <a href="javascript:void(0)">{{$item}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @yield('content')
        <div class="footer">
            <div class="container">
                <div class="footer__links body-row">
                    <div class="footer__col">
                        <h3 class="footer__title">Customer Services</h3>
                        <ul class="footer__list">
                            <li class="footer__item">
                                <a href="">Help center</a>
                            </li>
                            <li class="footer__item">
                                <a href="">Contact Us</a>
                            </li>
                            <li class="footer__item">
                                <a href="">Report Abuse</a>
                            </li>
                            <li class="footer__item">
                                <a href="">Submit a Dispute</a>
                            </li>
                            <li class="footer__item">
                                <a href="">Policies &amp; Rules</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__col">
                        <h3 class="footer__title">About Us</h3>
                        <ul class="footer__list">
                            <li class="footer__item">
                                <a href="">About Areystrade.com</a>
                            </li>
                            <li class="footer__item">
                                <a href="">About Areys Group</a>
                            </li>
                            <li class="footer__item">
                                <a href="">Sitemap</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__col">
                        <h3 class="footer__title">Buy on Areys</h3>
                        <ul class="footer__list">
                            <li class="footer__item">
                                <a href="" class="services__box-link">All Categories</a>
                            </li>
                            <li class="footer__item">
                                <a href="" class="services__box-link">Ready to Ship</a>
                            </li>
                            <li class="footer__item">
                                <a href="" class="services__box-link">Request for Quotation</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__col">
                        <h3 class="footer__title">Sell on Areys</h3>
                        <ul class="footer__list">
                            <li class="footer__item">
                                <a href="" class="footer__item">Supplier Memberships</a>
                            </li>
                            <li class="footer__item">
                                <a href="" class="footer__item">Learning Center</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__col">
                        <h3 class="footer__title">Trade Services</h3>
                        <ul class="footer__list">
                            <li class="footer__item"><a href="#">Trade Assurance</a></li>
                            <li class="footer__item"><a href="#">Business Identity</a></li>
                            <li class="footer__item"><a href="#">Logistics Service</a></li>
                            <li class="footer__item"><a href="#">Production Monitoring &amp; Inspection Services</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer__logo">
                <a href="#"><img src="{{asset('images/__design/footerareylogo.svg')}}" alt="#"> </a>
                <div class="footer__socials">
                    <a href="#"><i class="lab la-facebook-f"></i></a>
                    <a href="#"><i class="lab la-instagram"></i></a>
                    <a href="#"><i class="lab la-twitter"></i></a>
                </div>
            </div>
            <div class="footer__bottom">
                <p class="footer__languages">
                    Site:
                    <a href="#">International</a> -
                    <a href="#">Español</a> -
                    <a href="#">Português</a> -
                    <a href="#">Deutsch</a> -
                    <a href="#">Français</a> -
                    <a href="#">Italiano</a> -
                    <a href="#">हिंदी</a> -
                    <a href="#">Pусский</a> -
                    <a href="#" data-spm-anchor-id="a2700.8293689.0.0">한국어</a>-
                    <a href="#">日本語</a> -
                    <a href="#">اللغة العربية</a> -
                    <a href="#">ภาษาไทย</a> -
                    <a href="#">Türk</a> -
                    <a href="#">Nederlands</a> -
                    <a href="#">tiếng Việt</a> -
                    <a href="#">Indonesian</a> -
                    <a href="#">עברית</a>
                </p>
                <p class="footer__alternate-search">
                    Browse Alphabetically:
                    <a target="_blank" href="#">Onetouch</a>
                    | <a target="_blank" href="#">Showroom</a>
                    | <a target="_blank" href="#">Country Search</a>
                    | <a target="_blank" href="#">Suppliers</a>
                    | <a target="_blank" href="#">Wholesaler</a>
                    | <a target="_blank" href="#">Affiliate</a>
                </p>
                <p class="footer__policy">
                    <a href="#">
                        Product Listing Policy
                    </a>
                    - <a href="#">
                        Intellectual Property Protection
                    </a>
                    - <a href="#">
                        Privacy Policy
                    </a>
                    - <a href="#">
                        Terms of Use
                    </a>
                    - <a href="#">User Information
                        Legal Enquiry Guide</a>
                </p>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/yall.js')}}"></script>
<script>document.addEventListener("DOMContentLoaded", yall);</script>

<script>
    const wrapper = document.getElementById('__wrapper');
    const showSideBarIcon = document.getElementById('__show_side_menu');
    const headerNav = document.getElementById('__header_nav');
    showSideBarIcon.addEventListener('click', function () {
        if (!wrapper.classList.contains('wrapper--toggle')) {
            wrapper.classList.add('wrapper--toggle');
            headerNav.classList.add('c-header--moving');
            showSideBarIcon.classList.add('hamburger--active');
            return true;
        }
        wrapper.classList.remove('wrapper--toggle');
        headerNav.classList.remove('c-header--moving');
        showSideBarIcon.classList.remove('hamburger--active');
    });
</script>
@stack('javascript')
</body>
</html>