@extends('design.layouts.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/products.css')}}">
@endpush
@section('content')
    <div class="content-insider">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="products-image">
                        <div class="products-image__preview">
                            <img src="{{asset('images/test/Ambarella-A750-Car-Camera-DVR-FHD-1080P.jpg')}}" alt="#">
                        </div>
                        <a href="#" class="products-image__show">
                            <i class="las la-search-plus"></i> View Larger Image
                        </a>
                        <div class="products-image__thumbs">
                            <a class="products-image__thumb products-image__thumb--active" href="#">
                                <img src="{{asset('images/test/Ambarella-A750-Car-Camera-DVR-FHD-1080P.jpg')}}" alt="#">
                            </a>
                            @foreach(__('test.test_products') as $image)
                                <a href="#" class="products-image__thumb">
                                    <img src="{{asset($image['image'])}}" alt="#">
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop