@extends('design.layouts.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/parent.category.css')}}">
@endpush
@section('content')
    <div class="content-insider">
        <div class="container">
            <div class="header">
                <div class="header__nav">
                    <div class="category-nav">
                        <h1 class="category-nav__title">Categories</h1>
                        <ul class="category-nav__list">
                            <li class="category-nav__item"><a href="#">Agriculture Waste</a></li>
                            <li class="category-nav__item"><a href="#">Agriculture Equipment</a></li>
                            <li class="category-nav__item"><a href="#">Animal Feed</a></li>
                            <li class="category-nav__item"><a href="#">Animal Products</a></li>
                            <li class="category-nav__item"><a href="#">Beans</a></li>
                            <li class="category-nav__item"><a href="#">Fresh Fruit</a></li>
                            <li class="category-nav__item"><a href="#">Fresh Vegetables</a></li>
                            <li class="category-nav__item"><a href="#">Grain</a></li>
                        </ul>
                    </div>
                </div>
                <div class="header__details">
                    <div class="header__banner">
                        <img src="{{asset('images/test/TB1Z8PZv7T2gK0jSZPcXXcKkpXa-968-230.png')}}" alt="Category">
                    </div>
                    <div class="header__shortcuts">
                        @foreach(__('test.category_tags') as $tag)
                            <a href="#" class="header__shortcut">
                                <img src="{{asset($tag['image'])}}" alt="#">
                                <span>{{$tag['name']}}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="hot">
                <h1 class="hot__header">HOT CATEGORIES</h1>
                <div class="row">
                    @foreach(__('test.hot_categories') as $hot)
                        <div class="col-md-3">
                            <div class="hot__item">
                                <div class="hot__img">
                                    <img src="{{asset($hot['image'])}}" alt="#">
                                </div>
                                <h2 class="hot__title">{{$hot['name']}}</h2>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="box box--white">
                <div class="box__header">
                    <h1 class="box__title">Recommended Agriculture Products</h1>
                </div>
                <div class="box__body">
                    <div class="scrollable">
                        <a href=""
                           class="scrollable__control scrollable__control--prev"
                           id="__previous">
                            <i class="las la-angle-left"></i>
                        </a>
                        <a href="#"
                           class="scrollable__control scrollable__control--next scrollable__control--active"
                           id="__next">
                            <i class="las la-angle-right"></i>
                        </a>
                        <div class="scrollable__container">
                            <div class="scrollable__wrapper">
                                @for($i=1;$i<=3;$i++)
                                    @foreach(__('test.test_products') as $product)
                                        <div class="scrollable__item">
                                            <div class="product-item">
                                                <div class="product-item__image">
                                                    <img class="lazy"
                                                         src="{{asset('images/loading.gif')}}"
                                                         data-src="{{asset($product['image'])}}"
                                                         alt="Small Shoulder Leather Bag - Burgundy">
                                                </div>
                                                <div class="product-item__txt">
                                                    <h2 class="product-item__title">{{$product['title']}}</h2>
                                                    <div class="product-item__price">
                                                        from USD<span>{{$product['price']}}</span>/{{$product['unit']}}
                                                    </div>
                                                    <div class="product-item__quantity">
                                                        Min.Orders<span>({{$product['min_order']}})</span>{{$product['unit']}}
                                                    </div>

                                                    <button class="product-item__btn"></button>

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endfor
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="category-policies body-row">
                @foreach(__('test.category_policies') as $policy)
                    <div class="category-policies__item">
                        <h1 class="category-policies__title">{{$policy['title']}}</h1>
                        <div class="category-policies__text">
                            {{$policy['text']}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
