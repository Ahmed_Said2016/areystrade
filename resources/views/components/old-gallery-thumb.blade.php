<div class="thumb-preview">
    <div class="image-wrapper">
        <img src="{{asset('storage/'.$imageModel()->thumb)}}">
    </div>
    <div class="details-inputs">
        <input type="hidden"
               name="{{$name}}[{{$id()}}][image_id]"
               value="{{$imageModel()->id}}">
        <input type="hidden"
               name="{{$name}}[{{$id()}}][alt]"
               value="{{$old['alt']}}">
        <input type="hidden"
               name="{{$name}}[{{$id()}}][title]"
               value="{{$old['title']}}">
        <input type="hidden"
               name="{{$name}}[{{$id()}}][caption]"
               value="{{$old['caption']}}">
    </div>
</div>