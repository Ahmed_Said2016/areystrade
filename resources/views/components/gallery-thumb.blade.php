<div class="thumb-preview">
    <div class="image-wrapper">
        <img src="{{asset('storage/'.$image->url)}}">
    </div>
    <div class="details-inputs">
        <input type="hidden"
               name="{{$inputName}}[image_id]"
               value="{{$image->id}}">
        <input type="hidden"
               name="{{$inputName}}[alt]"
               value="{{$image->pivot->alt}}">
        <input type="hidden"
               name="{{$inputName}}[title]"
               value="{{$image->pivot->title}}">
        <input type="hidden"
               name="{{$inputName}}[caption]"
               value="{{$image->pivot->caption}}">
    </div>
</div>