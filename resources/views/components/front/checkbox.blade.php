<div {{ $attributes->merge(['class' => 'checkbox']) }}>
    <label class="checkbox__label">
        <input type="checkbox" class="checkbox__input" name="{{$name}}" value="{{$value}}"
               aria-label="checkbox" {{$isChecked()}}>
        <span class="checkbox__mark"></span>
        <span class="checkbox__txt">
            {{$slot}}
        </span>
    </label>
</div>