<div class="form-group{{$required?' is-required':''}} row">
    <label class="col-sm-3 col-form-label">{{$label}}</label>
    <div class="col-sm-6">
        <x-form-elements.input :name="$name" :value="$value" :required="$required" place-holder=""/>
    </div>
</div>
