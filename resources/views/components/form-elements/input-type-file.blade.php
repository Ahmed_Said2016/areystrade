<div class="custom-file @error($name) is-invalid @enderror">
    <input type="file" name="{{stringToInputArray($name)}}" class="custom-file-input" {{$required?'required':''}}>
    <label class="custom-file-label">Choose image</label>
</div>