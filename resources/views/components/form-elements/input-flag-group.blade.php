<div class="input-group input-group-min mb-3 @error($name)is-invalid @enderror">
    <div class="input-group-prepend">
        <span class="input-group-text">
            <img src="{{$imageSource($image)}}" alt="{{$placeHolder}}">
        </span>
    </div>
    <input type="text" name="{{stringToInputArray($name)}}" class="form-control" placeholder="{{$placeHolder}}"
           value="{{old($name)??$value}}" aria-label="{{$placeHolder}}" autocomplete="off"
           aria-describedby="{{$placeHolder}}" {{$required?'required':''}}>
</div>