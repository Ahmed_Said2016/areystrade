<div class="form-group{{$required?' is-required':''}} row">
    <label class="col-sm-3 col-form-label">
        {{$label}}
    </label>
    <div class="col-sm-9">
        @foreach($list as $item)
            <div class="form-check form-check-inline">
                <div class="checkbox">
                    <input id="__filter_checkbox_{{$item->id}}" name="{{stringToInputArray($name)}}" class="form-check-input"
                           type="checkbox" value="{{$item->id}}" {{$isChecked($item->id)}}>
                    <label for="__filter_checkbox_{{$item->id}}">
                        {{$item->en_name}}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
</div>