<div class="form-group{{$required?' is-required':''}} row">
    <label class="col-sm-3 col-form-label">
        {{$label}}
    </label>
    <div class="col-sm-6">
        <select name="{{stringToInputArray($name)}}" class="custom-select @error($name) is-invalid @enderror"
                aria-label="{{$ariaLabel($name)}}" {{$required?' required':''}}>
            <option></option>
            @foreach($list as $item)
                <option value="{{$item->id}}" {{$isSelected($item->id)}}>{{$item->en_name}}</option>
            @endforeach
        </select>
    </div>
</div>