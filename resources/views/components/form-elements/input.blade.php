<input class="form-control @error($name)is-invalid @enderror" name="{{stringToInputArray($name)}}"
       aria-label="{{$ariaLabel($name)}}" value="{{old($name)??$value}}" {{!is_null($type)?'type="'.$type.'"':''}}
        {!!  !is_null($placeHolder)?'placeholder="'.$placeHolder.'"':''!!} {{$required?'required':''}}>