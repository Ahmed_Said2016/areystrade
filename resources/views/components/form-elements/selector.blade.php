<select class="custom-select @error($name) is-invalid @enderror" name="{{stringToInputArray($name)}}"
        aria-label="{{$ariaLabel($name)}}" {{$isRequired()}} {{ $attributes }}>
    {{ $slot }}
    @foreach($list as $key=>$value)
        <option value="{{$key}}" {{$isSelected($key)}}>{{$value}}</option>
    @endforeach
</select>