<div class="form-group">
    <label>Meta Title</label>
    <div class="row">
        <div class="col">
            <x-form-elements.input-flag-group name="{{$inputName('en_title')}}"
                                              :value="optional($model->tags)->en_title"
                                              placeHolder="English Meta title" image="en"/>
        </div>
        <div class="col">
            <x-form-elements.input-flag-group name="{{$inputName('ar_title')}}"
                                              :value="optional($model->tags)->ar_title"
                                              placeHolder="Arabic Meta title" image="ar"/>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Meta Keywords</label>
    <div class="row">
        <div class="col">
            <x-form-elements.input-flag-group name="{{$inputName('en_keywords')}}"
                                              :value="optional($model->tags)->en_keywords"
                                              placeHolder="English Meta keywords" image="en"/>
        </div>
        <div class="col">
            <x-form-elements.input-flag-group name="{{$inputName('ar_keywords')}}"
                                              :value="optional($model->tags)->ar_keywords"
                                              placeHolder="Arabic Meta keywords" image="ar"/>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Meta Description</label>
    <div class="row">
        <div class="col">
            <x-form-elements.input-flag-group name="{{$inputName('en_description')}}"
                                              :value="optional($model->tags)->en_description"
                                              placeHolder="English Meta Description" image="en"/>
        </div>
        <div class="col">
            <x-form-elements.input-flag-group name="{{$inputName('ar_description')}}"
                                              :value="optional($model->tags)->ar_description"
                                              placeHolder="Arabic Meta Description" image="ar"/>
        </div>
    </div>
</div>