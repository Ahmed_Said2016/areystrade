<?php
return [
    'units' => [
        'Acre',
        'Ampere',
        'Bag',
        'Barrel',
        'Blade',
        'Box',
        'Bushel',
        'Carat',
        'Carton',
        'Case',
        'Centimeter',
        'Chain',
        'Combo',
        'Cubic Centimeter',
        'Cubic Foot',
        'Cubic Inch',
        'Cubic Meter',
        'Cubic Yard',
        'Degrees Celsius',
        'Degrees Fahrenheit',
        'Dozen',
        'Dram',
        'Fluid Ounce',
        'Foot',
    ]
];