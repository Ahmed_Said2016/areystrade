<?php
return [
    'categories' => [
        ' Machinery / Fabrication Service',
        'Consumer Electronics / Home Appliances',
        'Apparel / Textiles & Leather Products / Timepieces,Jewelry,Eyewear / Fashion Accessories',
        'Lights & Lighting / Furniture / Construction & Real Estate',
        'Home & Garden',
        ' Beauty & Personal Care / Health & Medical',
        ' Packaging & Printing / Office & School Supplies / Service Equipment',
        'Lights & Lighting / Furniture / Construction & Real Estate',
        'Home & Garden',
        ' Beauty & Personal Care / Health & Medical',
        ' Packaging & Printing / Office & School Supplies / Service Equipment',
        ' Machinery / Fabrication Service',
        'Consumer Electronics / Home Appliances',
        'Apparel / Textiles & Leather Products / Timepieces,Jewelry,Eyewear / Fashion Accessories',
        'Lights & Lighting / Furniture / Construction & Real Estate',
        'Home & Garden',
        ' Beauty & Personal Care / Health & Medical',
    ],
    'nav' => [
        'Agriculture',
        'Food & Beverage',
        'Apparel',
        'Textile & Leather Product',
        'Vehicles & Accessories',
        'Machinery & Fabrication Services'
    ],
    'welcome_top_categories' => [
        'images/1419-block9-home-brand.jfif',
        'images/1419-block10-etk.jfif',
        'images/1419_block8_tusale.jfif'
    ],
    'welcome_categories_nav' => [
        ['icon' => '<i class="las la-car"></i>', 'category' => 'Auto Parts & Accessories'],
        ['icon' => '<i class="las la-desktop"></i>', 'category' => 'Consumer Electronics'],
        ['icon' => '<i class="las la-gamepad"></i>', 'category' => 'Electronic Components'],
        ['icon' => '<i class="las la-shoe-prints"></i>', 'category' => 'Fashion Accessories'],
        ['icon' => '<i class="las la-tshirt"></i>', 'category' => 'Fashion Apparel'],
        ['icon' => '<i class="las la-gift"></i>', 'category' => 'Gifts & Premiums'],
        ['icon' => '<i class="las la-robot"></i>', 'category' => 'Hardware'],
        ['icon' => '<i class="las la-couch"></i>', 'category' => 'Home Products'],
        ['icon' => '<i class="las la-tractor"></i>', 'category' => 'Machinery & Parts'],
        ['icon' => '<i class="las la-mobile"></i>', 'category' => 'Mobile Electronics'],
        ['icon' => '<i class="las la-wifi"></i>', 'category' => 'Smart Living'],
    ],
    'test_products' => [
        [
            'image' => 'images/test/rBVaWF6qSEmAPROJAAEfVbNcSYY651.jpg',
            'title' => 'TWS Wireless Bluetooth 5.0 headphones Charging Case LED Display Waterproof Earphone Auto Pairing PK xiaomi i12 i500',
            'price' => '20.11',
            'unit' => 'Piece',
            'min_order' => '1000'
        ],
        [
            'image' => 'images/test/rBVaVl1jW6-AdZL9AAGD4MGloio610.jpg',
            'title' => 'Bluetooth Aux 3.5mm Jack Bluetooth Car Kit Hands Free Music Audio Receiver Adapter Auto AUX Kit for Speaker Bluetooth Car Stereo',
            'price' => '5.97',
            'unit' => 'Piece',
            'min_order' => '1000'
        ],
        [
            'image' => 'images/test/rBVaVF6-PEyALxANAAG5uflXsw4902.jpg',
            'title' => 'Best Quality Full Chip MB STAR C4 Multiplexer MB SD Connect Compact 4 Diagnostic Tool with WIFI CARD MB Star Diagnosis C4 ',
            'price' => '40.21',
            'unit' => 'Piece',
            'min_order' => '1000'
        ],
        [
            'image' => 'images/test/rBVaVl7XMjaAAzb2AACwAd2Wptc227.jpg',
            'title' => 'Rc Car 1:12 4WD Off-road Climbing Remote Control 2.4Hz Radio Controlled Tracked Rc Car Child Toy',
            'price' => '11.67',
            'unit' => 'Piece',
            'min_order' => '3000'
        ],
        [
            'image' => 'images/test/rBVaWV7PqcqAfVbmAAHHbwLSFh4423.jpg',
            'title' => 'Disposable mask pad anti-PM2.5 replaceable filter mask gasket dustproof anti-haze breathable mask gasket five-layer nonwoven filter',
            'price' => '0.26',
            'unit' => 'Piece',
            'min_order' => '250'
        ],
    ],
    'test_category_products' => [
        [
            'image' => 'images/test/Ambarella-A750-Car-Camera-DVR-FHD-1080P.jpg',
            'title' => 'Ambarella A750 Car Camera DVR FHD 1080P with Radar Detector Full Bands LED Display',
            'price' => '80',
            'unit' => 'Unit',
            'min_order' => '1000'
        ],
        [
            'image' => 'images/test/Car-camera-DVR-video-recorder.jpg',
            'title' => 'User\'s manual FHD 1080p car camera DVR video recorder A73 with night vision',
            'price' => '80',
            'unit' => 'Unit',
            'min_order' => '1'
        ],
        [
            'image' => 'images/test/Smart-Battery-Charger.jpg',
            'title' => 'Smart Battery Charger for Car',
            'price' => '9.3 - 9.5',
            'unit' => 'Unit',
            'min_order' => '1000'
        ],
        [
            'image' => 'images/test/1080P-WDR-GPS-WI-FI-Night-View.jpg',
            'title' => 'Antswoods Newest Private Mould Design Night View 1080P WDR Car Dvr With GPS Or WI-FI Optional',
            'price' => '55 - 60',
            'unit' => 'Piece',
            'min_order' => '1000'
        ]
    ],
    'category_tags' => [
        ['image' => 'images/test/index.png', 'name' => 'Agriculture Waste'],
        ['image' => 'images/test/index2.png', 'name' => 'Agriculture Equipments'],
        ['image' => 'images/test/index-3.png', 'name' => 'Animal Feed'],
        ['image' => 'images/test/index-4.png', 'name' => 'Animal Products'],
        ['image' => 'images/test/index-5.png', 'name' => 'Beans'],
        ['image' => 'images/test/index-6.png', 'name' => 'Fresh Fruit'],
    ],
    'hot_categories' => [
        ['image' => 'images/test/index-7.png', 'name' => 'Beans'],
        ['image' => 'images/test/index-8.png', 'name' => 'Essential Farm Products'],
        ['image' => 'images/test/index-9.png', 'name' => 'Grain Vegetables Fruit&Animal Products'],
        ['image' => 'images/test/index-10.png', 'name' => 'Plant Seeds & Ornamental Plants'],
    ],
    'category_policies' => [
        [
            'title' => 'Browse Our Trusted Agricultural Products Manufacturers & Suppliers',
            'text' => 'Agriculture is one of the oldest economic areas of humanity. It has nurtured the development of civilization. One third of the Earth’s land area is used for agriculture today. Agriculture directly and indirectly impacts not only food production but the clothing industry as well. Agricultural products that come from animals, plants and fungi are also used in the pharmaceutical industry. The production of raw materials is carried out for processing in the food industry and at the same time, agricultural raw materials such as cotton and linen, are also refined in the clothing industry. Agriculture is used to sustain and enhance human life. eWorldTrade has always been the ideal place for international e-commerce trade. Our B2B portal, opens your brand up to a huge range of manufacturers immersed in providing you with the highest quality of products.'
        ],
        [
            'title' => 'Premium Agricultural Machinery',
            'text' => 'Modern technological developments have hugely impacted the agriculture industry. We no longer need to use traditional methods that take up a lot of time to get the results we need. We now have advanced machinery to do the work for us in less than half the time period. Agricultural machinery is vastly the reason we have so much available to us today, as it has made the process easier and much more reliable. It has sharply increased yields from cultivation. Newer technology has helped decrease the environmental damage some of the olden technology was causing. Improvements are continuing to be made every day in the agricultural industry. Our B2B platform, eWorldTrade, is here to provide you with some of the technologically advanced farm machinery including, silos, greenhouses, aquaculture, and animal husbandry equipment.'
        ],
        [
            'title' => 'Produce and Plants',
            'text' => 'Today, we have fertilizers and pesticides available to us that do their job without excessively damaging the crops. We can easily provide nutrients to the plants and have them yield much better results than ever before. If you’re looking to make your crops, diet or lifestyle better, be sure to give eWorldTrade a look. We have everything from animal waste to provide your crops with the nutrients they need to grains, pulses, beans, fruits and produce for your body and health. eWorldTrade also carries ornamental plants and seeds, bulbs for decoration and gardening all in one place to make your shopping experience smoother.'
        ],
        [
            'title' => 'Service Beyond Expectation',
            'text' => 'Get in touch with premium manufacturers and suppliers free of charge and buy Agricultural Machinery and Equipment that best meet your budget and quality requirement. There are thousands of sellers from different countries who can help you buy the best Agricultural Machinery and Equipment available. Our services have been designed to make your process easier and faster. eWorldTrade is always working on improving itself so you know that we are always open to any feedback that you may have. If you have any queries to make, you can freely do so with the respective supplier of the products you are interested in.'
        ],
        [
            'title' => 'Trusted Trading Assurity',
            'text' => 'eWorldTrade is the best place where you can reach our trusted sellers, suppliers, manufacturers and dealers of new and used Agricultural Machinery, equipment and products. We have made sure that our clients are supplied with the best items possible. eWorldTrade makes sure that both buyers and suppliers honor the key terms and conditions of the contract. If you have any inquiries, you can freely contact our sellers and we’ll be more than happy to provide you with the appropriate information. Currently the platform is offering- Fertilizer spreader: a spreader for dosed and uniform application of granular fertilizer on agricultural lands such as fields or meadows. Fertilizer spreaders are built as a trailer, as a trailer or as a self-propelled vehicle. The drive is usually carried out via a power take-off (PTO) of the tractor or carrier vehicle, but there are also fertilizer spreaders with a drive through electric motors, hydraulic motors or floor drive via the wheels of the device; Thresher: an agricultural equipment for threshing of grain crops, particularly cereals. Today, threshing machines are no longer used in the professional field; they were replaced by combine harvesters in Germany in the 1950s to 1960s. There were stationary and mobile threshing machines. The threshing machines were first driven by horses\' heads or steam engines, later mostly by electric motors, stationary combustion engines or tractors. But there were also small threshing machines for manual operation. Another important machinery is the Cultivators also called a single-axle tractor is an agricultural machine for soil and grassland cultivation as well as transport tasks or land and path maintenance. A single-axle tractor has only two wheels driven by a gasoline or diesel engine using a mechanical transmission or hydrostatically and is steered by the operator, which is usually behind the engine. For the easier steering of the tractor, differentials, as well as single wheel clutches and brakes, can be installed. '
        ]
    ],
    'countries' => [
        ['name' => 'Argentina', 'code' => 'AR','amount'=>'6'],
        ['name' => 'Australia', 'code' => 'AU','amount'=>'1'],
        ['name' => 'Austria', 'code' => 'AT','amount'=>'4'],
        ['name' => 'Belgium', 'code' => 'BE','amount'=>'10'],
        ['name' => 'Brazil', 'code' => 'BR','amount'=>'156'],
        ['name' => 'Canada', 'code' => 'CA','amount'=>'2'],
    ],
];
