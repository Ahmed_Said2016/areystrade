<?php

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\UploadedFile;

if (!function_exists('paginationLimit')) {
    /**
     * @return array
     */
    function paginationLimit()
    {
        return [
            '10',
            '25',
            '50',
            '100',
        ];
    }
}
if (!function_exists('stringToInputArray')) {
    function stringToInputArray($str)
    {
        $strArr = explode('.', $str);
        return implode(array_map(function ($elm) use ($strArr) {
            if ($elm !== $strArr[0]) {
                return "[$elm]";
            }
            return $elm;
        }, $strArr));
    }
}
if (!function_exists('syncHasMany')) {
    function syncHasMany(HasMany $relation, array $request = [], $primary_key = 'id')
    {
        \App\Src\Sync\Sync::sync($relation, $request, $primary_key);
    }
}
if (!function_exists('image')) {
    /**
     * @return \App\Src\Imageable\Factory
     */
    function image()
    {
        return app(\App\Src\Imageable\Factory::class);
    }
}
//if (!function_exists('image')) {
//    /**
//     * @param array $data
//     * @param string $key
//     * @param $path
//     * @param \Closure $func
//     * @return \Uploading\Image\UploadingImage|null
//     */
//    function image(array $data, $key, $path, Closure $func)
//    {
//        return resolve('upload')->createImage($data, $key, $path, $func);
//    }
//}
if (!function_exists('multipleImage')) {
    /**
     * @param array $data
     * @param string $key
     * @param $path
     * @param \Closure $func
     * @return \Uploading\Image\UploadingImage|null
     */
    function multipleImage(array &$data, $key, $path, Closure $func)
    {
        return resolve('upload')->createMultipleImages($data, $key, $path, $func);
    }
}
if (!function_exists('uploading')) {
    function uploading(&$file, $path, Closure $func = null)
    {
        if ($file instanceof UploadedFile) {
            resolve('matrix.image')
                ->upload($file, $path, $func);
        }

    }
}
if (!function_exists('uploadingResolver')) {
    /**
     * @return \Matrix\Image\ImageFacade
     */
    function uploadingResolver()
    {
        return resolve('matrix.image');
    }
}
if (!function_exists('translate')) {
    function translate($word)
    {
        return resolve('localization.repo')->findByWord($word);
    }
}
if (!function_exists('translateModel')) {
    function translateModel($model, $field)
    {
        return resolve('localization.model')->translate($model, $field);
    }
}
if (!function_exists('currency')) {
    function currency()
    {
        return "usd";
    }
}
if (!function_exists('overAllRating')) {
    function overAllRating(\App\Models\Product $product)
    {
        if ($count = $product->ratings()->count()) {
            return $product->ratings()->sum('ratings.rate') / $count;
        }
        return 0;
    }
}
if (!function_exists('overAllRatingPercentage')) {
    function overAllRatingPercentage(\App\Models\Product $product)
    {
        $rate = overAllRating($product);
        if ($rate) {
            $rate = (round($rate * 2) / 2) / 5 * 100;
        }
        return $rate;

    }
}
if (!function_exists('inputIsChecked')) {
    function inputIsChecked(\Illuminate\Http\Request $request, $name, $val)
    {
        if ($request->has($name)) {
            $values = $request->get($name);
            if (is_array($values) && in_array($val, $values)) {
                return "checked";
            }
            if ($values == $val) {
                return "checked";
            }
        }
        return null;
    }
}
if (!function_exists('cart')) {
    /**
     * @return \App\Src\Cart\ShoppingCart
     */
    function cart()
    {
        return app()->make('cart');
    }
}