<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Model;

trait HasImage
{
    /**
     * @param Model $model
     * @param string $path
     * @param string $imageKey
     */
    protected function storeImageWithThumb(Model $model,string $path,string $imageKey)
    {
        if (request()->hasFile($imageKey)) {
            $imageInput = request()->file($imageKey);
            image()->make(function ($image) use ($model, $imageInput,$path) {
                /** @var  \App\Src\Imageable\Image $image */
                $image->file = $imageInput;
                $image->path = $path;
                $image->currents = [$model->image, $model->thumb];
                $thumb = $image->thumb(250);
                $image->save();
                $model->image = $image->name;
                $model->thumb = $thumb;
            });
        }

    }
}