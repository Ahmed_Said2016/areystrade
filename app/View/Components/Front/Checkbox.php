<?php

namespace App\View\Components\Front;

use Illuminate\View\Component;

class Checkbox extends Component
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $value;
    /**
     * @var bool
     */
    public $checked;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $value
     * @param bool $checked
     */
    public function __construct(string $name, string $value, bool $checked = false)
    {
        //
        $this->name = $name;
        $this->value = $value;
        $this->checked = $checked;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.front.checkbox');
    }

    public function isChecked()
    {
        return $this->checked ? 'checked' : '';
    }
}
