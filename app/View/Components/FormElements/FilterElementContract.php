<?php


namespace App\View\Components\FormElements;


use Illuminate\Support\Collection;
use Illuminate\View\Component;

abstract class FilterElementContract extends Component
{
    use AriaLabelResolving;
    /**
     * @var string
     */
    public $label;
    /**
     * @var string
     */
    public $name;
    /**
     * @var Collection|null
     */
    public $list;
    /**
     * @var array
     */
    public $current;
    /**
     * @var bool $required
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string $label
     * @param string $name
     * @param Collection|null $list
     * @param bool $required
     * * @param null|array $current
     */
    public function __construct($label, $name, ?Collection $list, $required = false, $current = null)
    {
        //
        $this->label = $label;
        $this->name = $name;
        $this->list = $list;
        $this->current = is_array($current) ? $current : [];
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    abstract function render();

    protected function elementNameWithoutLastDot($name)
    {
        $dotPosition = strrpos($name, ".");
        if ($dotPosition && $nameLength = strlen($name)) {
            return $dotPosition === ($nameLength - 1) ? substr($name, 0, $dotPosition) : $name;
        }
        return $name;
    }

}