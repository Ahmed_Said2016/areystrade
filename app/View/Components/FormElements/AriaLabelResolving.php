<?php


namespace App\View\Components\FormElements;


trait AriaLabelResolving
{
    /**
     * @param $name
     * @return string
     */
    public function ariaLabel($name)
    {
        if (strpos($name, '.') !== false) {
            return ucfirst(str_replace('.', " ", $name));
        }
        return ucfirst($name);
    }
}