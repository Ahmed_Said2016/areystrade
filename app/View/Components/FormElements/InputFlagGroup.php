<?php

namespace App\View\Components\FormElements;

use Illuminate\View\Component;

class InputFlagGroup extends Component
{
    const IMAGE_SOURCES = [
        'en' => 'images/panel/en-flag.jpg',
        'ar' => 'images/panel/eg-flag.jpg',
    ];
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $value;
    /**
     * @var string
     */
    public $image;
    /**
     * @var string
     */
    public $placeHolder;
    /**
     * @var bool
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $image
     * @param string $placeHolder
     * @param string|null $value
     * @param bool $required
     */
    public function __construct(string $name, string $image, string $placeHolder, ?string $value, bool $required = false)
    {
        $this->name = $name;
        $this->image = $image;
        $this->placeHolder = $placeHolder;
        $this->value = $value;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.input-flag-group');
    }

    public function imageSource($img)
    {

        return asset(static::IMAGE_SOURCES[$img]);
    }
}
