<?php

namespace App\View\Components\FormElements;

use Illuminate\View\Component;

class Input extends Component
{
    use AriaLabelResolving;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $value;
    /**
     * @var string
     */
    public $placeHolder;
    /**
     * @var string|null
     */
    public $type;
    /**
     * @var bool
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param string $value
     * @param string $placeHolder
     * @param bool $required
     * @param string|null $type
     */
    public function __construct(string $name, ?string $value, string $placeHolder, bool $required = false, string $type = null)
    {
        //
        $this->name = $name;
        $this->value = $value;
        $this->placeHolder = $placeHolder;
        $this->type = $type;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.input');
    }
}
