<?php

namespace App\View\Components\FormElements;

use Illuminate\View\Component;

class InlineInput extends Component
{
    /**
     * @var string
     */
    public $label;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string|null
     */
    public $value;
    /**
     * @var bool
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string $label
     * @param string $name
     * @param bool $required
     * @param string|null $value
     */
    public function __construct($label, $name, $required = false, $value = null)
    {
        //
        $this->label = $label;
        $this->name = $name;
        $this->value = $value;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.inline-input');
    }
}
