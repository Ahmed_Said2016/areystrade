<?php

namespace App\View\Components\FormElements;

use Illuminate\View\Component;

class Selector extends Component
{
    use AriaLabelResolving;
    /**
     * @var string
     */
    public $name;
    /**
     * @var array
     */
    public $list;
    /**
     * @var null
     */
    public $currentValue;
    /**
     * @var bool
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param array $list
     * @param null $currentValue
     * @param bool $required
     */
    public function __construct(string $name, array $list, $currentValue = null, bool $required = false)
    {
        //
        $this->name = $name;
        $this->list = $list;
        $this->currentValue = $currentValue;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.selector');
    }


    /**
     * @param $key
     * @return string
     */
    public function isSelected($key)
    {
        $checkValue=old($this->name)??$this->currentValue;
        return $checkValue == $key ? 'selected' : '';
    }

    /**
     * @return string
     */
    public function isRequired()
    {
        return $this->required ? 'required' : '';
    }
}
