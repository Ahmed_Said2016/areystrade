<?php

namespace App\View\Components\FormElements;



class InlineSelect extends FilterElementContract
{

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.inline-select');
    }

    public function isSelected(int $value)
    {
        $checkList = old($this->elementNameWithoutLastDot($this->name)) ?? $this->current;
        return in_array($value, $checkList) ? 'selected' : '';
    }
}
