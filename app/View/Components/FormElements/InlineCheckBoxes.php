<?php

namespace App\View\Components\FormElements;


class InlineCheckBoxes extends FilterElementContract
{


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.inline-check-boxes');
    }
    public function isChecked(int $value)
    {
        $checkList = old($this->elementNameWithoutLastDot($this->name)) ?? $this->current;
        return in_array($value, $checkList) ? 'checked' : '';
    }
}
