<?php

namespace App\View\Components\FormElements;

use Illuminate\View\Component;

class InputTypeFile extends Component
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var bool
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param bool $required
     */
    public function __construct(string $name, bool $required = false)
    {
        //
        $this->name = $name;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.input-type-file');
    }
}
