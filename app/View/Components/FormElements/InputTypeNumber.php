<?php

namespace App\View\Components\FormElements;

use Illuminate\View\Component;

class InputTypeNumber extends Component
{
    use AriaLabelResolving;
    /**
     * @var string
     */
    public $name;
    /**
     * @var null
     */
    public $value;
    /**
     * @var bool
     */
    public $required;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param null $value
     * @param bool $required
     */
    public function __construct(string $name, $value = null, bool $required = false)
    {
        //
        $this->name = $name;
        $this->value = $value;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.form-elements.input-type-number');
    }
}
