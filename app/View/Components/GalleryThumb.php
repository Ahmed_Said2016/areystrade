<?php

namespace App\View\Components;

use App\Models\Image;
use Illuminate\View\Component;

class GalleryThumb extends Component
{
    /**
     * @var Gallery
     */
    public $image;
    /**
     * @var string
     */
    public $inputName;

    /**
     * Create a new component instance.
     *
     * @param Image $image
     * @param string $inputName
     */
    public function __construct(Image $image,string $inputName)
    {
        //
        $this->image = $image;
        $this->inputName = $inputName;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.gallery-thumb');
    }
}
