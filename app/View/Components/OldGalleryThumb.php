<?php

namespace App\View\Components;

use App\Models\Image;
use Illuminate\View\Component;

class OldGalleryThumb extends Component
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var array
     */
    public $old;

    /**
     * Create a new component instance.
     *
     * @param string $name
     * @param array $old
     */
    public function __construct(string $name, array $old)
    {
        //
        $this->name = $name;
        $this->old = $old;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.old-gallery-thumb');
    }

    public function imageModel()
    {
        return Image::find($this->old['image_id']);
    }

    public function id()
    {
        return $this->imageModel()->id;
    }
}
