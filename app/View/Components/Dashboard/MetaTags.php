<?php

namespace App\View\Components\Dashboard;

use Illuminate\View\Component;

class MetaTags extends Component
{
    public $model;
    /**
     * @var string|null
     */
    public $inputArray;

    /**
     * Create a new component instance.
     *
     * @param $model
     * @param string|null inputArray
     */
    public function __construct($model, string $inputArray = null)
    {
        //
        $this->model = $model;
        $this->inputArray = $inputArray;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.dashboard.meta-tags');
    }

    public function inputName($name)
    {
        if (!is_null($this->inputArray)) {
            return "$this->inputArray.$name";
        }
        return $name;
    }
}
