<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    use MetaTagsTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                'main.parent_id' => 'sometimes|integer|nullable',
                'main.en_name' => sprintf('required|unique:categories,en_name,%d', $this->ignoringId()),
                'main.ar_name' => sprintf('required|unique:categories,ar_name,%d', $this->ignoringId()),
                'main.en_slug' => sprintf('required|unique:categories,en_slug,%d', $this->ignoringId()),
                'main.ar_slug' => sprintf('required|unique:categories,ar_slug,%d', $this->ignoringId()),
                'main.status' => 'required|boolean',
                'main.sort_order' => 'nullable|integer',
                'main.image' => 'sometimes|file|image|mimes:jpg,jpeg,bmp,png',
                'main.icon' => 'string|nullable',
                'main.has_products' => 'boolean|required',
                'filters'=>'required_if:main.has_products,1|array',
                'brands.*' => 'required_if:main.has_products,1|integer|exists:brands,id',
                'filters.*' => 'required_if:main.has_products,1|integer|exists:filters,id',
            ],
            $this->metaRules()
        );
    }
    public function setSlug($value){

    }

    /**
     * @return null|integer
     */
    private function ignoringId()
    {
        return optional($this->route('category'))->id;
    }
}
