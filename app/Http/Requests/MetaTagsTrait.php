<?php


namespace App\Http\Requests;


trait MetaTagsTrait
{
    public function metaRules()
    {
        return [
            'tags.en_title' => 'string|nullable',
            'tags.ar_title' => 'string|nullable',
            'tags.en_keywords' => 'string|nullable',
            'tags.ar_keywords' => 'string|nullable',
            'tags.en_description' => 'string|nullable',
            'tags.ar_description' => 'string|nullable',
        ];
    }
}