<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    use MetaTagsTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                'main.category_id' => 'required|exists:categories,id',
                'main.vendor_id' => 'nullable|exists:vendors,id',
                'main.en_name' => sprintf('required|unique:products,en_name,%d', $this->ignoringId()),
                'main.ar_name' => sprintf('required|unique:products,ar_name,%d', $this->ignoringId()),
                'main.en_slug' => sprintf('required|unique:products,en_slug,%d', $this->ignoringId()),
                'main.ar_slug' => sprintf('required|unique:products,ar_slug,%d', $this->ignoringId()),
                'main.status' => 'required|boolean',
                'main.sort_order' => 'nullable|integer',
                'main.image' => [Rule::requiredIf(is_null($this->ignoringId())), 'file', 'image', 'mimes:jpg,jpeg,bmp,png'],
                'gallery' => 'array|required|min:1',
                'gallery.*.image_id' => 'required|exists:images,id',
                'description.en_description' => 'required|string',
                'description.ar_description' => 'required|string',
                'filter_items' => 'array|required|min:1',
                'filter_items.*' => 'required|exists:filter_items,id',
                'price_setting.type' => ['required', Rule::in('fob', 'cif')],
                'price' => 'required|array|min:1',
                'price*amount' => 'required|integer|min:1',
                'price*minimum_quantity' => 'required|integer|min:1',
            ],
            $this->metaRules()
        );
    }

    public function setSlug($value)
    {

    }

    /**
     * @return null|integer
     */
    private function ignoringId()
    {
        return optional($this->route('product'))->id;
    }
}
