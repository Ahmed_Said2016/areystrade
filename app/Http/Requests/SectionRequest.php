<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'section.main.en_name' => sprintf('required|unique:sections,en_name,%d', $this->ignoringId()),
            'section.main.en_slug' => sprintf('required|unique:sections,en_slug,%d', $this->ignoringId()),
            'section.main.ar_name' => sprintf('required|unique:sections,ar_name,%d', $this->ignoringId()),
            'section.main.ar_slug' => sprintf('required|unique:sections,ar_slug,%d', $this->ignoringId()),
            'section.main.status' => 'required|boolean',
            'section.main.sort_order' => 'required|integer',
            'section.main.image' => 'sometimes|file|image',
            'section.tags.en_title' => 'required|string',
            'section.tags.ar_title' => 'required|string',
            'section.tags.en_keywords' => 'required|string',
            'section.tags.ar_keywords' => 'required|string',
            'section.tags.en_description' => 'required|string',
            'section.tags.ar_description' => 'required|string',
        ];
    }

    /**
     * @return null|integer
     */
    private function ignoringId()
    {
        return optional($this->route('brand'))->id;
    }
}
