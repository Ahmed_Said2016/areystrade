<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filters.basic.en_name' => sprintf('required|unique:filters,en_name,%d', $this->ignoringId()),
            'filters.basic.ar_name' => sprintf('required|unique:filters,ar_name,%d', $this->ignoringId()),
            'filters.basic.status' => 'required|boolean',
            'filters.basic.sort_order' => 'required|integer',
            'filters.items' => 'array|required',
            'filters.items.*.en_name' => 'required',
            'filters.items.*.ar_name' => 'required',
            'filters.items.*.sort_order' => 'required|integer',
        ];
    }

    /**
     * @return null|integer
     */
    private function ignoringId()
    {
        return optional($this->route('filter'))->id;
    }
}

