<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BrandsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en_name' => ['required', Rule::unique('brands')->ignore($this->ignoringId())],
            'ar_name' => ['required', Rule::unique('brands')->ignore($this->ignoringId())],
            'status' => 'required|boolean',
            'sort_order' => 'required|integer',
            'image' => 'sometimes|file|image'
        ];
    }

    /**
     * @return null|integer
     */
    private function ignoringId()
    {
        return optional($this->route('brand'))->id;
    }
}
