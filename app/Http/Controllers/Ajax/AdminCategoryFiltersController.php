<?php


namespace app\Http\Controllers\Ajax;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;

class AdminCategoryFiltersController extends Controller
{
    public function __invoke()
    {
        $category = Category::findOrFail(request()->get('category_id'));
        $product = !is_null(request()->get('product_id')) ? Product::findOrFail(request()->get('product_id')) : new Product();
        return view('ajax.admin.category_filters', compact('category', 'product'));
    }
}