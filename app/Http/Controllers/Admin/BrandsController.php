<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandsRequest;
use App\Models\Brand;
use Exception;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $limit = request()->get('limit') ?? 10;
        $brands = Brand::filter()->paginate($limit);
        return view('admin.brands.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $brand = new Brand();
        return view('admin.brands.create', compact('brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BrandsRequest $request)
    {
        try {
            Brand::create($request->input());
            return redirect()->route('admin.brands.index')->with('success', 'Brand has been inserted');
        } catch (Exception $e) {
            image()->rollback();
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Brand $brand)
    {
        return view('admin.brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandsRequest $request
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BrandsRequest $request, Brand $brand)
    {
        try {
            $brand->update($request->input());
            $brand->touch();
            return redirect()->route('admin.brands.index')->with('success', 'Brand has been updated');
        } catch (Exception $e) {
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Brand $brand)
    {
        try {
            $brand->delete();
            return redirect()->route('admin.brands.index')->with('success', 'Brand has been deleted');
        } catch (Exception $exception) {
            return redirect()->back()->with('failure', $exception->getMessage());
        }
    }

}
