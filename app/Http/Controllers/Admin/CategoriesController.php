<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Filter;
use Illuminate\Support\Carbon;
use Illuminate\Validation\ValidationException;
use Exception;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $limit = request()->get('limit') ?? 10;
        $categories = Category::filter()->paginate($limit);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $category = new Category();
        $categories = Category::parentsOnly()->get();
        $filters = Filter::all();
        $brands = Brand::all();
        return view('admin.categories.create', compact('categories', 'category', 'filters', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CategoryRequest $request)
    {
        try {
            Category::create($request->input('main'));
            return redirect()->route('admin.categories.index')->with('success', 'Category has been inserted');
        } catch (Exception $exception) {
            image()->rollback();
            return redirect()->back()->with('failure', $exception->getMessage())
                ->withInput($request->input());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        $categories = Category::parentsOnly()->get();
        $filters = Filter::all();
        $brands = Brand::all();
        return view('admin.categories.edit', compact('category', 'categories', 'brands', 'filters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryRequest $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $this->validateHasProducts($request, $category);
        try {
            $category->setUpdatedAt(Carbon::now());
            $category->update($request->input('main'));

            return redirect()->route('admin.categories.index')->with('success', 'Category has been updated');
        } catch (Exception $exception) {
            image()->rollback();
            return redirect()->back()->with('failure', $exception->getMessage())
                ->withInput($request->input());
        }

    }

    /**
     * @param CategoryRequest $request
     * @param Category $category
     * @throws ValidationException
     */
    private function validateHasProducts(CategoryRequest $request, Category $category)
    {
        if ($request->input('main.has_products') == 0 && $category->has_products && $category->products()->exists()) {
            throw ValidationException::withMessages(
                ['has_product_cant_changing' => 'this category has products inside , due you can\'t change category mode before delete related products']
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->route('admin.categories.index')->with('success', 'Category has been deleted');
    }


}
