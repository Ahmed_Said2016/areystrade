<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SectionRequest;
use App\Models\Brand;
use App\Models\Section;
use Illuminate\Http\Request;
use Exception;

class SectionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $limit = request()->get('limit') ?? 10;
        $sections = Section::filter()->paginate($limit);
        return view('admin.sections.index', compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $section = new Section();
        return view('admin.sections.create', compact('section'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SectionRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SectionRequest $request)
    {
        dd($request->input());
        try {

            Section::create($request->input('main'));
            return redirect()->route('admin.sections.index')->with('success', 'Section has been inserted');
        } catch (Exception $e) {
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Section $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        $brands = Brand::all();
        return view('admin.sections.edit', compact('section', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Section $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        $attributes = $request->all()['section'];
        $this->validator($attributes);
        if (isset($attributes['basic']['banner_img'])) {
            uploading($attributes['basic']['banner_img'], $this->path, function ($image) use ($section) {
                $image->current($section->banner_img);
            });
        }
//
//        image($attributes['basic'], 'banner_img', $this->path, function ($image) use (&$attributes, $section) {
//            $image->current($section->banner_img)
//                ->upload($attributes['basic']);
//        });
//        image($attributes['basic'], 'home_img', $this->path, function ($image) use (&$attributes, $section) {
//            $image->current($section->home_img)
//                ->thumb('thumb', 500)
//                ->upload($attributes['basic']);
//        });
        try {

            $section->update($attributes['basic']);
            $section->brands()->sync($attributes['brand']);
            $section->detail()->update($attributes['details']);
            return redirect()->route('admin.sections.index')->with('success', 'Section has been updated');
        } catch (\Exception $e) {
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Section $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        $section->delete();
        return redirect()->route('admin.sections.index')->with('success', 'Section has been deleted');
    }


}
