<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Filter;
use App\Models\Product;
use App\Models\Vendor;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Exception;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $limit = request()->get('limit') ?? 10;
        $products = Product::filter()->paginate($limit);
        return view('admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::where('has_products', 1)->get();
        $vendors = Vendor::all();
        $product = new Product();
        return view('admin.products.create', compact('categories', 'vendors', 'product'));
    }

    public function test()
    {
        return redirect()->route('admin.products.create')->withInput(
            [
                "_token" => "ullARJHTe4jDcY1pc5QIixhaYTUejBR1reR2l62u",
                "main" => [
                    "category_id" => "13",
                    "vendor_id" => "2",
                    "en_name" => "test en",
                    "ar_name" => "test ar",
                    "en_slug" => "test slug",
                    "ar_slug" => "test slug ar",
                    "status" => "1",
                    "sort_order" => "10",
                ],
                "tags" => [
                    "en_title" => "dsdasdasd",
                    "ar_title" => "asdasd",
                    "en_keywords" => "asdasd",
                    "ar_keywords" => "asdas",
                    "en_description" => "asdd",
                    "ar_description" => "asd",
                ],
                "gallery" => [
                    5 => [
                        "image_id" => "5",
                        "alt" => null,
                        "title" => null,
                        "caption" => null,
                    ],
                    6 => [
                        "image_id" => "6",
                        "alt" => null,
                        "title" => null,
                        "caption" => null,
                    ],
                    7 => [
                        "image_id" => "7",
                        "alt" => null,
                        "title" => null,
                        "caption" => null,
                    ],
                    8 => [
                        "image_id" => "8",
                        "alt" => "asdsad",
                        "title" => "asdasd",
                        "caption" => null,
                    ],
                ],
                "description" => [
                    "en_description" => "<p>asdasdads</p>",
                    "ar_description" => "<p>czczxczczx</p>",
                ],
                "filters" => [
                    3 => ["value" => "test filter input value"]
                ],
                "filter_items" => [
                    0 => "14",
                    1 => "15",
                    2 => "11",
                    3 => "17",
                ],
                "price_setting" => [
                    "type" => "cif",
                    "unit" => "Blade",
                ],
                "price" => [
                    0 => [
                        "minimum_quantity" => "100",
                        "amount" => "10",
                    ],
                    1 => [
                        "minimum_quantity" => "200",
                        "amount" => "9",
                    ],
                    2 => [
                        "minimum_quantity" => "300",
                        "amount" => "8",
                    ]
                ]
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        try {
            DB::beginTransaction();
            Product::create($request->input('main'));
            DB::commit();
            return redirect()->route('admin.products.index')->with('success', 'Product has been created');
        } catch (Exception $exception) {
            image()->rollback();
            DB::rollBack();
            return back()->with('failure', $exception->getMessage())->withInput($request->input());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        $vendors = Vendor::all();
        return view('admin.products.edit', compact('categories', 'vendors', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request, Product $product)
    {
        try {
            $product->setUpdatedAt(Carbon::now());
            $product->update($request->input('main'));
            return redirect()->route('admin.products.index')->with('success', 'Product has been updated');
        } catch (Exception $exception) {
            image()->rollback();
            return back()->with('failure', $exception->getMessage())->withInput($request->input());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        try {
            $product->delete();
            return redirect()
                ->route('admin.products.index')
                ->with('success', 'Product has successfully deleted');

        } catch (\Exception $e) {
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }

    public function getBrands($category = null)
    {
        $brands = Category::find($category)->allBrands();
        return view('admin.products.layouts.brands', compact('brands'));
    }

    public function getFilters($category = null)
    {
        $filters = Category::find($category)->filters()->get();
        return view('admin.products.layouts.filters', compact('filters'));
    }

    public function getFilterItems($filter)
    {
        $filter = Filter::find($filter);
        return view('admin.products.layouts.filters_items', compact('filter'));
    }

    private function validator(array $attributes, $id = null)
    {
        return Validator::make($attributes, [
            'basic.category_id' => 'required|integer|exists:categories,id',
            'basic.brand_id' => 'required|integer|exists:brands,id',
            'basic.vendor_id' => 'required|integer|exists:vendors,id',
            'basic.en_name' => ['required', Rule::unique('products', 'en_name')->ignore($id)],
            'basic.ar_name' => ['required', Rule::unique('products', 'en_name')->ignore($id)],
            'basic.ru_name' => ['required', Rule::unique('products', 'en_name')->ignore($id)],
            'basic.it_name' => ['required', Rule::unique('products', 'en_name')->ignore($id)],
            'basic.status' => 'boolean|required',
            'basic.sort_order' => 'required|integer',
            'basic.price' => 'required|integer|min:1',
            'basic.shipping' => 'boolean|required',
            'basic.quantity' => 'required|integer|min:1',
            'basic.min_quantity' => 'required|integer',
            'basic.date_available' => 'required|date',
            'basic.img' => 'image|nullable',
            // Meta tags
            'details.en_meta_title' => 'required|string',
            'details.ar_meta_title' => 'required|string',
            'details.ru_meta_title' => 'required|string',
            'details.it_meta_title' => 'required|string',
            'details.en_meta_keywords' => 'required|string',
            'details.ar_meta_keywords' => 'required|string',
            'details.it_meta_keywords' => 'required|string',
            'details.ru_meta_keywords' => 'required|string',
            'details.en_meta_description' => 'required|string',
            'details.ar_meta_description' => 'required|string',
            'details.it_meta_description' => 'required|string',
            'details.ru_meta_description' => 'required|string',
            // descriptions
            'description.en_description' => 'required|string',
            'description.ar_description' => 'required|string',
            'description.ru_description' => 'required|string',
            'description.it_description' => 'required|string',
            // filters
            'filters.*.filter_id' => 'required|integer|exists:filters,id',
            // filter items
            'filter_items.*.*' => 'integer|exists:filter_items,id',
        ])->validate();

    }

    protected function flattenFiltersArray($filters)
    {
        $result = [];
        array_walk($filters, function ($filter) use (&$result) {
            $result = array_merge($result, $filter);
        });
        return $result;
    }
}
