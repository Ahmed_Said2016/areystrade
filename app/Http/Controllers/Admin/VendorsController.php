<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class VendorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = request()->get('limit') ?? 10;
        $vendors = Vendor::filter()->paginate($limit);
        return view('admin.vendors.index', ['vendors' => $vendors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendor = new Vendor();
        return view('admin.vendors.create', compact('vendor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->all();
        $attributes['rand_code'] = md5(uniqid(rand(), TRUE));
        $attributes = $this->validator($attributes);
        try {
            Vendor::create($attributes);
            return redirect()->route('admin.vendors.index')->with('success', 'Vendor has been created');
        } catch (\Exception $e) {
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Vendor $vendor
     * @return void
     */
    public function edit(Vendor $vendor)
    {
        return view('admin.vendors.edit', compact('vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Vendor $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        $attributes = $this->validator($request->all(), $vendor->id);
        try {
            $vendor->update($attributes);
            return redirect()->route('admin.vendors.index')->with('success', 'Vendor has been updated');
        } catch (\Exception $e) {
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function validator(array $attributes, $id = null)
    {
        return Validator::make($attributes, [
            'email' => ['required', Rule::unique('vendors')->ignore($id)],
            'name' => "required|unique:vendors,name,$id",
            'rand_code' => ['sometimes', Rule::unique('vendors')->ignore($id)]
        ])->validate();

    }
}
