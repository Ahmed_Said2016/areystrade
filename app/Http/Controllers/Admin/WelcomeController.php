<?php


namespace App\Http\Controllers\Admin;


class WelcomeController
{
    public function __invoke()
    {
        return view('admin.welcome');
    }
}