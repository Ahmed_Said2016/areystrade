<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FilterRequest;
use App\Models\Filter;
use Exception;

class FiltersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $limit = request()->get('limit') ?? 10;
        $filters = Filter::filter()->paginate($limit);
        return view('admin.filters.index', compact('filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $filter = new Filter();
        return view('admin.filters.create', compact('filter'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FilterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FilterRequest $request)
    {
        try {
            $filters = Filter::create($request->input('filters.basic'));
            $filters->items()->createMany($request->input('filters.items', []));
            return redirect()->route('admin.filters.index')->with('success', 'Filters has been inserted');
        } catch (Exception $e) {
            return redirect()->back()
                ->withInput($request->input())
                ->with('failure', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Filter $filter
     * @return \Illuminate\View\View
     */
    public function edit(Filter $filter)
    {
        return view('admin.filters.edit', compact('filter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FilterRequest $request
     * @param Filter $filter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FilterRequest $request, Filter $filter)
    {
        try {
            $filter->update($request->input('filters.basic'));
            syncHasMany($filter->items(), $request->input('filters.items', []));
            return redirect()->route('admin.filters.index')->with('success', 'Filters has been updated');
        } catch (Exception $e) {
            return redirect()->back()
                ->withInput($request->input())
                ->with('failure', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Filter $filter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Filter $filter)
    {
        try {
            $filter->delete();
            return redirect()->route('admin.filters.index')->with('success', 'Filters has been deleted');
        } catch (Exception $e) {
            return redirect()->back()->with('failure', $e->getMessage());
        }
    }


}
