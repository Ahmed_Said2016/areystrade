<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function index()
    {
        if (auth()->guard()->check()) {
            return auth()->guard('vendor')->user()->gallery()->get();
        }
        return Image::all();
    }

    public function store(Request $request)
    {

        $uploadImage = $request->file('image');
        $imageModel = new Image();
        $imageModel->name = $uploadImage->getClientOriginalName();
        $imageModel->image_size = $uploadImage->getSize();
        image()->make(function ($image) use ($uploadImage, $imageModel) {
            /** @var  \App\Src\Imageable\Image $image */
            $image->file = $uploadImage;
            $image->path = "gallery";
            $thumb = $image->thumb(250);
            $image->save();
            $imageModel->url = $image->name;
            $imageModel->thumb = $thumb;
        });
        $imageModel->save();

        return json_encode([
            'id' => $imageModel->id,
            'name' => $imageModel->name,
            'url' => $imageModel->url,
            'thumb' => $imageModel->thumb,
            'created_at' => $imageModel->created_at,
        ]);
    }
}
