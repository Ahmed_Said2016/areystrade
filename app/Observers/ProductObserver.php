<?php

namespace App\Observers;

use App\Models\Product;
use App\Traits\HasImage;

class ProductObserver
{
    use HasImage;

    /**
     * Handle the product "created" event.
     *
     * @param Product $product
     * @return void
     */
    public function creating(Product $product)
    {
        $this->storeImageWithThumb($product, 'products', 'main.image');
    }

    /**
     * Handle the product "created" event.
     *
     * @param Product $product
     * @return void
     */
    public function created(Product $product)
    {
        $product->tags()->create(request()->input('tags'));
        $this->productGallery($product);
        $product->description()->create(request()->input('description'));
        $this->productFilters($product);
        $this->productFilterItems($product);
        $product->filterItems()->sync(request()->input('filter_items'));
        $product->priceSetting()->create(request()->input('price_setting'));
        $product->prices()->createMany(request()->input('price'));
    }

    /**
     * Handle the product "updating" event.
     *
     * @param Product $product
     * @return void
     */
    public function updating(Product $product)
    {
        $this->storeImageWithThumb($product, 'products', 'main.image');
    }

    /**
     * Handle the product "updated" event.
     *
     * @param Product $product
     * @return void
     */
    public function updated(Product $product)
    {
        image()->clean();
        $product->tags()->updateOrCreate(
            ['taggable_type' => Product::class, 'taggable_id' => $product->id],
            request()->input('tags')
        );
        $this->productGallery($product);
        $product->description()->updateOrCreate(['product_id' => $product->id], request()->input('description'));
        $this->productFilters($product);
        $this->productFilterItems($product);
        $product->priceSetting()->updateOrCreate(['product_id' => $product->id], request()->input('price_setting'));
        syncHasMany($product->prices(), request()->input('price', []));
    }

    /**
     * Handling Product Images Relation
     *
     * @param Product $product
     */
    private function productGallery(Product $product)
    {
        $product->images()->sync(request()->input('gallery'));
    }

    /**
     * Handling Product Filters Relation
     *
     * @param Product $product
     */
    private function productFilters(Product $product)
    {
        $product->filters()->sync(request()->input('filters'));
    }

    /**
     * Handling Product Filter Items Relation
     *
     * @param Product $product
     */
    private function productFilterItems(Product $product)
    {
        $product->filterItems()->sync(request()->input('filter_items'));
    }


}
