<?php


namespace App\Observers;


use App\Models\Category;
use App\Models\MetaTag;

class CategoryObserver
{
    public function creating(Category $category)
    {
        $category->image = $this->storeImage();
    }

    public function created(Category $category)
    {
        $category->tags()->create(request()->input('tags'));
        if ($category->has_products) {
            $category->brands()->sync(request()->input('brands', []));
            $category->filters()->sync(request()->input('filters', []));
        }
    }

    public function updating(Category $category)
    {
        if (request()->hasFile('main.image')) {
            $category->image = $this->storeImage($category->image);
        }
    }

    public function updated(Category $category)
    {
        image()->clean();
        $category->tags()->updateOrCreate(
            ['taggable_type' => Category::class, 'taggable_id' => $category->id],
            request()->input('tags')
        );
        if ($category->has_products) {
            $category->brands()->sync(request()->input('brands', []));
            $category->filters()->sync(request()->input('filters', []));
        }
    }

    private function storeImage(string $current = null)
    {
        return image()->upload(request()->file('main.image'), 'categories', $current);
    }

}