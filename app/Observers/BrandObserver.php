<?php

namespace App\Observers;

use App\Models\Brand;
use App\Traits\HasImage;

class BrandObserver
{
    use HasImage;
    /**
     * @var string|null
     */
    public $current_image;
    /**
     * @var string|null
     */
    private $current_thumb;

    /**
     * @param Brand $brand
     */
    public function creating(Brand $brand)
    {
        $this->storeImageWithThumb($brand,'brands','image');

    }

    public function updating(Brand $brand)
    {
        $this->storeImageWithThumb($brand,'brands','image');
    }


    public function updated(Brand $brand)
    {
        image()->clean();
    }

}