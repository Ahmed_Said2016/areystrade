<?php


namespace App\Src\Sync;


class Inserter extends QueryBuilder
{

    /**
     * Preparing Attributes
     *
     * @return QueryBuilder
     */
    public function prepareAttributes()
    {
        $this->attributes = array_filter($this->manager->attributes, function ($attribute) {

            return !isset($attribute[$this->primaryKey]);
        });;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function exec(): void
    {
        $this->relationInstance->createMany($this->attributes);
    }

}
