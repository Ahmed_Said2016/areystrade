<?php


namespace App\Src\Sync;


abstract class QueryBuilder
{
    /**
     * @var SyncManager
     */
    protected $manager;
    /**
     * @var string $primaryKey
     */
    protected $primaryKey;
    /**
     * @var \Illuminate\Database\Eloquent\Relations\HasMany $relationInstance
     */
    protected $relationInstance;
    /**
     * @var array $attributes
     */
    protected $attributes;

    /**
     * QueryBuilder constructor.
     * @param SyncManager $manager
     */
    public function __construct(SyncManager $manager)
    {
        $this->manager = $manager;
        $this->primaryKey = $manager->primaryKey;
        $this->relationInstance = $manager->relation;
    }

    /**
     * Preparing Attributes
     *
     * @return QueryBuilder
     */

    abstract public function prepareAttributes();

    /**
     * Accomplished DB Query
     */
    abstract public function exec(): void;

    protected function pluckByPrimary()
    {
        return $this->relationInstance->pluck($this->manager->primaryKey);
    }

}
