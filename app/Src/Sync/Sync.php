<?php

namespace App\Src\Sync;

use Illuminate\Database\Eloquent\Relations\HasMany;

class Sync
{


    public static function sync(HasMany $relation, array $request = [], $primary_key = 'id')
    {
        (new SyncManager($relation, $request, $primary_key))->sync();
    }


}
