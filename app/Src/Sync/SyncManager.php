<?php


namespace App\Src\Sync;


use Illuminate\Database\Eloquent\Relations\HasMany;

class SyncManager
{
    /**
     * @var HasMany
     */
    public $relation;
    /**
     * @var array
     */
    public $attributes;
    /**
     * @var string
     */
    public $primaryKey;
    /**
     * @var SyncBuilderInterface $updater
     */
    public $updater;
    /**
     * @var SyncBuilderInterface $inserter
     */
    public $inserter;
    /**
     * @var SyncBuilderInterface $deleter
     */
    public $deleter;

    /**
     * SyncManager constructor.
     * @param HasMany $relation
     * @param array $attributes
     * @param string $primaryKey
     */
    public function __construct(HasMany $relation, array $attributes, string $primaryKey)
    {
        $this->relation = $relation;
        $this->attributes = $attributes;
        $this->primaryKey = $primaryKey;
    }

    /**
     * Initiate query process
     */
    public function init()
    {
        $this->updater = (new Updater($this))->prepareAttributes();
        $this->inserter = (new Inserter($this))->prepareAttributes();
        $this->deleter = (new Deleter($this))->prepareAttributes();
    }

    /**
     * Execute Updating
     *
     * @return $this
     */
    private function update()
    {
        $this->updater->exec();
        return $this;
    }

    /**
     * Execute Inserting
     *
     * @return $this
     */
    private function insert()
    {
        $this->inserter->exec();
        return $this;
    }

    /**
     * Execute Deleting
     *
     * @return $this
     */
    private function delete()
    {
        $this->deleter->exec();
        return $this;
    }

    /**
     * Executing all queries
     */
    public function exec()
    {
        $this->update()
            ->delete()
            ->insert();

    }

    public function sync()
    {
        $this->init();
        $this->exec();
    }

}
