<?php


namespace App\Src\Sync;


class Updater extends QueryBuilder
{


    /**
     * Preparing Attributes
     *
     * @return QueryBuilder
     */
    public function prepareAttributes()
    {
        $this->attributes = array_filter($this->manager->attributes, function ($attribute) {
            $primaryKey = $this->primaryKey;
            $current = $this->pluckByPrimary()->toArray();
            return isset($attribute[$primaryKey]) && in_array($attribute[$primaryKey], $current);
        });
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function exec(): void
    {
        foreach ($this->attributes as $attribute) {
            $primaryKey = $this->primaryKey;
            $relationInstance=clone $this->relationInstance;
            $relationInstance->where($primaryKey, $attribute[$primaryKey])->update($attribute);
        }
    }

}
