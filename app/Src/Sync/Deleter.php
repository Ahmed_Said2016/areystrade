<?php


namespace App\Src\Sync;


class Deleter extends QueryBuilder
{
    /**
     * Preparing Attributes
     *
     * @return QueryBuilder
     */
    public function prepareAttributes()
    {
        $current = $this->pluckByPrimary()->toArray();
        $currentAttributes = array_column($this->manager->attributes, $this->primaryKey);
        $this->attributes = array_filter($current, function ($row) use ($currentAttributes) {
            return !in_array($row, $currentAttributes);
        });
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function exec(): void
    {
        $this->relationInstance->whereIn($this->primaryKey, $this->attributes)->delete();
    }

}
