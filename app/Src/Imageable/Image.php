<?php


namespace App\Src\Imageable;

use http\Exception\InvalidArgumentException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * Class Image
 * @package App\Src\Imageable
 * @property string $file
 * @property array $currents
 */
class Image
{
    /**
     * @var string $name Image Name
     */
    public $name;
    /**
     * @var Thumb[] $thumbs
     */
    public $thumbs = [];
    /**
     * @var null|UploadedFile $uploadFile
     */
    public $uploadFile;
    /**
     * @var string
     */
    public $path;
    /**
     * @var array $currents Current Images
     */
    private $currents = [];

    /**
     * @param UploadedFile $file
     * @param string $path
     * @param string|null $current
     * @return false|string
     */

    public function upload(UploadedFile $file, string $path, string $current = null)
    {
        $this->name = $file->store($path, 'public');
        if (!is_null($current)) {
            $this->currents[] = $current;
        }
        return $this->name;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    private function setFile(UploadedFile $file)
    {
        $this->uploadFile = $file;
        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    private function setPath(string $path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param $currents
     * @return $this
     */
    private function setCurrents($currents)
    {
        $this->currents = $currents;
        return $this;
    }

    /**
     * @param int $width
     * @param int|null $height
     * @return string
     */
    public function thumb(int $width, int $height = null)
    {
        $thumbInstance = $this->newThumbInstance($width, $height);
        return $thumbInstance->generate();
    }

    /**
     * @param int $width
     * @param int|null $height
     * @return Thumb
     */
    private function newThumbInstance(int $width, ?int $height)
    {
        return new Thumb($this, $width, $height);
    }

    /**
     * Saving image and thumbs
     */
    public function save()
    {
        $this->upload($this->uploadFile, $this->path);
        array_map(function ($thumb) {
            /**
             * @var Thumb $thumb ;
             */
            $thumb->make();
        }, $this->thumbs);
    }

    /**
     * Deleting image and all thumbs
     */
    public function rollback(): void
    {
        $this->delete()->deleteThumbs();
    }

    /**
     * @return $this
     */
    private function delete()
    {
        if (!is_null($this->name)) {
            Storage::disk('public')->delete($this->name);
        }
        return $this;
    }

    /**
     *
     */
    private function deleteThumbs()
    {
        array_map(function ($thumb) {
            /**
             * @var Thumb $thumb ;
             */
            $thumb->delete();
        }, $this->thumbs);
    }

    /**
     * Deleting Currents Images
     */
    public function clean()
    {
        Storage::disk('public')->delete($this->currents);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $method = sprintf('set%s', ucfirst($name));
        if (method_exists($this, $method)) {
            return $this->{$method}(...$arguments);
        }
        return $this->{$name}(...$arguments);
    }

    /**
     * @param $name
     * @param $value
     * @return mixed
     */
    public function __set($name, $value)
    {
        $method = sprintf('set%s', ucfirst($name));
        if (method_exists($this, $method)) {
            return $this->{$method}($value);
        }
        throw new InvalidArgumentException('this property not exists');
    }
}