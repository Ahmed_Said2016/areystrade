<?php


namespace App\Src\Imageable;


use Closure;

class Factory1
{
    public $image;

    private static $instance = null;

    private function __construct()
    {
    }

    private function __clone()
    {

    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function make(Closure $func = null)
    {
        $this->setImageInstance();
        if (!is_null($func)) {
            $func($this->image);
        }
        return $this->image;
    }

    private function setImageInstance()
    {
        if (is_null($this->image)) {
            $this->image = new Image();
        }
    }

//    public function __call($name, $arguments)
//    {
//        $this->setImageInstance();
//        return $this->image->{$name}(...$arguments);
//    }

}