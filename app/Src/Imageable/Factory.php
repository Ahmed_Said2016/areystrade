<?php


namespace App\Src\Imageable;


use Closure;

/**
 * Class Factory
 * @package App\Src\Imageable
 * @method string upload(\Illuminate\Http\UploadedFile $file, string $path ,string $current=null)
 */
class Factory
{
    /**
     * @var Image[] Image Instances
     */
    public $imageInstances = [];

    /**
     * @param Closure|null $func
     * @return void
     */
    public function make(Closure $func = null): void
    {
        $instance = $this->setImageInstance();
        if (!is_null($func)) {
            $func($instance);
        }
    }

    /**
     * @return Image
     */
    private function setImageInstance()
    {
        $instance = new Image();
        $this->imageInstances[] = $instance;
        return $instance;
    }

    public function rollback()
    {
        array_map(function ($image) {
            /**
             * @var Image $image
             */
            $image->rollback();
        }, $this->imageInstances);
    }

    /**
     * Clean All Image Instances Current Images
     */
    public function clean()
    {
        array_map(function ($image) {
            /**
             * @var Image $image
             */
            $image->clean();
        }, $this->imageInstances);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $instance = $this->setImageInstance();
        return $instance->{$name}(...$arguments);
    }

}