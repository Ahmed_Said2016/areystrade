<?php


namespace App\Src\Imageable;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as InterventionImage;

class Thumb
{
    /**
     * @var int
     */
    private $width;
    /**
     * @var int|null
     */
    private $height;
    /**
     * Thumb name
     *
     * @var string $name
     */
    public $name;
    /**
     * @var Image
     */
    private $image;

    /**
     * Thumb constructor.
     * @param Image $image
     * @param int $width
     * @param int|null $height
     */
    public function __construct(Image $image, int $width, ?int $height)
    {
        $this->width = $width;
        $this->height = $height;
        $this->image = $image;
    }

    public function generate()
    {
        $this->name = $this->generateThumbName();
        $this->image->thumbs[] = $this;
        return $this->name;
    }

    private function generateThumbName()
    {
        return sprintf(
            '%s/%s.%s',
            $this->image->path,
            md5(uniqid(mt_rand())),
            $this->image->uploadFile->getClientOriginalExtension()
        );
    }

    public function make(): void
    {
        Storage::disk('public')->put($this->name, $this->makeThumb());
    }

    private function makeThumb()
    {
        return InterventionImage::make($this->image->uploadFile->getRealPath())
            ->resize($this->width, $this->height, function ($ratio) {
                $ratio->aspectRatio();
            })->save();
    }

    public function delete()
    {
        if (!is_null($this->name)) {
            Storage::disk('public')->delete($this->name);
        }
    }
}