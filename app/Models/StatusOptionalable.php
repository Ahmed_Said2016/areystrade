<?php


namespace App\Models;


trait StatusOptionalable
{
    /**
     * @return array
     */
    public static function statusOptions()
    {
        return [
            0 => [
                'status' => 'Pending',
                'btn_color' => 'btn-warning'
            ],
            1 => [
                'status' => 'Confirmed',
                'btn_color' => 'btn-success'
            ]
        ];
    }

    public static function statusOptionsOnly()
    {
        return array_map(function ($option) {
            return $option['status'];
        }, self::statusOptions());
    }

    /**
     * @param $status
     * @return string|null
     */
    public function getStatusAttribute($status)
    {
        return !is_null($status) ? self::statusOptions()[$status] : null;
    }

    public function getStatusKeyAttribute()
    {
        return $this->getAttributes()['status']??null;
    }
}