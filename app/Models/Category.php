<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Category
 * @package App\Models
 * @property integer $id
 * @property bool $has_products
 * @property Category $parents
 * @property string $en_name
 * @property string $ar_name
 * @property array $full_en_name
 * @property string $image
 * @method static Category create(array $attribute)
 * @method static Builder parentsOnly()
 * @method static Category findOrFail(int $id)
 */
class Category extends Model
{
    use FilterableSearch, StatusOptionalable;
    protected  $with = ['brands', 'filters', 'parents'];
    protected  $fillable = [
        'parent_id',
        'en_name',
        'ar_name',
        'en_slug',
        'ar_slug',
        'sort_order',
        'status',
        'image',
        'icon',
        'has_products',
    ];

    /**
     * @param $sort_order
     *
     * Setting Sort Order Column
     */
    public function setSortOrderAttribute($sort_order)
    {
        $this->attributes['sort_order'] = $sort_order ?? 0;
    }

    /**
     * @param $en_slug
     *
     * Setting English Slug Column
     */
    public function setEnSlugAttribute($en_slug)
    {
        $this->attributes['en_slug'] = Str::slug($en_slug);
    }

    /**
     * @param $ar_slug
     *
     * Setting Arabic Slug Column
     */
    public function setArSlugAttribute($ar_slug)
    {
        $this->attributes['ar_slug'] = Str::slug($ar_slug);
    }

    /**
     * @return array
     */
    public static function hasProductOptions()
    {
        return [
            0 => [
                'status' => 'Parent Category',
                'btn_color' => 'btn-warning'
            ],
            1 => [
                'status' => 'Has Products',
                'btn_color' => 'btn-success'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function productOptionsOnly()
    {
        return array_map(function ($option) {
            return $option['status'];
        }, self::hasProductOptions());
    }

    public function getHasProductStatusAttribute()
    {
        return self::hasProductOptions()[$this->has_products];
    }

    public function brands()
    {
        return $this->belongsToMany(Brand::class, 'category_brand');
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class);
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function parents()
    {
        return $this->parent()->with('parents');
    }

    public function getFullEnNameAttribute()
    {
        $nameList = [$this->en_name];
        $this->extractParentsName($this, 'en_name', $nameList);
        return $nameList;
    }

    private function extractParentsName(Category $category, string $column, array &$nameList)
    {
        if (!is_null($category->parents)) {
            $nameList[] = $category->parents->{$column};
            $category->extractParentsName($category->parents, $column, $nameList);
        }
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function tags()
    {
        return $this->morphOne(MetaTag::class, 'taggable');
    }

    /**
     * @param Builder $query
     */
    public function scopeParentsOnly(Builder $query)
    {
        $query->where('has_products', '0');
    }


//    /**
//     * @param array $children
//     * @return Category[]
//     */
//    public function allChildren(&$children = [])
//    {
//        if ($this->children()->exists()) {
//            $categories = $this->children()->get()->all();
//            /**
//             * @var Category $category
//             */
//            foreach ($categories as $category) {
//                $children[] = $category;
//                $category->allChildren($children);
//            }
//        }
//        return $children;
//    }
//
//    public function allBrands(&$brands = [])
//    {
//        $brands = array_merge($brands, array_udiff($this->brands()->get()->all(), $brands, function ($brand_1, $brand_2) {
//            $id_1 = $brand_1->id;
//            $id_2 = $brand_2->id;
//            if ($id_1 == $id_2) {
//                return 0;
//            }
//            return $id_1 > $id_2 ? 1 : -1;
//        }));
//        if ($this->parent()->exists()) {
//            $this->parent()->first()->allBrands($brands);
//        }
//        return $brands;
//    }
//
//
//    public function allParents(array $parents = [])
//    {
//        $parents = array_merge($parents, [$this]);
//        if (!is_null($this->parent)) {
//            return $this->parent->allParents($parents);
//        }
//        return collect(array_reverse($parents));
//    }
//
//    public function fullName($property, $separating = " > ")
//    {
//        $fullName = [];
//        $fullName[] = $this->{$property};
//        if (!is_null($this->parent)) {
//            $fullName[] = $this->parent->fullName($property);
//        }
//        return self::joinName($fullName, $separating);
//    }
//
//    private static function joinName(array $name, $separating)
//    {
//        return implode($separating, array_reverse($name));
//    }
//
//    public static function recommendationCategories()
//    {
//        return (new static)
//            ->where('recommended', 1)
//            ->where('image', '!=', '')
//            ->orderBy('sort_order')
//            ->limit(3)
//            ->get()
//            ->all();
//    }
//
//    public static function homeCategories()
//    {
//        return (new static)
//            ->where('status', 1)
//            ->where('home', 1)
//            ->where('welcome_image', '!=', '')
//            ->orderBy('home_sort_order')
//            ->limit(2)
//            ->get()
//            ->all();
//    }
}
