<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Filter
 * @package App\Models
 * @property boolean $is_required
 * @property boolean $accept_value
 * @property boolean $has_single_value
 * @method static Filter create(array $attributes)
 */
class Filter extends Model
{
    use StatusOptionalable, FilterableSearch;
    protected $fillable = [
        'en_name',
        'ar_name',
        'sort_order',
        'status',
        'has_single_value',
        'is_required',
        'accept_value',
    ];

    /**
     * @return array
     */
    public static function booleanAttributesOptions()
    {
        return [
            0 => [
                'status' => 'false',
                'style' => 'lar la-times-circle text-danger'
            ],
            1 => [
                'status' => 'true',
                'style' => 'las la-check-circle la-large text-success'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function singleAttributesOptions()
    {
        return [
            1 => [
                'status' => 'Single',
                'btn_color' => 'btn-warning'
            ],
            0=> [
                'status' => 'Multiple',
                'btn_color' => 'btn-success'
            ]
        ];
    }

    /**
     * @return array
     */
    public function getRequiredStatusAttribute()
    {
        return self::booleanAttributesOptions()[$this->is_required];
    }

    /**
     * @return array
     */
    public function getAcceptableStatusAttribute()
    {
        return self::booleanAttributesOptions()[$this->accept_value];
    }

    /**
     * @return array
     */
    public function getSingleStatusAttribute()
    {
        return self::singleAttributesOptions()[$this->has_single_value];
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(FilterItem::class);
    }


    /**
     * @return bool|void|null
     * @throws \Exception
     */
    public function delete()
    {
        parent::delete();
        $this->items()->delete();
    }

    public function productFilterItems($product_id)
    {
        return $this
            ->hasManyThrough(ProductFilterItem::class, FilterItem::class)
            ->where('product_filter_items.product_id', $product_id);
    }

}
