<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 * @package App\Models
 * @method static Brand create(array $attributes)
 * @property string $image
 * @property string $thumb
 */
class Brand extends Model
{
    use StatusOptionalable, FilterableSearch;
    protected $fillable = [
        'en_name', 'ar_name', 'sort_order', 'status', 'image', 'thumb'
    ];
    public $count;

    public function setCount($products)
    {
        $this->count = $products;
        return $this;
    }
}
