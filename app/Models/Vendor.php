<?php

namespace App\Models;


use App\User;

//use Illuminate\Database\Query\Builder;

class Vendor extends User
{
    protected $fillable = ['email', 'name', 'password', 'confirm', 'rand_code'];

    public static function statusOptions()
    {
        return [
            0 => [
                'status' => 'Pending',
                'btn_color' => 'btn-warning'
            ],
            1 => [
                'status' => 'Confirmed',
                'btn_color' => 'btn-success'
            ]
        ];
    }

    public function getConfirmAttribute($confirm)
    {
        return self::statusOptions()[$confirm];
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     */

    public function scopeFilter($query)
    {
        if (request()->has('criteria') && !is_null(request()->get('criteria'))) {
            $criteria = request()->get('criteria');
            $query->where('name', 'like', "%$criteria%")
                ->orWhere('email', 'like', "%$criteria%");
        }
        if (request()->has('status') && !is_null(request()->get('status'))) {
            $status = request()->get('status');
            $query->where('confirm', $status);
        }
    }
}
