<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaTag extends Model
{
    protected $fillable = [
        'taggable_id',
        'taggable_type',
        'en_title',
        'ar_title',
        'en_keywords',
        'ar_keywords',
        'en_description',
        'ar_description',
    ];
}
