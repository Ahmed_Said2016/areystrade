<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App\Models
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property string $thumb
 * @property string $image_size
 * @property string $created_at
 */
class Image extends Model
{
    protected $fillable = [
        'vendor_id',
        'name',
        'url',
        'thumb',
        'image_size',
    ];
}
