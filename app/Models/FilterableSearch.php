<?php


namespace App\Models;

/**
 * Trait FilterableSearch
 * @package App\Models
 * @method static \Illuminate\Database\Query\Builder filter()
 */
trait FilterableSearch
{
    /**
     * @param \Illuminate\Database\Query\Builder $query
     */
    public function scopeFilter($query)
    {
        if (request()->has('criteria') && !is_null(request()->get('criteria'))) {
            $criteria = request()->get('criteria');
            $query->where('en_name', 'like', "%$criteria%")
                ->orWhere('ar_name', 'like', "%$criteria%");
        }
        if (request()->has('status') && !is_null(request()->get('status'))) {
            $status = request()->get('status');
            $query->where('status', $status);
        }
    }
}