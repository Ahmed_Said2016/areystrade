<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 * @property string $image
 * @property int $id
 * @property string $thumb
 * @method static Product findOrFail(int $id)
 * @method static Product create(array $attributes)
 */
class Product extends Model
{
    use FilterableSearch, StatusOptionalable;
    protected $fillable = [
        'vendor_id',
        'category_id',
        'en_slug',
        'ar_slug',
        'en_name',
        'ar_name',
        'sort_order',
        'status',
        'image',
    ];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'product_filter')->withPivot(['value']);
    }

    public function filterItems()
    {
        return $this->belongsToMany(FilterItem::class, 'product_filter_item');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function priceSetting()
    {
        return $this->hasOne(PriceSetting::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function tags()
    {
        return $this->morphOne(MetaTag::class, 'taggable');
    }


    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable')
            ->withPivot('alt', 'title', 'caption');
    }

    public function description()
    {
        return $this->hasOne(ProductDescription::class);
    }

    public function getMinPriceAttribute()
    {
        return $this->prices()->orderBy('amount')->first();
    }
    public function getMinQuantityAttribute()
    {
        return $this->prices()->orderBy('minimum_quantity')->first();
    }


//    public function productFilters()
//    {
//        return $this->belongsToMany(FilterItem::class, 'product_filter_items');
//    }
//
//    public function productFilterList()
//    {
//        return array_column($this->productFilters()->get()->toArray(), 'id');
//    }
//
//    public function discount()
//    {
//        return $this->hasMany(Discount::class);
//    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function reviews()
    {
        return $this->ratings()
            ->whereNotNull('review')
            ->whereNotNull('title')
            ->where('confirm', 1);
    }


    /**
     * @param * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $field
     * @param string|null $table
     */
//    public function scopeHome($query, $field, $table = null)
//    {
//        $query->where($table . $field, 1)
//            ->where($table . 'status', 1)
//            ->orderBy($table . 'sort_order');
//    }

//    public static function recommendationProducts()
//    {
//        return (new static)
//            ->home('recommended')
//            ->get()
//            ->all();
//    }

//    public static function popularProducts()
//    {
//        return (new static)
//            ->home('popular')
//            ->get()
//            ->all();
//    }

//    public static function topProducts()
//    {
//        return (new static)
//            ->home('top')
//            ->get()
//            ->all();
//    }

//    public function delete()
//    {
//        $this->meta()->delete();
//        $this->gallery()->delete();
//        $this->description()->delete();
//        $this->discount()->delete();
//        $this->ratings()->delete();
//        parent::delete();
//    }
}
