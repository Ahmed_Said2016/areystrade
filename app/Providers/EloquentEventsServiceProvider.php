<?php

namespace App\Providers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\ServiceProvider;

class EloquentEventsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Brand::observe(\App\Observers\BrandObserver::class);
        Category::observe(\App\Observers\CategoryObserver::class);
        Product::observe(\App\Observers\ProductObserver::class);
    }
}
