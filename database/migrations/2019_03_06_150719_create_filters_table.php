<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('en_name')->unique();
            $table->string('ar_name')->unique();
            $table->integer('sort_order')->default(0);
            $table->boolean('status')->default(1);
            $table->boolean('has_single_value')->default(1);
            $table->boolean('is_required')->default(0);
            $table->boolean('accept_value')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
}
