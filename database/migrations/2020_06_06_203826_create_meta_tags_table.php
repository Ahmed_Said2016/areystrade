<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_tags', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('taggable_id');
            $table->string('taggable_type');
            $table->string('en_title')->nullable();
            $table->string('ar_title')->nullable();
            $table->string('en_keywords')->nullable();
            $table->string('ar_keywords')->nullable();
            $table->string('en_description')->nullable();
            $table->string('ar_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_tags');
    }
}
