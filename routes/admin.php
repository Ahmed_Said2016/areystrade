<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'WelcomeController')->name('welcome');
Route::resource('/vendors', 'VendorsController');
Route::resource('/filters', 'FiltersController')->except('show');
Route::resource('/brands', 'BrandsController')->except('show');
Route::resource('/sections', 'SectionsController')->except('show');
Route::resource('/categories', 'CategoriesController')->except('show');
Route::resource('products', 'ProductsController')->except('show');
Route::get('test/products', 'ProductsController@test');