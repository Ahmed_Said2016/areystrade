<?php

use Illuminate\Support\Facades\Route;


Route::view('/design/welcome', 'design.welcome');
Route::view('/design/category-parent', 'design.category-parent');
Route::view('/design/category-child', 'design.category-child');
Route::view('/design/product', 'design.product');


//Route::get('/design/category-child', function () {
//    dd(__('countries'));
//});

Route::get('/', function () {
    return "Welcome";
})->name('home');

//end Customer

// Admin
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Auth\LoginController@login')->name('admin.login');
Route::get('/logout', 'Auth\LoginController@logout')->name('admin.logout');
