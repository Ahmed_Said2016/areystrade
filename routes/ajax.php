<?php

use Illuminate\Support\Facades\Route;

Route::get('/admin/category/filters', 'AdminCategoryFiltersController')
    ->name('admin.category.filters');